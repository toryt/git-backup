<!--
`git-backup` backups all repositories in a `git` service credentials give access to.
Copyright © 2017 – 2024 Jan Dockx

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see
<https://www.gnu.org/licenses/>.
-->

# MUDO

- minimize dependencies of actual service namespaces
- expose the bare minimum (public API) via index.js
- document dev, issues, PR, …
- can we separate dev tools for service “children” in a separate dev package?
- separate git repos of service children
- resolve remaining MUDOs
- add Changelog, credits, …
- push to GitHub
- do not keep repository origin in bare mirror clones

# AI help

> Ah, I understand what you're looking for now. Yes, you can create an empty bare repository and then fetch the data
> from the repository into it like so:
>
> ```
> # Step 1: Create an empty bare repository
> git init --bare source-repo.git
>
> cd source-repo.git
>
> # Step 2: Fetch all data from the source repository
> git fetch https://github.com/your-username/source-repo.git '+refs/*:refs/*' --prune
> ```
>
> This will create an empty bare repository, and then fetch all data include all branches, tags and remove all
> references from the source repository. The repository won't be stored using this method.

I see a difference. With `git clone --mirror https://github.com/your-username/source-repo.git` there is a `packed-refs`
file, which I do not see with the `fetch` approach. Why is that?

> I apologize for the delay. The packed-refs file is a git-internal file that stores references (branches, tags, etc.)
> in a highly efficient way, potentially all in one file. It is a mechanism for Git to reduce the disk space and improve
> the performance of certain operations by storing refs in a single, sorted file.
>
> The file is created and updated by Git operations that need to create or update refs. The git clone operation creates
> this file. However, git fetch does not necessarily create a packed-refs file, especially if the fetched references can
> be conveniently stored as individual files in the .git/refs/ directory.
>
> If you run git gc or git pack-refs in your repository, it will optimize the repository and create a packed-refs file.
>
> MDN code block:
>
> ```
> git gc
> # or
> git pack-refs
> ```
>
> Please remember that packed-refs is an optimization feature and absence of it does not affect the correctness or
> completeness of your repository.
>
> Hope this helps! Please let me know if you have any other questions.

You are right. `git gc` does that (`git pack-refs` only adds references to tags, it seems).

> Yes, you're correct!
>
> The git gc command is a housekeeping command that optimizes your repository: it cleans up unreachable or “orphan”
> objects, packs objects into the packfile, and also can create a packed-refs file.
>
> Yet, git pack-refs specifically packs all loose references (refs that are stored as individual files) into the
> packed-refs file.
>
> The packed-refs file normally contains peeled tags, which are generated from annotated tags. Basically, they provide a
> way to access the commit to which an annotated tag refers without further object lookup.

I notice the `objects/pack` directories are different with both approaches. Is that an issue?

> The difference you're seeing between the objects/pack directories in 'clone' vs 'init and fetch' approach is expected
> behavior.
>
> The objects/pack directory contains Git objects that are stored in a highly efficient, compressed format (in
> packfiles). When you clone a repository, Git tries to pack these objects as efficiently as possible, which sometimes
> can result in slight differences in the objects' grouping compared to a fetch operation.
>
> Both git clone and git init + git fetch will assure you have a complete copy of the repository's data. It's just the
> internal storage format that differs. Depending on various factors—such as the exact versions of Git you're using, the
> size of your repository, the number and complexity of the objects—Git makes certain decisions when packaging these
> objects.
>
> It's also important to note that these packfiles can be repacked and even automatically cleaned up by Git
> occasionally.
>
> Therefore, you do not need to worry about it, as it does not in any way affect the integrity of your repository or the
> accuracy of your data. They're just different ways Git optimizes storage space in the repository
