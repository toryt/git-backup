#!/usr/bin/env node

const { GitHub } = require('../lib/github/GitHub')
const { main } = require('../lib/bin/main')

main(
  {
    humanServiceName: 'GitHub',
    shortCredentialsDescription: 'GitHub personal access token',
    credentialsDescription: `A <token> must be a GitHub personal access token that has the permission to read the
organizations the account is associated with, list the repositories of the account and of the organizations the account
is associated with, and read those repositories (scopes: \`read:user\`, \`read:org\`, \`repo\`).

See
<https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens>
on how to create GitHub personal access tokens.`,
    credentialsExample1: 'ghp_kD30Ew02kdee58We47lllEWJ32Ew48KKjdj3',
    credentialsExample2: 'ghp_kLDksi45aq22As5NOd244KdD654KD2ISI22d'
  },
  (baseDirectory, runDate, credentialsCandidates, salt) =>
    new GitHub(baseDirectory, runDate, credentialsCandidates, salt),
  process.argv
).then(/* resolve the Promise, because top-level await is not allowed */ () => {})
