#!/usr/bin/env node

const { main } = require('../lib/bin/main')
const { Bitbucket } = require('../lib/bitbucket/Bitbucket')

main(
  {
    humanServiceName: 'Bitbucket',
    shortCredentialsDescription: 'Bitbucket credentials',
    credentialsDescription: `An <account credentials> value must be a Bitbucket username and a Bitbucket account app
password separated by a colon, e.g., \`myusername:cbJdRb4BrHljFDi79nNl\`. The app password must allow to get the
workspaces associated with the account, the list of repositories associated the account and of the organizations the
account is associated with, and read those repositories (Account: Read; Workspace membership: Read; Repositories: Read).

See <https://support.atlassian.com/bitbucket-cloud/docs/app-passwords/> on how to create Bitbucket app passwords.`,
    credentialsExample1: 'firstusername:cbJdRb4BrHljFDi79nNl',
    credentialsExample2: 'secondusername:54YXlt3QzGdx2Z0fy0gp'
  },
  (baseDirectory, runDate, credentialsCandidates, salt) =>
    new Bitbucket(baseDirectory, runDate, credentialsCandidates, salt),
  process.argv
).then(/* resolve the Promise, because top-level await is not allowed */ () => {})
