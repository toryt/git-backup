# Desensifying

## General desensifier

An idea was to have a general `Desensifier`, where different functions would register sensitive information as soon as
it comes available. All output (logs, reports on disk, …) would then pass _all_ strings through the `Desensifier`, to be
sure the sensitive information never gets out of RAM. The `Desensifier` would replace any registered sensitive
information by a hash.

This is insecure though, through unwanted exposure, and might lead to injection.

### Exposure

Suppose the domain name of a `git` service is `https://git.gitfactory.org/`. Suppose `someUser` has the bright idea to
use `gitfactory` as password. The `git` URL would start with

```
https://someUser:gitfactory@git.gitfactory.org/…
```

and the desensified version would read

```
https://someUser:🤐da6e9e7ced🤐@git.🤐da6e9e7ced🤐.org/…
```

From this, anybody can derive that the sensitive password was `gitfactory`.

### Injection

In a second example, suppose we would make our live easier, and pass the entire text of a report in YAML format, which
contains the property `duration`, through the `Desensifier`.

```
…
duration: PT1M19.209S
…
```

Suppose `someUser` has the bright idea to use `duration` as password. The YAML output would be transformed into

```
…
🤐da6e9e7ced🤐: PT1M19.209S
…
```

This exposes the sensitive information, but this is also invalid YAML. Reader programs would crash.

If such `Desensifier` _would_ be used in this case, we would have to make sure only property values of type string are
desensified. But even then, the exposure issue remains.

### Conclusion

Desnsification must be done only for strings that are known to be sensitive, which cannot be determined in general.

## But isn’t the exposure case an issue anyway?

The exposure case is a special case of the issue considering a hash a safe way to protect sensitive information, with or
without a salt. In general, to retrieve the sensitive information from a hash, with or without a salt, we need to
calculate the hash of all possible strings of all possible lengths to see what original string results in the same hash.
That then was the sensitive information. This is an impossible feat.

One could however invest in creating a table with a few billion hashes of the most used sensitive data, and then just
lookup the hash in that table. If the sensitive data is in the collection of original strings for which the hash was
added to the table, the sensitive data is immediately exposed.

A salt protects against this by making even the most used sensitive data less used. But we cannot use a salt in this
program. The intention of showing a hash of sensitive information in logs and reports is to allow the user to identify
the sensitive information, and to known whether the same or different sensitive information was used in different runs.
When a salt is used, this requires the salt to be persistent for the sensitive information over different runs. There is
no easy way this program can do that, and if it would do that, it would expose the sensitive information through that
persistence.

This means a hash of the sensitive information exposed by this program is only safe when the sensitive information was
not in a collection of original strings for which a hash can be calculated in a lookup table. This means the sensitive
information must have enough entropy of itself to begin with.

This can be mitigated by using a general salt for the program as a whole. This is less secure that using a separate salt
for each sensitive information instance, but it does protect against the use of global lookup tables. The salt is known,
so a lookup table for this program could be created, but that is a much larger investment.

One step further, a salt could be generated, persisted, and re-used for a base directory. In that case, attackers would
not only need to create a specific lookup table for the program in general, but for each use of the program, which in
general serves a limited number of sensitive information instances (less than 10 in general, less than 1000 for sure).
This is considered secure enough, as it is as secure as using a separate hash for each sensitive information instance by
a factor of 3.

## Remaining issue

Sensitive information is still reported in the error messages of errors thrown by some libraries. These are revealed in
logs and reports.
