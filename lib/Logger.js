/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { isIn, isFunction, isA } = require('./pre')
const { Desensifier } = require('./Desensifier')

/**
 * @typedef {undefined |'error' | 'warn'| 'info' | 'debug' | 'trace' } TLogLevel
 */

/**
 * @type {Array<TLogLevel>}
 */
const loggingLevels = [undefined, 'error', 'warn', 'info', 'debug', 'trace']

/**
 * @type {TLogLevel}
 */
let loggingLevel = 'info'

class Logger {
  /**
   * @type {() => string}
   */
  #identifier

  /**
   * @type {Desensifier}
   */
  #desensifier

  /**
   * @param {() => string} identifier
   * @param {Desensifier} desensifier
   */
  constructor(identifier, desensifier) {
    isFunction(identifier)
    isA(Desensifier, desensifier)

    this.#identifier = identifier
    this.#desensifier = desensifier
  }

  /**
   * A function that returns a _non-sensitive_ representation of the instance that is logging. This might change when
   * more information comes available.
   *
   * @type {() => string}
   */
  get identifier() {
    return this.#identifier
  }

  get desensifier() {
    return this.#desensifier
  }

  get prefix() {
    return `[${this.desensifier.desensify(this.identifier())}]`
  }

  /**
   * @param {TLogLevel} messageLevel
   * @returns {boolean}
   */
  shouldLog(messageLevel) {
    return loggingLevels.indexOf(loggingLevel) >= loggingLevels.indexOf(messageLevel)
  }

  /**
   * @param {string} message
   */
  trace(message) {
    if (this.shouldLog('trace')) {
      console.debug('🔹', this.prefix, this.desensifier.desensify(message)) // do not use console.trace: it adds a stack trace
    }
  }

  /**
   * @param {string} message
   */
  debug(message) {
    if (this.shouldLog('debug')) {
      console.debug('🔷', this.prefix, this.desensifier.desensify(message))
    }
  }

  /**
   * @param {string} emoticon
   * @param {string} message
   */
  success(emoticon, message) {
    if (this.shouldLog('info')) {
      console.info(emoticon, this.prefix, this.desensifier.desensify(message))
    }
  }

  /**
   * @param {string} message
   */
  warn(message) {
    if (this.shouldLog('warn')) {
      console.warn('⚠️', this.prefix, this.desensifier.desensify(message))
    }
  }

  /**
   * @param {string} message
   */
  error(message) {
    if (this.shouldLog('error')) {
      console.error('🛑', this.prefix, this.desensifier.desensify(message))
    }
  }
}

/**
 * @returns {TLogLevel}
 */
function getLoggingLevel() {
  return loggingLevel
}

/**
 * @param {TLogLevel} level
 */
function setLoggingLevel(level) {
  isIn(loggingLevels, loggingLevel)

  loggingLevel = level
}

module.exports = { loggingLevels, getLoggingLevel, setLoggingLevel, Logger }
