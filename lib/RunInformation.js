/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { RelativePath } = require('./RelativePath')
const { isTrimmedNonEmptyString, isA } = require('./pre')
const { join, basename } = require('node:path')

const backupDirectoryName = '.git-backup'

/**
 * Read-only passive data structure with direct and derived properties that determine run parameters for all
 * {@link Service}s, {@link Account}s, and {@link Repository}s.
 *
 * @public
 */
class RunInformation {
  /**
   * @type {string}
   */
  #baseDirectory

  /**
   * @type {Date}
   */
  #runDate

  /**
   * @param {string} baseDirectory - Absolute path of the directory this program works in.
   * @param {Date} runDate - The time of the start of program execution. Used as unique identification of the run.
   */
  constructor(baseDirectory, runDate) {
    isTrimmedNonEmptyString(baseDirectory)
    isA(Date, runDate)

    this.#baseDirectory = baseDirectory
    this.#runDate = runDate
  }

  /**
   * Absolute path of the directory this program works in.
   *
   * @returns {string}
   */
  get baseDirectory() {
    return this.#baseDirectory
  }

  /**
   * Transform a path that is relative to the {@link #baseDirectory} to an absolute path.
   *
   * @param {TRelativePath} relativePath
   * @returns {string}
   */
  absolutePath(relativePath) {
    RelativePath.parse(relativePath)

    return join(this.baseDirectory, relativePath)
  }

  /**
   * The time of the start of program execution.
   *
   * Used as unique identification of the run, e.g., to make identifiers or paths unique.
   *
   * @returns {Date}
   */
  get runDate() {
    return this.#runDate
  }

  /**
   * Name of the directory in which this `Service` works, and used in reporting ang logging to identify the `Service`.
   *
   * @returns {string}
   */
  get baseDirectoryName() {
    return basename(this.baseDirectory)
  }

  /**
   * Relative path to the subdirectory of {@link #baseDirectory} where backups and repository run reports are stored
   * for this {@link Service}.
   *
   * @returns {TRelativePath}
   */
  get backupDirectory() {
    return backupDirectoryName
  }
}

module.exports = { backupDirectoryName, RunInformation }
