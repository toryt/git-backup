/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { ServiceGetAccountsResult } = require('./ServiceGetAccountsResult')
const { ServiceGetRepositoriesResult } = require('./ServiceGetRepositoriesResult')
const { isA, isTrimmedNonEmptyString, isStringArray, isNonEmptyString } = require('../pre')
const { RunInformation } = require('../RunInformation')
const { abstractMethod } = require('../abstract')
const { ServiceRunReport } = require('./ServiceRunReport')
const { saveRunReport } = require('../runReport/saveRunReport')
const { Desensifier } = require('../Desensifier')
const { Logger } = require('../Logger')
const { join } = require('node:path')
const { inspect } = require('node:util')
const { environmentInfo } = require('../runReport/EnvironmentInfo')
const { endAndDuration } = require('../runReport/endAndDuration')
const { AccountGetRepositoriesResult } = require('../account/AccountGetRepositoriesResult')

const maxConcurrentBackups = 20

/**
 * A name that (probably) will not conflict with the id of repositoroes in the backup directory, yet is readable for
 * humans, and will be shown on top in most OS listings.
 *
 * @public
 * @type {string}
 */
const runReportsDirectoryName = '_runs'

/**
 * The main responsibility of `Service` instances is to build service-specific {@link Account}s, gather
 * {@link Repository}s from them and call their {@link Repository#backup} method inside {@link backup}, and report on
 * all actions.
 *
 * `Service` instances are created from a CLI with a set of service-specific credentials, retrieved from outside this
 * program.
 *
 * @public
 * @implements {TSaveRunReportThis<TServiceRunReport>}
 */
class Service {
  static {
    this.prototype.runReportSchema = ServiceRunReport
    this.prototype.saveRunReport = saveRunReport
  }

  /**
   * @type {RunInformation}
   */
  #runInfo

  /**
   * @type {Array<string>}
   */
  #credentialsCandidates

  /**
   * @tyoe {Desensifier}
   */
  #desinsifier

  /**
   * @type {Logger}
   */
  #logger

  /**
   * @param {string} baseDirectory - Absolute path of the directory this program works in.
   * @param {Date} runDate - The time of the start of program execution. Used as unique identification of the run.
   * @param {Array<string>} credentialsCandidates
   * @param {string | undefined} [salt]
   */
  constructor(baseDirectory, runDate, credentialsCandidates, salt) {
    isTrimmedNonEmptyString(baseDirectory)
    isA(Date, runDate)
    isStringArray(credentialsCandidates)
    isNonEmptyString(salt, true)

    this.#runInfo = new RunInformation(baseDirectory, runDate)
    this.#credentialsCandidates = Array.from(new Set(credentialsCandidates).values()) // only store unique values
    this.#desinsifier = new Desensifier(salt)
    this.#credentialsCandidates.forEach(cc => {
      this.#desinsifier.addSensitive(cc)
    })
    this.#logger = new Logger(() => this.runInfo.baseDirectoryName, this.#desinsifier)
  }

  /**
   * Common data about the current execution.
   *
   * @returns {RunInformation}
   */
  get runInfo() {
    return this.#runInfo
  }

  /**
   * @type {Array<string>}
   */
  get credentialsCandidates() {
    return this.#credentialsCandidates.slice()
  }

  /**
   * @type {Desensifier}
   */
  get desensifier() {
    return this.#desinsifier
  }

  /**
   * @type {Logger}
   */
  get logger() {
    return this.#logger
  }

  /**
   * Return accounts for the supplied service-specific credentials, if possible.
   *
   * This method never throws. Reports on credentials that are not in the expected format, for which an {@link Account}
   * cannot be created.
   *
   * All {@link Account}s that have access to a {@link Repository} report it. It is possible that several of the
   * {@link Account}s we ask have access to the same {@link Repository} (determined by the {@link Repository#id}). We
   * need to back up each {@link Repository} only once. {@link #getRepositories} chooses the {@link Account} to use for
   * the backup in a consistent way, based on the order in which the {@link Account}s are returned by this method: first
   * one wins. This means that a concrete `Service` implementation should return the {@link Account}s with the more
   * important or stable ones earlier.
   *
   * @protected
   * @returns {TServiceGetAccountsResult}
   */
  getAccounts() {
    abstractMethod()
  }

  /**
   * Relative path to the subdirectory of the {@link #backupDirectory} where {@link TRepositoryRunReport}s are or will
   * be stored.
   *
   * @public
   * @returns {TRelativePath}
   */
  get runReportDirectory() {
    return join(this.runInfo.backupDirectory, runReportsDirectoryName)
  }

  /**
   * Return an {@link Array} of {@link Repository} instances from all {@link Account}s of this Service, without
   * duplicates based on {@link Repository#id}.
   *
   * This method never throws.
   *
   * @internal
   * @return {Promise<TServiceGetRepositoriesResult>}
   */
  async getRepositories() {
    const { wrongCredentials, accounts } = ServiceGetAccountsResult.parse(this.getAccounts())
    this.logger.success(`❇️`, `Found ${accounts.length} accounts.`)
    if (wrongCredentials.length > 0) {
      this.logger.warn(`Encountered ${wrongCredentials.length} credentials that could not be parsed:`)
      wrongCredentials.forEach(wc => this.logger.warn(inspect(wc)))
    }

    if (accounts.length > 0) {
      this.logger.trace(`Getting repositories from ${accounts.length} accounts …`)
    }

    const accountGetRepositoryResultsAndFailures = await Promise.allSettled(
      accounts.map(async account => {
        const result = await account.getRepositories()
        return AccountGetRepositoriesResult.parse(result)
      })
    )
    const {
      /** @type {Array<TAccountGetRepositoriesProgressStep>} */ progress,
      /** @type {number} */ nrOfAccountFailures,
      /** @type {number} */ nrOfAccountSuccesses,
      /** @type {number} */ nrOfRepositoriesFound,
      /** @type {Map<string, Repository>} */ uniqueRepositories
    } = [...accountGetRepositoryResultsAndFailures].reduce(
      (acc, settledResult) => {
        if (settledResult.status === 'fulfilled') {
          acc.progress.push(settledResult.value.progressStep)
          acc.nrOfAccountSuccesses++
          acc.nrOfRepositoriesFound = acc.nrOfRepositoriesFound + settledResult.value.repositories.length
          settledResult.value.repositories.forEach(repository => {
            // first one wins
            if (!acc.uniqueRepositories.has(repository.id)) {
              acc.uniqueRepositories.set(repository.id, repository)
            }
          })
        } else {
          acc.progress.push(settledResult.reason)
          acc.nrOfAccountFailures++
        }
        return acc
      },
      {
        progress: [],
        nrOfAccountFailures: 0,
        nrOfAccountSuccesses: 0,
        nrOfRepositoriesFound: 0,
        /** @type {Map<string, Repository>} */ uniqueRepositories: new Map()
      }
    )
    this.logger.success(
      `✳️`,
      `Found ${uniqueRepositories.size} unique repositories out of ${nrOfRepositoriesFound} in ${nrOfAccountSuccesses} accounts.`
    )
    if (nrOfAccountFailures > 0) {
      this.logger.warn(`${nrOfAccountFailures} accounts could not retrieve their repositories.`)
    }

    const repositories = Array.from(uniqueRepositories.values())

    return {
      wrongCredentials,
      progress,
      nrOfAccountFailures,
      nrOfAccountSuccesses,
      nrOfRepositoriesFound,
      nrOfUniqueRepositoriesFound: repositories.length,
      repositories
    }
  }

  /**
   * This method never throws.
   *
   * @internal
   * @returns {Promise<TServiceRunReport>}
   */
  async backupAllRepositories() {
    const start = new Date()

    const { default: pLimit } = await import('p-limit')
    const limit = pLimit(maxConcurrentBackups)

    /** @type {TServiceGetRepositoriesResult} */
    const {
      wrongCredentials,
      progress,
      nrOfAccountFailures,
      nrOfAccountSuccesses,
      nrOfRepositoriesFound,
      nrOfUniqueRepositoriesFound,
      repositories
    } = ServiceGetRepositoriesResult.parse(await this.getRepositories())

    const backupResults = await Promise.allSettled(repositories.map(r => limit(() => r.backup())))

    /**
     * @type { {repositoryRunReports: Array<TRepositoryRunReport>, unsavedRepositoryRunReports:
     *   TUnsavedRunReportThrowable<TRepositoryRunReport>}}
     */
    const {
      /** @type {Array<TRepositoryRunReport>} */ repositoryRunReports,
      /** @type {Array<TUnsavedRunReportThrowable<TRepositoryRunReport>>} */ unsavedRepositoryRunReports
    } = backupResults.reduce(
      (acc, settledResult) => {
        if (settledResult.status === 'fulfilled') {
          acc.repositoryRunReports.push(settledResult.value)
        } else {
          acc.repositoryRunReports.push(settledResult.reason.runReport)
          // MUDO add run reports of failed create
          acc.unsavedRepositoryRunReports.push(settledResult.reason)
        }
        return acc
      },
      {
        repositoryRunReports: [],
        unsavedRepositoryRunReports: []
      }
    )

    const vInfo = await environmentInfo()
    return {
      v: 1,
      environment: vInfo,
      service: this.runInfo.baseDirectoryName,
      backupDirectory: this.runInfo.backupDirectory,
      start: start.toISOString(),
      wrongCredentials,
      progress,
      nrOfAccountFailures,
      nrOfAccountSuccesses,
      nrOfRepositoriesFound,
      nrOfUniqueRepositoriesFound,
      repositoryRunReports,
      unsavedRepositoryRunReports,
      ...endAndDuration(start, new Date()),
      success:
        wrongCredentials.length <= 0 &&
        nrOfAccountFailures <= 0 &&
        repositoryRunReports.every(rrr => rrr.success) &&
        unsavedRepositoryRunReports.length <= 0
    }
  }

  /**
   * @public
   * @returns {Promise<TServiceRunReport>}
   * @throws {TUnsavedRunReportThrowable<TServiceRunReport>}
   */
  async backup() {
    const runReport = await this.backupAllRepositories()
    await this.saveRunReport(runReport)
    return runReport
  }
}

module.exports = { runReportsDirectoryName, Service }
