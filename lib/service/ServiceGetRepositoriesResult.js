/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { Repository } = require('../repository/Repository')
const { AccountGetRepositoriesProgressStep } = require('../account/AccountGetRepositoriesProgressStep')
const { JSONValue } = require('../JSONValue')
const { SafeNatural } = require('../SafeNatural')

/**
 * @internal
 */
const ServiceGetRepositoriesResult = z.object({
  wrongCredentials: z.array(JSONValue),
  progress: z.array(AccountGetRepositoriesProgressStep),
  nrOfAccountFailures: SafeNatural,
  nrOfAccountSuccesses: SafeNatural,
  nrOfRepositoriesFound: SafeNatural,
  nrOfUniqueRepositoriesFound: SafeNatural,
  repositories: z.array(z.instanceof(Repository))
})

/**
 * @typedef {Object} TServiceGetRepositoriesResult
 * @internal
 * @property {Array<TJSONValue>} wrongCredentials
 * @property {Array<TAccountGetRepositoriesProgressStep>} progress
 * @property {number} nrOfAccountFailures
 * @property {number} nrOfAccountSuccesses
 * @property {number} nrOfRepositoriesFound
 * @property {number} nrOfUniqueRepositoriesFound
 * @property {Array<Repository>} repositories
 */

module.exports = {
  ServiceGetRepositoriesResult
}
