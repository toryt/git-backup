/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { dirname, relative, join } = require('node:path')
const { ensureSymlink, remove, lstat } = require('fs-extra')
const simpleGit = require('simple-git')
const { Logger } = require('../Logger')
const { saveRunReport } = require('../runReport/saveRunReport')
const { environmentInfo } = require('../runReport/EnvironmentInfo')
const { errorReport } = require('../runReport/ErrorReport')
const { endAndDuration } = require('../runReport/endAndDuration')
const { createFailed } = require('./Progress')
const { isA, isTrimmedNonEmptyString } = require('../pre')
const { RunInformation } = require('../RunInformation')
const { abstractProperty } = require('../abstract')
const { makePathSegmentSafe } = require('./makePathSegmentSafe')
const { create } = require('./GitCreateProgressStep')
const { ensureOrigin } = require('./GitEnsureOriginProgressStep')
const { update } = require('./GitUpdateProgressStep')
const { symLink } = require('./GitSymLinkProgressStep')
const { RepositoryRunReport } = require('./RepositoryRunReport')
const { GitURL } = require('./GitURL')
const { Desensifier } = require('../Desensifier')

const simpleGitNoSuchPathMessage = 'Cannot use simple-git on a directory that does not exist'

const runReportsDirectoryName = '.git-backup'

/**
 * @implements {TSaveRunReportThis<TRepositoryRunReport>}
 * @public
 */
class Repository {
  static {
    this.prototype.runReportSchema = RepositoryRunReport
    this.prototype.saveRunReport = saveRunReport
  }

  /**
   * @type {RunInformation}
   */
  #runInfo

  /**
   * @type {Desensifier}
   */
  #desensifier

  /**
   * @type {string}
   */
  #id

  /**
   * @type string
   */
  #name

  /**
   * @type {string}
   */
  #owner

  /** @type {Logger} */
  #logger

  /**
   * @param {RunInformation} runInfo - Information about the current run and the {@link Service} under which this
   *                                   repository was found.
   * @param {Desensifier} desensifier
   * @param {string} id - The id local to the service.
   * @param {string} owner
   * @param {string} name
   */
  constructor(runInfo, desensifier, id, owner, name) {
    isA(RunInformation, runInfo)
    isA(Desensifier, desensifier)
    isTrimmedNonEmptyString(id)
    isTrimmedNonEmptyString(owner)
    isTrimmedNonEmptyString(name)

    this.#runInfo = runInfo
    this.#desensifier = desensifier
    this.#id = id
    this.#owner = owner
    this.#name = name
    this.#logger = new Logger(() => join(this.runInfo.baseDirectoryName, owner, name), desensifier)
  }

  /**
   * Common data about the current execution.
   *
   * @returns {RunInformation}
   */
  get runInfo() {
    return this.#runInfo
  }

  /**
   * @returns {Desensifier}
   */
  get desensifier() {
    return this.#desensifier
  }

  /**
   * A string that uniquely identifies the repository in its {@link Service}. The structure depends on the `git`
   * service used.
   *
   * @return {string}
   */
  get id() {
    return this.#id
  }

  /**
   * Name of the owner, used in {@link #gitSymLinkPath} and reporting.
   *
   * @return {string}
   */
  get owner() {
    return this.#owner
  }

  /**
   * The name of the repository (without the owner). Used in the directory name of the backup.
   *
   * @return {string}
   */
  get name() {
    return this.#name
  }

  /**
   * @type {Logger}
   * @protected
   */
  get logger() {
    return this.#logger
  }

  /**
   * String that represents the URI at which `git` can access the repository on the service. This might contain
   * sensitive information (e.g., credentials as HTTP Basic Auth).
   *
   * @return {TGitURL}
   */
  get remoteURL() {
    abstractProperty()
  }

  /**
   * Relative path to the subdirectory of the {@link RunInformation#backupDirectory} where the mirror clone is or will
   * be located.
   *
   * @returns {TRelativePath}
   */
  get mirrorClonePath() {
    return join(this.runInfo.backupDirectory, makePathSegmentSafe(`${this.id}.git`))
  }

  /**
   * Make sure the origin of the repository at {@link #mirrorClonePath} is {@link #remoteURL}.
   *
   * It is a precondition that {@link #mirrorClonePath} should not exist, or be a bare `git` mirror clone.
   *
   * If this fails, it reports whether there is something at {@link #mirrorClonePath}, or whether the location is free.
   *
   * If this fails for any other reason, there are permission issues, there is an issue with the file system, or what is
   * found {@link #mirrorClonePath} is not a bare `git` mirror clone.
   *
   * There is nothing we can do about permission problems or other problem with disk access. These are permanent. The
   * user needs to intervene. No flotsam is left behind.
   *
   * If what is found {@link #mirrorClonePath} is not a bare `git` mirror clone, there is nothing we can do. Somebody
   * or something else put that there. This problem is permanent, and the user needs to intervene. The directory is left
   * behind, but this is not flotsam we created, so we do not know what to do with it. (Note: we could archive the
   * directory, and reuse the space, but since this is either deliberate interaction, or probably an error in this
   * program, we will not).
   *
   * In conclusion: if this fails for any other reason then that there is nothing at {@link #mirrorClonePath}, there is
   * nothing we must do, or nothing we can do. When this fails repeatedly, and other repositories do not have that
   * problem, user interaction is required.
   *
   * @internal
   * @returns {Promise<TGitEnsureOriginSuccess>}
   * @throws {TGitEnsureOriginFailure}
   */
  async gitEnsureOrigin() {
    const start = new Date()
    /**
     * @type {Omit<TGitEnsureOriginSuccess, 'success' | 'end' | 'duration'>}
     */
    const progress = {
      action: ensureOrigin,
      start: start.toISOString()
    }
    try {
      this.logger.trace(`Ensuring origin is ${this.remoteURL} …`)
      const mirrorClone = simpleGit(this.runInfo.absolutePath(this.mirrorClonePath))
      await mirrorClone.remote(['set-url', 'origin', GitURL.parse(this.remoteURL)])
      this.logger.debug(`Ensured origin is ${this.remoteURL}.`)
      return {
        ...progress,
        ...endAndDuration(start, new Date()),
        success: true
      }
    } catch (/** @type {Error} */ err) {
      const error = errorReport(err)
      /** @type {TGitEnsureOriginFailure} */
      const failure = {
        ...progress,
        ...endAndDuration(start, new Date()),
        success: false,
        pathExists: false,
        error
      }
      if (error.message === simpleGitNoSuchPathMessage) {
        this.logger.debug(`The mirror clone directory does not exist at ${this.mirrorClonePath}.`)
        throw failure
      }

      failure.pathExists = true
      this.logger.warn(
        `Could not ensure that mirror clone at ${this.mirrorClonePath} has origin ${this.remoteURL}: "${error.message}".`
      )
      throw failure
    }
  }

  /**
   * Git-update the mirror clone at {@link #mirrorClonePath}.
   *
   * It is a precondition that {@link #mirrorClonePath} should be a bare `git` mirror clone.
   *
   * This fails if there is nothing at {@link #mirrorClonePath}, when what is there is not a `git` directory, or there
   * is a permission or other problem with disk access, or when the update itself fails.
   *
   * There is nothing we can do about permission problems or other problem with disk access. These are permanent. The
   * user needs to intervene. No flotsam is left behind.
   *
   * If there is a problem with accessing the repository, there is nothing we can do. Either the problem is permanent
   * (e.g., access rights, or the repository disappeared or was renamed between discovering it and this attempt), or
   * transient (e.g., network issues). The user needs to intervene for a permanent issue. In any case, a future run
   * will
   * retry this if the repository is still found under its current id, which covers transient issues. No flotsam is
   * left
   * behind.
   *
   * If the directory does not exist, or is not a bare `git` mirror clone, this is unexpected. It was changed between
   * our previous check in {@link #ensureOrigin} and now, outside this program. If it turns out this is the correct
   * mirror clone, a future run will update it. If not, that will fail in the future if the repository is discovered
   * again. This problem is permanent, and the user needs to intervene. The directory is left behind, but this is not
   * flotsam we created, so we do not know what to do with it. (Note: we could archive the directory, and reuse the
   * space, but since this is either deliberate interaction, or probably an error in this program, we will not).
   *
   * In conclusion: if this fails, there is nothing we must do, or nothing we can do. When this fails repeatedly, and
   * other repositories do not have that problem, user interaction is required.
   *
   * @internal
   * @returns {Promise<TGitUpdateSuccess>}
   * @throws {TGitUpdateFailure}
   */
  async gitUpdate() {
    const start = new Date()
    /** @type {Omit<TGitUpdateSuccess, 'success' | 'end' | 'duration'>} */
    const progress = {
      action: update,
      start: start.toISOString()
    }
    try {
      this.logger.trace(`Starting update from ${this.remoteURL} …`)
      const mirrorClone = simpleGit(this.runInfo.absolutePath(this.mirrorClonePath))
      await mirrorClone.remote(['update'])
      this.logger.success(`♻️`, `Update done.`)
      this.logger.debug(`Update from ${this.remoteURL} done.`)
      return { ...progress, ...endAndDuration(start, new Date()), success: true }
    } catch (/** @type {Error} */ err) {
      const error = errorReport(err)
      this.logger.warn(`Could not update from ${this.remoteURL}: "${error.message}"`)
      // noinspection UnnecessaryLocalVariableJS
      /** @type {TGitUpdateFailure} */
      const failure = { ...progress, ...endAndDuration(start, new Date()), success: false, error }
      throw failure
    }
  }

  // MUDO do not leave the origin in the directory: it contains credentials, that can be read when the repository is served

  /**
   * Creates a new mirror clone from {@link remoteURL} at {@link mirrorClonePath}.
   *
   * It is a precondition that {@link #mirrorClonePath} should be free.
   *
   * This will fail if the directory already exists at {@link #mirrorClonePath}, if there is a problem with accessing
   * the repository at {@link #remoteURL}, or there is a permission or other problem with disk access.
   *
   * There is nothing we can do about permission problems or other problem with disk access. These are permanent. The
   * user needs to intervene. No flotsam is left behind.
   *
   * If there is a problem with accessing the repository, there is nothing we can do. Either the problem is permanent
   * (e.g., access rights, or the repository disappeared or was renamed between discovering it and this attempt), or
   * transient (e.g., network issues). The user needs to intervene for a permanent issue. In any case, a future run
   * will
   * retry this if the repository is still found under its current id, which covers transient issues. No flotsam is
   * left
   * behind.
   *
   * If the directory already exists, this is unexpected. It was created between our previous check in
   * {@link #ensureOrigin} and now, outside this program. If it turns out this is the correct mirror clone, a future
   * run
   * will update it. If not, that will fail in the future if the repository is discovered again. This problem is
   * permanent, and the user needs to intervene. The directory is left behind, but this is not flotsam we created, so
   * we do not know what to do with it. (Note: we could archive the directory, and reuse the space, but since this is
   * either deliberate interaction, or probably an error in this program, we will not).
   *
   * In conclusion: if this fails, there is nothing we must do, or nothing we can do. When this fails repeatedly, and
   * other repositories do not have that problem, user interaction is required.
   *
   * @internal
   * @returns {Promise<TGitCreateSuccess>}
   * @throws {TGitCreateFailure}
   */
  async gitCreate() {
    /* await asyncPrecondition(async () => !(await this.pathExists()))
         unnecessary to test: git.mirror will throw if the path is not free */

    const start = new Date()
    /** @type {Omit<TGitCreateSuccess, 'success' | 'end' | 'duration'>} */
    const progress = {
      action: create,
      start: start.toISOString()
    }
    const git = simpleGit()
    this.logger.trace(`Starting mirror clone from ${this.remoteURL} …`)
    try {
      await git.mirror(GitURL.parse(this.remoteURL), this.runInfo.absolutePath(this.mirrorClonePath))
      this.logger.success(`🔰️`, `Mirror clone created.`)
      this.logger.debug(`Mirror clone from ${this.remoteURL} created.`)
      return {
        ...progress,
        ...endAndDuration(start, new Date()),
        success: true
      }
    } catch (/** @type {Error} */ err) {
      const error = errorReport(err)
      // MUDO The error message might contain the remoteURL, which might contain credentials. Unsafe.
      this.logger.warn(`Mirror clone from ${this.remoteURL} failed: "${error.message}".`)
      // noinspection UnnecessaryLocalVariableJS
      /** @type {TGitCreateFailure} */
      const failure = {
        ...progress,
        ...endAndDuration(start, new Date()),
        success: false,
        error
      }
      throw failure
    }
  }

  // MUDO do not leave the origin in the directory: it contains credentials, that can be read when the repository is served

  /**
   * The relative path where a symbolic link will be created to {@link #mirrorClonePath}. The link has
   * `{@link #name}.git` as name, inside a directory name by the {@link owner}.
   *
   * @public
   * @returns {TRelativePath}
   */
  get gitSymLinkPath() {
    return join(this.owner, `${this.name}.git`)
  }

  /**
   * The relative path from (the directory of) {@link #gitSymLinkPath} to {@link #mirrorClonePath}.
   *
   * @internal
   * @returns {TRelativePath}
   */
  get gitSymLinkReference() {
    return relative(
      dirname(this.runInfo.absolutePath(this.gitSymLinkPath)),
      this.runInfo.absolutePath(this.mirrorClonePath)
    )
  }

  /**
   * Create a symbolic link at {@link #gitSymLinkPath} to {@link #mirrorClonePath} (using the relative
   * {@link #gitSymLinkReference}). If something exists at {@link #gitSymLinkPath} it is overwritten.
   *
   * This fails if nothing exists at {@link #mirrorClonePath} (or it is not a directory on Windows).
   * If this fails, what exists at {@link #gitSymLinkPath} remains untouched.
   *
   * @internal
   * @returns {Promise<TGitSymLinkSuccess>}
   * @throws {TGitSymLinkFailure}
   */
  async ensureGitSymLink() {
    /* NOTE: Introduced to work around https://github.com/jprichardson/node-fs-extra/issues/1038 */
    const symLinkExists = async absoluteGitSymLinkPath => {
      try {
        const stats = await lstat(absoluteGitSymLinkPath)
        if (!stats.isSymbolicLink()) {
          this.logger.trace(`${this.gitSymLinkPath} exists, but is not a symbolic link`)
        } else {
          this.logger.trace(`${this.gitSymLinkPath} exists, and is a symbolic link`)
        }
        return true
      } catch (err) {
        // This cannot be tested on CI, can it? It always works as root.
        /* istanbul ignore else */
        if (err.code === 'ENOENT') {
          this.logger.trace(`${this.gitSymLinkPath} does not exist.`)
          return false
        }

        // This cannot be tested on CI, can it? It always works as root.
        /* istanbul ignore next */
        throw err
      }
    }

    const start = new Date()
    this.logger.trace(`Ensuring symbolic link to ${this.mirrorClonePath} at ${this.gitSymLinkPath} …`)
    /** @type {Omit<TGitSymLinkSuccess, 'success' | 'end' | 'duration'>} */
    const progress = {
      action: symLink,
      start: start.toISOString(),
      overwritten: false
    }
    const absoluteGitSymLinkPath = this.runInfo.absolutePath(this.gitSymLinkPath)
    try {
      /* NOTE: We remove the symbolic link, if it exists, before we do anything else, to work around
             https://github.com/jprichardson/node-fs-extra/issues/1038
             We only do this when the target exists. Otherwise we leave it alone. */
      const [gitSymLinkExists] = await Promise.all([
        symLinkExists(absoluteGitSymLinkPath),
        // `lstat` will throw `ENOENT` when it does not exist, which is what we want; we are not interested in the result
        lstat(this.runInfo.absolutePath(this.mirrorClonePath))
      ])
      if (gitSymLinkExists) {
        await remove(absoluteGitSymLinkPath)
        progress.overwritten = true
      }
      await ensureSymlink(
        this.gitSymLinkReference,
        absoluteGitSymLinkPath,
        'junction' /* NOTE: yeah, 'junction'. For Windows. Never mind. */
      )
      this.logger.debug(`Ensured symbolic link to ${this.mirrorClonePath} at ${this.gitSymLinkPath}.`)
      return {
        ...progress,
        ...endAndDuration(start, new Date()),
        success: true
      }
    } catch (err) {
      const error = errorReport(err)
      this.logger.warn(
        `Could not create symbolic link to ${this.mirrorClonePath} at ${this.gitSymLinkPath}: "${error.message}".`
      )
      // noinspection UnnecessaryLocalVariableJS
      /** @type {TGitSymLinkFailure} */
      const failure = {
        ...progress,
        ...endAndDuration(start, new Date()),
        success: false,
        error
      }
      throw failure
    }
  }

  /**
   * Attempt to
   *
   * * update or create the mirror clone at {@link #mirrorClonePath} with the correct {@link #remoteURL}, and
   * * create a symbolic link at {@link #gitSymLinkPath} to the mirror clone
   *
   * and return a {@link TRepositoryRunReport}.
   *
   * _This method never throws_. The returned {@link TRepositoryRunReport} is either a
   * {@link TSuccessRepositoryRunReport} or a {@link TFailureRepositoryRunReport}. It’s `success` property tells the
   * caller whether the operation succeeded or failed.
   *
   * @internal
   * @returns {Promise<TRepositoryRunReport>}
   */
  async updateOrCreateAndLink() {
    const start = new Date()
    const vInfo = await environmentInfo()
    /** @type {Omit<TRepositoryRunReport, 'success' | 'end' | 'duration' | 'progress'>} */
    const runReport = {
      v: 1,
      environment: vInfo,
      service: this.runInfo.baseDirectoryName,
      id: this.id,
      owner: this.owner,
      name: this.name,
      mirrorClone: this.mirrorClonePath,
      remoteURL: this.remoteURL,
      gitSymLink: this.gitSymLinkPath,
      start: start.toISOString()
    }
    /* IDEA At the sha and date of the most recent commit
            git log --branches --no-walk --date=iso-strict --pretty=format:"%cd %H" | sort -r | head -n 1 */
    const progress = []
    try {
      progress.push(await this.gitEnsureOrigin())
      progress.push(await this.gitUpdate())
      progress.push(await this.ensureGitSymLink())
      return { ...runReport, progress, ...endAndDuration(start, new Date()), success: true }
    } catch (/** @type {TGitEnsureOriginFailure | TGitUpdateFailure | TGitSymLinkFailure} */ failure) {
      progress.push(failure)
      if (failure.action === ensureOrigin && !failure.pathExists) {
        try {
          progress.push(await this.gitCreate())
          progress.push(await this.ensureGitSymLink())
          return { ...runReport, progress, ...endAndDuration(start, new Date()), success: true }
        } catch (/** @type {TGitCreateFailure | TGitSymLinkFailure} */ gitCreateOrSymLinkFailure) {
          progress.push(gitCreateOrSymLinkFailure)
        }
      }

      return { ...runReport, progress, ...endAndDuration(start, new Date()), success: false }
    }
  }

  /**
   * Relative path to the subdirectory of the {@link #mirrorClonePath} where {@link TRepositoryRunReport}s are
   * or will be stored.
   *
   * @public
   * @returns {TRelativePath}
   */
  get runReportDirectory() {
    return join(this.mirrorClonePath, runReportsDirectoryName)
  }

  /**
   * Attempts to
   *
   * * update or create the mirror clone for this repository,
   * * to create a link to it that can be used to serve the mirror clone via a `git` server, and
   * * save the {@link TRepositoryRunReport}.
   *
   * It resolves to the {@link TRepositoryRunReport} when it can save the {@link TRepositoryRunReport}, whether the
   * update or create and creation of the link succeeds or not. It rejects when it cannot save the
   * {@link TRepositoryRunReport} for some reason.
   *
   * @public
   * @returns {Promise<TRepositoryRunReport>}
   * @throws {TUnsavedRunReportThrowable<TRepositoryRunReport>}
   */
  async backup() {
    /** @type {TRepositoryRunReport} */ const runReport = await this.updateOrCreateAndLink()
    if (!createFailed(runReport.progress)) {
      /* Do not save run reports here if create failed: there is no mirror clone, so there can be no run report
         directory in there. */
      await this.saveRunReport(runReport, `${this.owner}_${this.name}`)
    }
    return runReport
  }
}

module.exports = { runReportsDirectoryName, Repository }
