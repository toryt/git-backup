/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { isNonEmptyString } = require('../pre')
const { forbiddenWindowsChars } = require('../RelativePath')

const unsafeCodePoints = new RegExp(
  `[${forbiddenWindowsChars
    .map(c => (c === '\\' ? '\\\\' : c))
    .concat(['\\p{C}', '%'])
    .join('')}]`,
  'uig'
)

const dotCodePoint = `%u00002E`

const onlyDots = /^\.+$/

/**
 * Replaces characters in `ps` with a cross-platform path segment safe representation, i.e., `%uZZZZZZ`, where `ZZZZZZ`
 * is the hexadecimal representation of the offending character’s Unicode code point.
 *
 * Apart from the offending characters, the escape symbol `%` is replaced too.
 *
 * @internal
 * @param {string} pathSegment
 * @returns {string}
 */
function makePathSegmentSafe(pathSegment) {
  isNonEmptyString(pathSegment)

  if (onlyDots.test(pathSegment)) {
    return dotCodePoint.repeat(pathSegment.length)
  }

  return pathSegment.replace(
    unsafeCodePoints,
    match => `%u${match.codePointAt(0).toString(16).toUpperCase().padStart(6, '0')}`
  )
}

module.exports = { forbiddenWindowsChars, makePathSegmentSafe }
