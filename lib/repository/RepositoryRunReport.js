/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { RelativePath } = require('../RelativePath')
const { GitURL } = require('./GitURL')
const { RunReport } = require('../runReport/RunReport')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')
const { SuccessProgress, FailureProgress } = require('./Progress')

/**
 * @internal
 */
const RepositoryRunReportBase = RunReport.extend({
  v: z.literal(1),
  id: z.string().min(3).readonly(),
  owner: TrimmedNonEmptyString,
  name: TrimmedNonEmptyString,
  mirrorClone: RelativePath.readonly(),
  remoteURL: GitURL.readonly(),
  gitSymLink: RelativePath.readonly()
})

/**
 * @public
 */
const SuccessRepositoryRunReport = RepositoryRunReportBase.extend({
  progress: SuccessProgress.readonly(),
  success: z.literal(true).readonly()
})

/**
 * @typedef {Object} TSuccessRepositoryRunReport
 * @public
 * @property {1} v - version of this data structure
 * @property {TEnvironmentInfo} environment
 * @property {string} service
 * @property {string} id
 * @property {string} owner
 * @property {string} name
 * @property {TRelativePath} mirrorClone
 * @property {TGitURL} remoteURL
 * @property {TRelativePath} gitSymLink
 * @property {TISODate} start
 * @property {TSuccessProgress} progress
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {true} success
 */

/**
 * @public
 */
const FailureRepositoryRunReport = RepositoryRunReportBase.extend({
  progress: FailureProgress.readonly(),
  success: z.literal(false).readonly()
})

/**
 * @typedef {Object} TFailureRepositoryRunReport
 * @public
 * @property {1} v - version of this data structure
 * @property {TEnvironmentInfo} environment
 * @property {string} service
 * @property {string} id
 * @property {string} owner
 * @property {string} name
 * @property {TRelativePath} mirrorClone
 * @property {TGitURL} remoteURL
 * @property {TRelativePath} gitSymLink
 * @property {TISODate} start
 * @property {TFailureProgress} progress
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {false} success
 */

/**
 * @public
 */
const RepositoryRunReport = z.union([SuccessRepositoryRunReport, FailureRepositoryRunReport])

/**
 * @typedef {TSuccessRepositoryRunReport | TFailureRepositoryRunReport} TRepositoryRunReport
 * @public
 */

module.exports = {
  SuccessRepositoryRunReport,
  FailureRepositoryRunReport,
  RepositoryRunReport
}
