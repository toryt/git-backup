/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { GitEnsureOriginSuccess, GitEnsureOriginFailure } = require('./GitEnsureOriginProgressStep')
const { GitUpdateSuccess, GitUpdateFailure } = require('./GitUpdateProgressStep')
const { GitCreateSuccess, GitCreateFailure, create } = require('./GitCreateProgressStep')
const { GitSymLinkSuccess, GitSymLinkFailure } = require('./GitSymLinkProgressStep')

/**
 * @public
 */
const UpdateSuccessProgress = z.tuple([GitEnsureOriginSuccess, GitUpdateSuccess, GitSymLinkSuccess])

/**
 * @typedef {[TGitEnsureOriginSuccess, TGitUpdateSuccess, TGitSymLinkSuccess]} TUpdateSuccessProgress
 * @public
 */

/**
 * @public
 */
const CreateSuccessProgress = z.tuple([GitEnsureOriginFailure, GitCreateSuccess, GitSymLinkSuccess])

/**
 * @typedef {[TGitEnsureOriginFailure, TGitCreateSuccess, TGitSymLinkSuccess]} TCreateSuccessProgress
 * @public
 */

/**
 * @public
 */
const SuccessProgress = z.union([UpdateSuccessProgress, CreateSuccessProgress])

/**
 * @typedef {TUpdateSuccessProgress | TCreateSuccessProgress} TSuccessProgress
 * @public
 */

/**
 * @public
 */
const UpdateFailureProgress = z.tuple([GitEnsureOriginSuccess, GitUpdateFailure])

/**
 * @typedef {[TGitEnsureOriginSuccess, TGitUpdateFailure]} TUpdateFailureProgress
 * @public
 */

/**
 * @public
 */
const CreateFailureProgress = z.tuple([GitEnsureOriginFailure, GitCreateFailure])

/**
 * @typedef {[TGitEnsureOriginFailure, TGitCreateFailure]} TCreateFailureProgress
 * @public
 */

/**
 * @public
 */
const EnsureOriginFailureProgress = z.tuple([GitEnsureOriginFailure])

/**
 * @typedef {[TGitEnsureOriginFailure]} TEnsureOriginFailureProgress
 * @public
 */

/**
 * @public
 */
const UpdateSymLinkFailureProgress = z.tuple([GitEnsureOriginSuccess, GitUpdateSuccess, GitSymLinkFailure])

/**
 * @typedef {[TGitEnsureOriginSuccess, TGitUpdateSuccess, TGitSymLinkFailure]} TUpdateSymLinkFailureProgress
 * @public
 */

/**
 * @public
 */
const CreateSymLinkFailureProgress = z.tuple([GitEnsureOriginFailure, GitCreateSuccess, GitSymLinkFailure])

/**
 * @typedef {[TGitEnsureOriginFailure, TGitCreateSuccess, TGitSymLinkFailure]} TCreateSymLinkFailureProgress
 * @public
 */

/**
 * @public
 */
const FailureProgress = z.union([
  UpdateFailureProgress,
  CreateFailureProgress,
  EnsureOriginFailureProgress,
  UpdateSymLinkFailureProgress,
  CreateSymLinkFailureProgress
])

/**
 * @typedef {TUpdateFailureProgress | TCreateFailureProgress | TEnsureOriginFailureProgress | TUpdateSymLinkFailureProgress | TCreateSymLinkFailureProgress} TFailureProgress
 * @public
 */

/**
 * @typedef {TSuccessProgress | TFailureProgress} TProgress
 * @public
 */

/**
 * @public
 * @param {TProgress} progress
 * @returns {boolean}
 */
function createFailed(progress) {
  return progress.some(p => p.action === create && !p.success)
}

module.exports = {
  UpdateSuccessProgress,
  CreateSuccessProgress,
  SuccessProgress,
  UpdateFailureProgress,
  CreateFailureProgress,
  EnsureOriginFailureProgress,
  UpdateSymLinkFailureProgress,
  CreateSymLinkFailureProgress,
  FailureProgress,
  createFailed
}
