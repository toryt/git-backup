/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { ProgressStepSuccess, ProgressStepFailure, ProgressStep } = require('../runReport/ProgressStep')

/**
 * @type {'sym link'}
 * @public
 */
const symLink = 'sym link'

/**
 * @private
 */
const GitSymLinkBase = ProgressStep.extend({
  action: z.literal(symLink)
})

/**
 * @typedef {Object} TGitSymLinkBase
 * @orivate
 * @property {'sym link'} action
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {boolean} success
 */

/* Zod types are too complex for a simple guy like me. Note that this would not compile for some reason in TS. */
// noinspection JSCheckFunctionSignatures
/**
 * @public
 */
const GitSymLinkSuccess = GitSymLinkBase.merge(ProgressStepSuccess).extend({
  overwritten: z.boolean()
})

/**
 * @typedef {Object} TGitSymLinkSuccess
 * @public
 * @property {'sym link'} action
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {boolean} overwritten
 * @property {true} success
 */

/* Zod types are too complex for a simple guy like me. Note that this would not compile for some reason in TS. */
// noinspection JSCheckFunctionSignatures
/**
 * @public
 */
const GitSymLinkFailure = GitSymLinkBase.merge(ProgressStepFailure)

/**
 * @typedef {Object} TGitSymLinkFailure
 * @public
 * @property {'sym link'} action
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {false} success
 * @property {TErrorReport} error
 */

module.exports = {
  symLink,
  GitSymLinkSuccess,
  GitSymLinkFailure
}
