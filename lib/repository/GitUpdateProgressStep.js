/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { ProgressStepSuccess, ProgressStepFailure, ProgressStep } = require('../runReport/ProgressStep')

/**
 * @type {'update'}
 * @public
 */
const update = 'update'

/**
 * @private
 */
const GitUpdateBase = ProgressStep.extend({
  action: z.literal(update)
})

/**
 * @typedef {Object} TGitUpdateBase
 * @public
 * @property {'update'} action
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {boolean} success
 */

/* Zod types are too complex for a simple guy like me. Note that this would not compile for some reason in TS. */
// noinspection JSCheckFunctionSignatures
/**
 * @public
 */
const GitUpdateSuccess = GitUpdateBase.merge(ProgressStepSuccess)

/**
 * @typedef {Object} TGitUpdateSuccess
 * @public
 * @property {'update'} action
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {true} success
 */

/* Zod types are too complex for a simple guy like me. Note that this would not compile for some reason in TS. */
// noinspection JSCheckFunctionSignatures
/**
 * @public
 */
const GitUpdateFailure = GitUpdateBase.merge(ProgressStepFailure)

/**
 * @typedef {Object} TGitUpdateFailure
 * @public
 * @property {'update'} action
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {false} success
 * @property {TErrorReport} error
 */

module.exports = { update, GitUpdateSuccess, GitUpdateFailure }
