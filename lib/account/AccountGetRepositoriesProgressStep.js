/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { ProgressStepSuccess, ProgressStepFailure, ProgressStep } = require('../runReport/ProgressStep')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')

const AccountGetRepositoriesBase = ProgressStep.extend({
  name: TrimmedNonEmptyString.optional(),
  credentials: TrimmedNonEmptyString
})

/**
 * @typedef {Object} TAccountGetRepositoriesBase
 * @property {string} [name] - the name of the account, when possible
 * @property {string} credentials - A none-sensitive representation of the credentials used for the account, e.g., a
 *                                      hash. This can be used in reports to compare the credentials used in consecutive
 *                                      runs, and as an approximation to identify the account when a `name` cannot be
 *                                      determined.
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {boolean} success
 * @property {string} end
 */

/* Zod types are too complex for a simple guy like me. Note that this would not compile for some reason in TS. */
// noinspection JSCheckFunctionSignatures
/**
 * @public
 */
const AccountGetRepositoriesSuccess = AccountGetRepositoriesBase.merge(ProgressStepSuccess).extend({
  nrOfRepositoriesFound: z.number().int().nonnegative().finite().safe()
})

/**
 * @typedef {Object} TAccountGetRepositoriesSuccess
 * @public
 * @property {string} [name] - the name of the account, when possible
 * @property {string} credentials - A none-sensitive representation of the credentials used for the account, e.g., a
 *                                      hash. This can be used in reports to compare the credentials used in consecutive
 *                                      runs, and as an approximation to identify the account when a `name` cannot be
 *                                      determined.
 * @property {TISODate} start
 * @property {number} nrOfRepositoriesFound
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {true} success
 */

/* Zod types are too complex for a simple guy like me. Note that this would not compile for some reason in TS. */
// noinspection JSCheckFunctionSignatures
/**
 * @public
 */
const AccountGetRepositoriesFailure = AccountGetRepositoriesBase.merge(ProgressStepFailure)

/**
 * @typedef {Object} TAccountGetRepositoriesFailure
 * @public
 * @property {string} [name] - the name of the account, when possible
 * @property {string} credentials - A none-sensitive representation of the credentials used for the account, e.g., a
 *                                      hash. This can be used in reports to compare the credentials used in consecutive
 *                                      runs, and as an approximation to identify the account when a `name` cannot be
 *                                      determined.
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {false} success
 * @property {TErrorReport} error
 */

const AccountGetRepositoriesProgressStep = z.union([AccountGetRepositoriesSuccess, AccountGetRepositoriesFailure])

/**
 * @public
 * @typedef {TAccountGetRepositoriesSuccess | TAccountGetRepositoriesFailure} TAccountGetRepositoriesProgressStep
 */

module.exports = {
  AccountGetRepositoriesSuccess,
  AccountGetRepositoriesFailure,
  AccountGetRepositoriesProgressStep
}
