/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { abstractMethod } = require('../abstract')
const { isA } = require('../pre')
const { RunInformation } = require('../RunInformation')
const { Desensifier } = require('../Desensifier')

/**
 * The main responsibility of `Account` instances is to retrieve the list of {@link Repository}s the `Account` has
 * access to from the service in a service-specific way via {@link #getRepositories()}.
 *
 * @public
 */
class Account {
  /**
   * @type {RunInformation}
   */
  #runInfo

  /**
   * @type {Desensifier}
   */
  #desensifier

  /**
   * @param {RunInformation} runInfo
   * @param {Desensifier} desensifier
   */
  constructor(runInfo, desensifier) {
    isA(RunInformation, runInfo)
    isA(Desensifier, desensifier)

    this.#runInfo = runInfo
    this.#desensifier = desensifier
  }

  /**
   * @return {RunInformation}
   */
  get runInfo() {
    return this.#runInfo
  }

  /**
   * @return {Desensifier}
   */
  get desensifier() {
    return this.#desensifier
  }

  /**
   * Return a collection of {@link Repository} instances for all repositories to which this `Account` has access.
   *
   * Repositories are owned by the `Account`, others are owned by organizations, teams, workspaces, or other types of
   * owners, depending on the service. This method returns all repositories to which the account has access, not only
   * those that are owned by the `Account`.
   *
   * Some repositories are mentioned for the Account as well as for an organization, team, or workspace. There might be
   * different instances in the collection with the same {@link Repository#id}.
   *
   * This might return an empty array.
   *
   * This might throw exceptions if there are issues with communicating with the service.
   *
   * @public
   * @return {Promise<TAccountGetRepositoriesResult>}
   * @throws {TAccountGetRepositoriesFailure}
   */
  async getRepositories() {
    abstractMethod()
  }
}

module.exports = { Account }
