/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { ok, equal } = require('node:assert')
const assert = require('node:assert')
const { ZodType } = require('zod')

let preconditionsEnabled = false

/**
 * @internal
 * @param {boolean} enabled
 * @returns {void}
 */
function enablePreconditions(enabled) {
  preconditionsEnabled = enabled
}

/**
 * @internal
 * @param {() => void} assertion
 * @returns {void}
 */
function ifEnabled(assertion) {
  /* istanbul ignore else */
  if (preconditionsEnabled) {
    assertion()
  }
}

/**
 * @internal
 * @param {typeof Object.constructor} T
 * @param {unknown} x
 * @param {boolean} [canBeUndefined]
 * @returns {void}
 */
function isA(T, x, canBeUndefined) {
  ifEnabled(() => {
    ok(T)
    equal(typeof T, 'function')
    /* istanbul ignore if */
    if (x === undefined) {
      ok(canBeUndefined)
    } else {
      equal(typeof x, 'object')
      assert(x instanceof T)
    }
  })
}

/**
 * @internal
 * @param {ZodType} schema
 * @param {unknown} x
 * @param {boolean} [canBeUndefined]
 * @returns {void}
 */
function adheresTo(schema, x, canBeUndefined) {
  ifEnabled(() => {
    ok(schema)
    ok(schema instanceof ZodType)
    /* istanbul ignore if */
    if (x === undefined) {
      ok(canBeUndefined)
    } else {
      schema.parse(x) // throws if nok
    }
  })
}

/**
 * @internal
 * @param {unknown} x
 * @returns {void}
 */
function isStringArray(x) {
  ifEnabled(() => {
    assert(Array.isArray(x))
    x.forEach(e => {
      equal(typeof e, 'string')
    })
  })
}

/**
 * @internal
 * @param {unknown} x
 * @returns {void}
 */
function isFunction(x) {
  ifEnabled(() => {
    equal(typeof x, 'function')
  })
}

/**
 * @internal
 * @param {unknown} x
 * @returns {void}
 */
function isString(x) {
  ifEnabled(() => {
    equal(typeof x, 'string')
  })
}

/**
 * @internal
 * @param {unknown} x
 * @param {boolean} [canBeUndefined]
 * @returns {void}
 */
function isNonEmptyString(x, canBeUndefined) {
  ifEnabled(() => {
    if (x === undefined) {
      ok(canBeUndefined)
    } else {
      ok(x)
      equal(typeof x, 'string')
    }
  })
}

/**
 * @internal
 * @param {unknown} x
 * @returns {void}
 */
function isTrimmedNonEmptyString(x) {
  ifEnabled(() => {
    ok(x)
    equal(typeof x, 'string')
    equal(x, x.trim())
  })
}

/**
 * @internal
 * @param {Array<unknown>} a
 * @param {unknown} x
 * @returns {void}
 */
function isIn(a, x) {
  ifEnabled(() => {
    assert(Array.isArray(a))
    assert(a.includes(x))
  })
}

/**
 * @internal
 * @param {unknown} x
 * @returns {void}
 */
function isNonNegativeNumber(x) {
  ifEnabled(() => {
    ok(typeof x === 'number' && Number.isFinite(x) && !Number.isNaN(x) && x >= 0)
  })
}

module.exports = {
  enablePreconditions,
  isA,
  isStringArray,
  isFunction,
  adheresTo,
  isString,
  isNonEmptyString,
  isTrimmedNonEmptyString,
  isIn,
  isNonNegativeNumber
}
