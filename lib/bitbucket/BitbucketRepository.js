/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Repository } = require('../repository/Repository')
const { adheresTo } = require('../pre')
const { GitURL } = require('../repository/GitURL')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')

class BitbucketRepository extends Repository {
  /**
   * @type {string} cloneURI
   */
  #cloneURI

  /**
   * @type {TBitbucketPassword}
   */
  #appPassword

  /**
   * @param {RunInformation} runInfo - Information about the current run and the {@link Service} under which this repository
   *                                   was found.
   * @param {Desensifier} desensifier
   * @param {TBitbucketRepository} bitbucketRepository
   * @param {TBitbucketPassword} appPassword
   */
  constructor(runInfo, desensifier, { uuid, workspaceSlug, slug, cloneURI }, appPassword) {
    adheresTo(GitURL, cloneURI)
    adheresTo(TrimmedNonEmptyString, appPassword)

    super(runInfo, desensifier, uuid, workspaceSlug, slug)
    desensifier.addSensitive(appPassword)

    this.#cloneURI = cloneURI
    this.#appPassword = appPassword
  }

  /**
   * @return {string}
   */
  get cloneURI() {
    return this.#cloneURI
  }

  /**
   * @return {TBitbucketPassword}
   */
  get appPassword() {
    return this.#appPassword
  }

  /**
   * @inheritDoc
   * @returns {TGitURL}
   */
  get remoteURL() {
    const url = new URL(this.cloneURI)
    url.password = this.appPassword
    return url.href
  }
}

module.exports = { BitbucketRepository }
