/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Account } = require('../account/Account')
const { BitbucketUsername, BitbucketPassword } = require('./BitbucketCredentials')
const { Logger } = require('../Logger')
const { adheresTo } = require('../pre')
const { BitbucketRepository } = require('./BitbucketRepository')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')
const { z } = require('zod')
const { errorReport } = require('../runReport/ErrorReport')
const { endAndDuration } = require('../runReport/endAndDuration')

const bitbucketWorkspacesURI = 'https://api.bitbucket.org/2.0/workspaces'

const BitbucketWorkspace = z.object({
  slug: TrimmedNonEmptyString,
  repositoriesURI: z.string().url()
})

/**
 * @typedef {object} TBitbucketWorkspace
 * @property {string} slug
 * @property {string} repositoriesURI
 */

/**
 * @typedef {object} TBitbucketRepository
 * @property {string} uuid
 * @property {string} workspaceSlug
 * @property {string} slug
 * @property {string} cloneURI
 */

/**
 * @typedef {import("undici-types").HeadersInit & {authorization: string, accept: 'application/json'}} TFetchHeadersInit
 */

/**
 * @typedef {import("undici-types").RequestInit & {headers: TFetchHeadersInit}} TFetchRequestInit
 */

/**
 * @extends {Account<BitbucketRepository>}
 */
class BitbucketAccount extends Account {
  /** @type {Logger} */
  #logger

  /** @type {TBitbucketUsername} */
  #username

  /** @type {TBitbucketPassword} */
  #password

  /**
   * @type {TFetchRequestInit}
   */
  #fetchRequest

  /**
   * @param {RunInformation} runInfo
   * @param {Desensifier} desensifier
   * @param {TBitbucketUsername} username
   * @param {TBitbucketPassword} password
   */
  constructor(runInfo, desensifier, username, password) {
    adheresTo(BitbucketUsername, username)
    adheresTo(BitbucketPassword, password)

    super(runInfo, desensifier)
    this.#username = username
    this.#password = password
    desensifier.addSensitive(password)
    this.#logger = new Logger(() => this.username, desensifier)
    this.#fetchRequest = {
      method: 'GET',
      headers: {
        authorization: `Basic ${Buffer.from(username + ':' + password, 'utf-8').toString('base64')}`,
        accept: 'application/json'
      }
    }
  }

  /** @type {Logger} */
  get logger() {
    return this.#logger
  }

  /**
   * @returns {TBitbucketUsername}
   */
  get username() {
    return this.#username
  }

  /**
   * @returns {TBitbucketPassword}
   */
  get password() {
    return this.#password
  }

  /**
   * @type {TFetchRequestInit}
   */
  get fetchRequest() {
    return this.#fetchRequest
  }

  /**
   * @param {string} pluralTypeName
   * @param {string} [nextURI]
   * @returns {Promise<Array<unknown>>}
   * @throws {Error}
   */
  async getFromBitbucket(pluralTypeName, nextURI) {
    if (!nextURI) {
      this.logger.debug(`There are no more ${pluralTypeName} to get.`)
      return []
    }

    this.logger.trace(`Getting ${pluralTypeName} from ${nextURI} …`)
    const response = await fetch(nextURI, this.fetchRequest)
    const body = await response.json()

    const responses = body.values
    this.logger.debug(`Response from ${nextURI} received (${responses.length} ${pluralTypeName}).`)
    const nextResponses = await this.getFromBitbucket(pluralTypeName, body.next)

    return responses.concat(nextResponses)
  }

  /**
   * @returns {Promise<Array<TBitbucketWorkspace>>}
   * @throws {Error}
   */
  async getWorkspaces() {
    const responses = await this.getFromBitbucket('workspaces', bitbucketWorkspacesURI)
    return responses.map(ws => ({ slug: ws.slug, repositoriesURI: ws.links.repositories.href }))
  }

  /**
   *
   * @param {TBitbucketWorkspace} bitbucketWorkspace
   * @returns {Promise<Array<TBitbucketRepository>>}
   * @throws {Error}
   */
  async getRepositoriesForWorkspace(bitbucketWorkspace) {
    adheresTo(BitbucketWorkspace, bitbucketWorkspace)

    const responses = await this.getFromBitbucket('repositories', bitbucketWorkspace.repositoriesURI)
    return responses.map(r => {
      const cloneURI = r.links.clone.find(c => c.name === 'https').href
      return /** @type {TBitbucketRepository} */ {
        uuid: r.uuid,
        workspaceSlug: r.workspace.slug,
        slug: r.slug,
        cloneURI
      }
    })
  }

  /**
   * {@inheritDoc}
   *
   * @return {Promise<TAccountGetRepositoriesResult>}
   * @throws {TAccountGetRepositoriesFailure}
   */
  async getRepositories() {
    const start = new Date()
    /** @type {Omit<TAccountGetRepositoriesBase, 'success' | 'end' | 'duration'>} */
    const progress = {
      name: this.username,
      credentials: this.password,
      start: start.toISOString()
    }
    try {
      const workspaces = await this.getWorkspaces()
      /* IDEA: Here, we could seperate succesful and failing calls, proceed with the successful results, and report the
               failed ones. That would require extra complexity in the reporting though, which would be different
               between services. Since the chance that some GET calls would work, and others fail, close together, with
               the same credentials, where the service just told us what we have access to, are repository, this is not
               worth the complexity. */
      /** @type {Array<Array<TBitbucketRepository>>} */
      const workspacesRepositories = await Promise.all(workspaces.map(ws => this.getRepositoriesForWorkspace(ws)))
      /** @type {Array<TBitbucketRepository>} */
      const bitbucketRepositories = workspacesRepositories.flat()
      this.logger.debug(`Found ${bitbucketRepositories.length} repositories.`)
      /** @type {Array<BitbucketRepository>} */
      const repositories = bitbucketRepositories.map(
        bbr => new BitbucketRepository(this.runInfo, this.desensifier, bbr, this.password)
      )
      return {
        progressStep: {
          ...progress,
          nrOfRepositoriesFound: repositories.length,
          success: true,
          ...endAndDuration(start, new Date())
        },
        repositories
      }
    } catch (err) {
      const error = errorReport(err)
      this.logger.warn(`Could not get repositories: "${error.message}".`)
      // noinspection UnnecessaryLocalVariableJS
      /** @type {TAccountGetRepositoriesFailure} */
      const failure = {
        ...progress,
        success: false,
        ...endAndDuration(start, new Date()),
        error
      }
      throw failure
    }
  }
}

module.exports = { bitbucketWorkspacesURI, BitbucketWorkspace, BitbucketAccount }
