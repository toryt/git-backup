/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')

/**
 * The username and password, separated by a colon, trimmed.
 *
 * The username can only contain upper– and lowercase Roman letters, or digits, `_`, `-`.
 *
 * The password can only contain upper– and lowercase Roman letters, or digits.
 *
 * (These are best guesses, as no documentation on this was found).
 */
const BitbucketCredentials = z
  .string()
  .min(3)
  .regex(/^[-\w]+:[A-Za-z0-9]+$/)

/** @typedef {string} TBitbucketCredentials */

const BitbucketUsername = z
  .string()
  .min(1)
  .regex(/^[-\w]+$/)

/** @typedef {string} TBitbucketUsername */

const BitbucketPassword = z
  .string()
  .min(1)
  .regex(/^[A-Za-z0-9]+$/)

/** @typedef {string} TBitbucketPassword */

module.exports = { BitbucketCredentials, BitbucketUsername, BitbucketPassword }
