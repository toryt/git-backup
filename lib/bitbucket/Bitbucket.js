/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Service } = require('../service/Service')
const { BitbucketAccount } = require('./BitbucketAccount')
const { BitbucketCredentials } = require('./BitbucketCredentials')

class Bitbucket extends Service {
  /**
   * {@inheritDoc}
   *
   * @type {TServiceGetAccountsResult}
   */
  getAccounts() {
    return this.credentialsCandidates.reduce(
      (acc, credentialsCandidate) => {
        const { success /*, data: parsedCredentials */ } = BitbucketCredentials.safeParse(credentialsCandidate)
        if (success) {
          const [username, password] = credentialsCandidate.split(':')
          this.desensifier.addSensitive(password)
          acc.accounts.push(new BitbucketAccount(this.runInfo, this.desensifier, username, password))
        } else {
          acc.wrongCredentials.push(credentialsCandidate)
        }
        return acc
      },
      /** @type {TServiceGetAccountsResult} */ { wrongCredentials: [], accounts: [] }
    )
  }
}

module.exports = { Bitbucket }
