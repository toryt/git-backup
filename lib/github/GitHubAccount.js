/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Account } = require('../account/Account')
const { name: programName } = require('../../package.json')
const { GitHubRepository } = require('./GitHubRepository')
const { adheresTo } = require('../pre')
const { GitHubCredentials } = require('./GitHubCredentials')
const { endAndDuration } = require('../runReport/endAndDuration')
const { Logger } = require('../Logger')
const { errorReport } = require('../runReport/ErrorReport')

/**
 * @typedef GitHubPageInfo
 * @property {string} endCursor
 * @property {boolean} hasNextPage
 */

/**
 * @typedef GitHubRepositories
 * @property {number} [totalCount]
 * @property {Array<GitHubRepositoryNode>} [nodes]
 * @property {GitHubPageInfo} [pageInfo]
 */

/**
 * @typedef GitHubOrganizationNode
 * @property {string} login
 * @property {GitHubRepositories} [repositories]
 */

/**
 * @typedef GitHubOrganizations
 * @property {number} [totalCount]
 * @property {Array<GitHubOrganizationNode>} [nodes]
 * @property {GitHubPageInfo} [pageInfo]
 */

/**
 * @typedef GitHubViewer
 * @property {string} login
 * @property {GitHubRepositories} [repositories]
 * @property {GitHubOrganizations} [organizations]
 */

/**
 * @typedef GitHubData
 * @property {GitHubViewer} viewer
 */

const apiUri = 'https://api.github.com/graphql'

/**
 * For pagination, see https://docs.github.com/en/graphql/guides/using-pagination-in-the-graphql-api.
 *
 * @param {(chunks: TemplateStringsArray, ...variables: unknown[]) => string} gql
 * @type {string}
 */
function createQuery(gql) {
  // noinspection GrazieInspection
  return gql`
    {
      viewer {
        login
        organizations(first: 100) {
          totalCount
          nodes {
            login
            repositories(first: 100) {
              totalCount
              nodes {
                id
                name
                owner {
                  login
                }
              }
              pageInfo {
                endCursor
                hasNextPage
              }
            }
          }
          pageInfo {
            endCursor
            hasNextPage
          }
        }
        repositories(first: 100) {
          totalCount
          nodes {
            id
            name
            owner {
              login
            }
          }
          pageInfo {
            endCursor
            hasNextPage
          }
        }
      }
    }
  `
}

/**
 * Gets a collection from GraphQL response safely.
 *
 * We expect there to be a `totalCount` and a `nodes` property, and for the property to exist, and be an array, but we
 * are defensive about that.
 *
 * @param {Logger} logger
 * @param {GitHubViewer} container
 * @return {Array<GitHubOrganizationNode>}
 */
function safeOrganizations(logger, container) {
  const organizations = container.organizations
  if (!organizations) {
    return []
  }

  if (organizations.pageInfo?.hasNextPage) {
    logger.warn(`There are more organizations for ${container.login}. Paging for more repositories is NYI.`)
  }
  return organizations.nodes ?? []
}

/**
 * Gets a collection from GraphQL response safely.
 *
 * We expect there to be a `totalCount` and a `nodes` property, and for the property to exist, and be an array, but we
 * are defensive about that.
 *
 * @param {Logger} logger
 * @param {GitHubViewer | GitHubOrganizationNode} container
 * @return {Array<GitHubRepositoryNode>}
 */
function safeRepositories(logger, container) {
  const repositories = container.repositories
  if (!repositories) {
    return []
  }

  if (repositories.pageInfo?.hasNextPage) {
    logger.warn(`There are more repositories for ${container.login}. Paging for more repositories is NYI.`)
  }
  return repositories.nodes ?? []
}

/**
 * @extends {Account<GitHubRepository>}
 */
class GitHubAccount extends Account {
  /** @type {Logger} */
  #logger

  /** @type {TGitHubCredentials} */
  #token

  /** @type {string | undefined} */
  #accountName

  /**
   * @param {RunInformation} runInfo
   * @param {Desensifier} desensifier
   * @param {TGitHubCredentials} gitHubToken - the GitHub token to be used for AuthNZ when connecting to the
   *                                                 GitHub API, and as password for git to be used over HTTPS
   */
  constructor(runInfo, desensifier, gitHubToken) {
    adheresTo(GitHubCredentials, gitHubToken)

    super(runInfo, desensifier)
    desensifier.addSensitive(gitHubToken)
    this.#logger = new Logger(
      () => (this.accountName ? `${this.token} (${this.accountName})` : this.token),
      desensifier
    )
    this.#token = gitHubToken
  }

  /** @type {Logger} */
  get logger() {
    return this.#logger
  }

  /**
   * The token to use when connecting to GitHub. This identifies the “viewer”, for which we will gather all the
   * repositories she has access to (directly, or via an organization). The token is also used as password for git to
   * access the repositories over HTTPS.
   *
   * @return {TGitHubCredentials}
   */
  get token() {
    return this.#token
  }

  /** @type {string | undefined} */
  get accountName() {
    return this.#accountName
  }

  // MUDO paging
  /**
   * Call the GitHub API to return information about the repositories the account has access to.
   *
   * _**MUDO:** The GitHub API uses paging, with a maximum of 100 results per page. Getting next pages is NIY. To test
   *            this, a GitHub account should be created with 100+ repos._
   *
   * @return {Promise<GitHubViewer>}
   */
  async getGitHubData() {
    // noinspection NpmUsedModulesInstalled — bogus warning: 'graphql-request' is installed
    const { gql, GraphQLClient } = await import('graphql-request')
    const client = new GraphQLClient(apiUri, {
      headers: {
        'User-Agent': programName,
        Authorization: `token ${this.token}`
      }
    })
    this.logger.trace(`Getting data from ${apiUri} …`)
    /** @type {GitHubData} */ const response = await client.request(createQuery(gql))
    this.logger.debug(`Response from ${apiUri} received.`)

    return response.viewer
  }

  // noinspection JSCheckFunctionSignatures
  /**
   * {@inheritDoc}
   *
   * _**MUDO:** The GitHub API uses paging, with a maximum of 100 results per page. Getting next pages is NIY. To test
   *            this, a GitHub account should be created with 100+ repos._
   *
   * @return {Promise<TAccountGetRepositoriesResult>}
   * @throws {TAccountGetRepositoriesFailure}
   */
  async getRepositories() {
    const start = new Date()
    /** @type {Omit<TAccountGetRepositoriesBase, 'success' | 'end' | 'duration'>} */
    const progress = {
      start: start.toISOString(),
      credentials: this.token
    }
    try {
      const gitHubResult = await this.getGitHubData()
      this.#accountName = gitHubResult.login
      const result = safeOrganizations(this.logger, gitHubResult).reduce(
        (acc, organization) => acc.concat(safeRepositories(this.logger, organization)),
        safeRepositories(this.logger, gitHubResult)
      )
      const repositories = result.map(
        gitHubRepoNode =>
          new GitHubRepository(this.runInfo, this.desensifier, gitHubRepoNode, gitHubResult.login, this.token)
      )
      this.logger.debug(`Found ${repositories.length} repositories.`)
      return {
        progressStep: {
          ...progress,
          name: gitHubResult.login,
          nrOfRepositoriesFound: repositories.length,
          success: true,
          ...endAndDuration(start, new Date())
        },
        repositories
      }
    } catch (err) {
      const error = errorReport(err)
      this.logger.warn(`Could not get repositories from ${apiUri}: "${error.message}".`)
      // noinspection UnnecessaryLocalVariableJS
      /** @type {TAccountGetRepositoriesFailure} */
      const failure = {
        ...progress,
        success: false,
        ...endAndDuration(start, new Date()),
        error
      }
      throw failure
    }
  }
}

module.exports = { GitHubAccount }
