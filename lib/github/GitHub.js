/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Service } = require('../service/Service')
const { GitHubAccount } = require('./GitHubAccount')
const { GitHubCredentials } = require('./GitHubCredentials')

class GitHub extends Service {
  /**
   * {@inheritDoc}
   *
   * @type {TServiceGetAccountsResult}
   */
  getAccounts() {
    return this.credentialsCandidates.reduce(
      (acc, token) => {
        const { success, data: parsedToken } = GitHubCredentials.safeParse(token)
        if (success) {
          acc.accounts.push(new GitHubAccount(this.runInfo, this.desensifier, parsedToken))
        } else {
          acc.wrongCredentials.push(token)
        }
        return acc
      },
      /** @type {TServiceGetAccountsResult} */ { wrongCredentials: [], accounts: [] }
    )
  }
}

module.exports = { GitHub }
