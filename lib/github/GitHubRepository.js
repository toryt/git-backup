/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Repository } = require('../repository/Repository')
const { isA, isTrimmedNonEmptyString } = require('../pre')

const gitFqdn = 'github.com'

/**
 * @typedef GitHubRepositoryOwner
 * @property {string} login
 */

/**
 * @typedef GitHubRepositoryNode
 * @property {string} id
 * @property {string} name
 * @property {GitHubRepositoryOwner} owner
 */

class GitHubRepository extends Repository {
  /** @type {string} */
  #username

  /** @type {string} */
  #token

  /**
   * @param {RunInformation} runInfo - Information about the current run and the {@link Service} under which this repository
   *                                   was found.
   * @param {Desensifier} desensifier
   * @param {GitHubRepositoryNode} gitHubRepo
   * @param {string} viewerLogin
   * @param {TGitHubCredentials} gitHubToken
   */
  constructor(runInfo, desensifier, gitHubRepo, viewerLogin, gitHubToken) {
    isA(Object, gitHubRepo)
    isTrimmedNonEmptyString(gitHubRepo.id)
    isTrimmedNonEmptyString(gitHubRepo.name)
    isA(Object, gitHubRepo.owner)
    isTrimmedNonEmptyString(gitHubRepo.owner.login)
    isTrimmedNonEmptyString(viewerLogin)
    isTrimmedNonEmptyString(gitHubToken)

    super(runInfo, desensifier, gitHubRepo.id, gitHubRepo.owner.login, gitHubRepo.name)
    desensifier.addSensitive(gitHubToken)
    this.#username = viewerLogin
    this.#token = gitHubToken
  }

  /**
   * @inheritDoc
   * @returns {string}
   */
  get remoteURL() {
    return `https://${this.#username}:${this.#token}@${gitFqdn}/${this.owner}/${this.name}.git`
  }
}

module.exports = { gitFqdn, GitHubRepository }
