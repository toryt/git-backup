/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { z } = require('zod')

// see https://github.com/colinhacks/zod?tab=readme-ov-file#json-type

/**
 * This should pass `NaN`, but Zod bars that.
 *
 * This is an extreme case: `NaN` would be stringified to `null` (as do the infinities, that do pass the schema)
 */
const JSONLiteral = z.union([z.string(), z.number(), z.boolean(), z.null()])

/**
 * @param {unknown} v
 * @param {Set<unknown>} [visited]
 * @returns {boolean}
 */
function isCircular(v, visited) {
  if (v === null || typeof v !== 'object') {
    return false
  }

  if (visited?.has(v)) {
    return true
  }

  const actualVisited = visited ?? new Set()
  actualVisited.add(v)

  if (Array.isArray(v)) {
    return v.some(av => isCircular(av, actualVisited))
  }

  return Object.values(v).some(ov => isCircular(ov, actualVisited))
}

const NotCircular = z.unknown().superRefine((val, ctx) => {
  if (isCircular(val, new Set())) {
    ctx.addIssue({
      code: z.ZodIssueCode.custom,
      message: 'values cannot be circular data structures',
      fatal: true
    })

    return z.NEVER
  }
})

/**
 * Note that infinities and NaN are stringified as `null`.
 *
 * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#description
 *
 * @typedef {string | number | boolean | null} TJSONLiteral
 */

const JSONValue = NotCircular.pipe(z.lazy(() => z.union([JSONLiteral, z.array(JSONValue), z.record(JSONValue)])))

/**
 * This does not take into accoun BigInt objects, which cannot be stringified.
 *
 * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#description
 *
 * @typedef {TJSONLiteral | Record<string, TJSONValue> | Array<TJSONValue>} TJSONValue
 */

module.exports = { JSONLiteral, isCircular, NotCircular, JSONValue }
