/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { sep } = require('node:path')

/**
 * Characters forbidden in Windows path segments.
 *
 * @type {string[]}
 */
const forbiddenWindowsChars = [
  '<',
  '>',
  ':', // (colon — sometimes works, but is actually NTFS Alternate Data Streams)'
  '"',
  '/',
  '\\',
  '|',
  '?',
  '*'
]

const allowedCharactersWithoutDot = `[^${forbiddenWindowsChars.map(c => (c === '\\' ? '\\\\' : c)).join('')}\\.\\p{C}]`

/**
 * A string that matches this expression is a valid path segment for this program.
 *
 * Path segments cannot contain invisible control characters and unused code points, {@link forbiddenWindowsChars} or
 * sequences of only dots, and cannot be empty.
 *
 * @type {string}
 */
const safePathSegmentString = `(${allowedCharactersWithoutDot}|\\.+${allowedCharactersWithoutDot}|${allowedCharactersWithoutDot}\\.+)+`

/**
 * {@link RegExp} that describes a restricted relative directory path with the given path separator.
 *
 * The path is not allowed to contain `.` or `..` directories, only going deep.
 *
 * The path is intended to be usable on all platforms, and therefor acceptable segments are more limited than on any
 * specific platform. E.g., `:` is not allowed in a segment for macOS and is difficult on Windows (which also rules out
 * the use of drive letters in Windows); `?`, `*` are confusing (glob) in any case; Windows further does not allow `<`,
 * `>`, `"`, `|`. See {@link safePathSegmentString}.
 *
 * @param {('/'|'\\')} separator
 * @return {string}
 */
function platformSpecificRelativeDirPathString(separator) {
  return `(${safePathSegmentString}\\${separator})*`
}

/**
 * {@link RegExp} that describes a platform-agnostic restricted relative file path.
 *
 * The path is not allowed to contain `.` or `..` directories, only going deep.
 *
 * The path is intended to be usable on all platforms, and therefor acceptable segments are more limited than on any
 * specific platform. E.g., `:` is not allowed in a segment for macOS and is difficult on Windows (which also rules out
 * the use of drive letters in Windows); `?`, `*` are confusing (glob) in any case; Windows further does not allow `< `,
 * `>`, `"`, `|`. The path separator used must be consistent in the path. See {@link safePathSegmentString} and
 * {@link platformSpecificRelativeDirPathString}.
 *
 * @type {RegExp}
 */
// NOTE: flag `v` supported in Node only from v20 upward
// NOTE: setting `g` (global) would mean that we need to reset lastIndex to 0 before every call to test
const relativePathRegExp = new RegExp(
  `^(${platformSpecificRelativeDirPathString('/')}|${platformSpecificRelativeDirPathString(
    '\\'
  )})${safePathSegmentString}$`,
  'ui'
)
const relativePathMessage =
  'Not a valid relative path. The path is not allowed to contain `.` or `..` directories, only going deep. ' +
  'The path is intended to be usable on all platforms. ' +
  'Path segments cannot contain invisible control characters and unused code points, characters forbidden ' +
  'in Windows paths, nor be sequences of only dots, and cannot be empty. ' +
  'The path separator used must be consistent in the path.'

/* istanbul ignore next (depends on the platform the tests are run on) */
const otherSep = new RegExp(sep === '/' ? '\\\\' : '/', 'g')

/**
 * Zod schema, that transforms a relative path, using `/` or `\` as path separator, to a relative path that uses the
 * platform specific path separator. This is intended to make moving the base directory between operating systems
 * possible.
 *
 * See the {@link relativePathMessage error message value} for details.
 */
const RelativePath = z
  .string()
  .regex(relativePathRegExp, {
    message: relativePathMessage
  })
  .transform(rp => rp.replace(otherSep, sep))

/**
 * @typedef {string} TRelativePath
 */

module.exports = {
  forbiddenWindowsChars,
  safePathSegmentString,
  platformSpecificRelativeDirPathString,
  relativePathRegExp,
  relativePathMessage,
  RelativePath
}
