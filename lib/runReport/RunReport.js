/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { ISODate } = require('./ISODate')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')
const { EnvironmentInfo } = require('./EnvironmentInfo')
const { ISODuration } = require('./ISODuration')
const { SafeNatural } = require('../SafeNatural')

/**
 * @public
 */
const RunReport = z.object({
  v: SafeNatural,
  environment: EnvironmentInfo,
  service: TrimmedNonEmptyString,
  start: ISODate,
  end: ISODate,
  duration: ISODuration,
  success: z.boolean()
})

/**
 * Run reports must be stringifiable (see {@link TJSONValue}).
 *
 * @typedef {Object} TRunReport
 * @public
 * @property {number} v
 * @property {TEnvironmentInfo} environment
 * @property {string} service
 * @property {TISODate} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {boolean} success
 */

module.exports = {
  RunReport
}
