/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { version } = require('../../package.json')
const {
  version: engineVersion,
  release: { name }
} = require('node:process')
const { platform, release, arch, machine, version: osVersion, hostname } = require('node:os')
const simpleGit = require('simple-git')
const z = require('zod')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')

/**
 * @public
 */
const EnvironmentInfo = z.object({
  version: TrimmedNonEmptyString,
  host: TrimmedNonEmptyString,
  engine: TrimmedNonEmptyString,
  os: TrimmedNonEmptyString,
  git: TrimmedNonEmptyString
})

/**
 * @typedef {object} TEnvironmentInfo
 * @public
 * @property {string} version
 * @property {string} host
 * @property {string} engine
 * @property {string} os
 * @property {string} git
 */

/**
 * @internal
 * @returns {Promise<TEnvironmentInfo>}
 */
async function environmentInfo() {
  const git = simpleGit()
  const { major, minor, patch } = await git.version()
  return {
    version,
    host: hostname(),
    engine: `${name} ${engineVersion}`,
    /* The reason there are so many entries here, with duplicate information on some environments, is to get some
       useful information on _every_ environment. On macOS, `osVersion()` contains everything we need, e.g., but on
       Synology, it contains hardly any information. Then again, on macOS `osVersion()` contains information not found
       in the other entries that is of interest. `uname -a` would in general be the most interesting, but that is

       a) not natively supported in Node, and
       b) not supported on Windows. */
    // IDEA ameliorate this
    os: `${platform()} ${release()} ${arch()} ${machine()} ${osVersion()}`,
    git: `${major}.${minor}.${patch}`
  }
}

module.exports = { EnvironmentInfo, environmentInfo }
