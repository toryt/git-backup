/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { ISODate } = require('./ISODate')
const { ErrorReport } = require('./ErrorReport')
const { ISODuration } = require('./ISODuration')

/**
 * @public
 */
const ProgressStep = z.object({
  start: ISODate,
  end: ISODate,
  duration: ISODuration,
  success: z.boolean().readonly()
})

/**
 * @typedef {Object} TProgressStep
 * @property {TISODate} start
 * @public
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {boolean} success
 */

/**
 * Mixin.
 *
 * This cannot extend {@link ProgressStep}, because when used as a mixin, it would reset the overridden properties (notably
 * `action`) to the general definition.
 *
 * @public
 */
const ProgressStepSuccess = z.object({
  success: z.literal(true)
})

/**
 * @typedef {Object} TProgressStepSuccess
 * @public
 * @property {string} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {true} success
 */

/**
 * Mixin.
 *
 * This cannot extend {@link ProgressStep}, because when used as a mixin, it would reset the overridden properties (notably
 * `action`) to the general definition.
 *
 * @public
 */
const ProgressStepFailure = z.object({
  success: z.literal(false),
  error: ErrorReport
})

/**
 * @typedef {Object} TProgressStepFailure
 * @public
 * @property {string} start
 * @property {TISODate} end
 * @property {TISODuration} duration
 * @property {false} success
 * @property {TErrorReport} error
 */

module.exports = { ProgressStep, ProgressStepSuccess, ProgressStepFailure }
