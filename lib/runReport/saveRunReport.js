/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { adheresTo, isA } = require('../pre')
const { join } = require('node:path')
const { writeFile } = require('node:fs/promises')
const { stringify } = require('yaml')
const { ensureDir } = require('fs-extra')
const { RunInformation } = require('../RunInformation')
const { errorReport } = require('../runReport/ErrorReport')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')
const { ISODate } = require('./ISODate')

/**
 * @public
 * @type {string}
 */
const successMark = 'OK'

/**
 * @public
 * @type {string}
 */
const failureMark = 'XX'

/**
 * {@link RunReport}s are saved in a directory structure, organised by year, month, and day of the
 * {@link RunInformation#runDate}. This returns the `YEAR/MONTH/DAY` relative path of that structure.
 *
 * @public
 * @param {RunInformation} runInfo
 * @returns {TRelativePath}
 */
function isoYMDPath(runInfo) {
  isA(RunInformation, runInfo)

  return join(
    runInfo.runDate.getUTCFullYear().toString(10).padStart(4, '0'),
    (runInfo.runDate.getUTCMonth() + 1).toString(10).padStart(2, '0'),
    runInfo.runDate.getUTCDate().toString(10).padStart(2, '0')
  )
}

/**
 * @public
 * @param {string} date
 * @returns {string}
 */
function makeISODatePathCompatible(date) {
  adheresTo(ISODate, date)

  return date.replace(/:/g, '-')
}

/**
 * File name of a {@link TRunReport}s that reports a successful run.
 *
 * @public
 * @param {RunInformation} runInfo
 * @param {string} [extra]
 * @returns {TRelativePath}
 */
function runReportSuccessFileName(runInfo, extra) {
  isA(RunInformation, runInfo)
  if (extra !== undefined) {
    adheresTo(TrimmedNonEmptyString, extra)
  }

  return `${makeISODatePathCompatible(runInfo.runDate.toISOString())}-${successMark}${extra ? `-${extra}` : ''}.yaml`
}

/**
 * File name of a {@link TRunReport}s that reports a failed run.
 *
 * @public
 * @param {RunInformation} runInfo
 * @param {string} [extra]
 * @returns {TRelativePath}
 */
function runReportFailureFileName(runInfo, extra) {
  isA(RunInformation, runInfo)
  if (extra !== undefined) {
    adheresTo(TrimmedNonEmptyString, extra)
  }

  return `${makeISODatePathCompatible(runInfo.runDate.toISOString())}-${failureMark}${extra ? `-${extra}` : ''}.yaml`
}

/**
 * @typedef {object} TSaveRunReportThis
 * @internal
 * @template {TRunReport} RunReportType
 * @property {Logger} logger
 * @property {Desensifier} desensifier
 * @property {RunInformation} runInfo
 * @property {TRelativePath} runReportDirectory
 * @property {import('zod').ZodTypeAny} runReportSchema
 * @property {TSaveRunReport<RunReportType>} saveRunReport
 */

/**
 * @callback TSaveRunReport
 * @internal
 * @template {TRunReport} RunReportType
 * @this {TSaveRunReportThis<RunReportType>}
 * @param {RunReportType} runReport
 * @param {string} [extra]
 * @returns {Promise<void>}
 * @throws {TUnsavedRunReportThrowable<RunReportType>}
 */

/**
 * @internal
 * @type{TSaveRunReport} TSaveRunReport
 */
async function saveRunReport(runReport, extra) {
  adheresTo(this.runReportSchema, runReport)

  if (extra !== undefined) {
    adheresTo(TrimmedNonEmptyString, extra)
  }

  this.logger.trace('Saving run report …')
  const runReportFileName = runReport.success
    ? runReportSuccessFileName(this.runInfo, extra)
    : runReportFailureFileName(this.runInfo, extra)
  const nonSensitiveRunReport = this.desensifier.desensify(runReport)
  const yaml = stringify(nonSensitiveRunReport)
  const runReportYMDDirectory = join(this.runReportDirectory, isoYMDPath(this.runInfo))
  const runReportPath = join(runReportYMDDirectory, runReportFileName)
  try {
    await ensureDir(this.runInfo.absolutePath(runReportYMDDirectory))
    await writeFile(this.runInfo.absolutePath(runReportPath), yaml, {
      encoding: 'utf8',
      flag: 'wx' /* do not overwrite */
    })
    this.logger.debug(`Run report saved at ${runReportPath}.`)
  } catch (/** @type {Error} */ err) {
    const error = errorReport(err)
    this.logger.error(`Could not save run report at ${runReportPath}: ${error.message}.`)
    // noinspection UnnecessaryLocalVariableJS
    const throwable = { runReport: nonSensitiveRunReport, error }
    throw throwable
  }
}

module.exports = {
  isoYMDPath,
  makeISODatePathCompatible,
  successMark,
  runReportSuccessFileName,
  failureMark,
  runReportFailureFileName,
  saveRunReport
}
