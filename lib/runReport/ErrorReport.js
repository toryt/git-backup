/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')
const { TrimmedNonEmptyString } = require('../TrimmedNonEmptyString')
const { JSONValue } = require('../JSONValue')
const { stackWithCauses, messageWithCauses } = require('pony-cause')

/**
 * @public
 */
const ErrorReport = z.object({
  message: TrimmedNonEmptyString,
  stack: z.string().optional(),
  details: JSONValue.optional()
})

/**
 * @typedef {object} TErrorReport
 * @public
 * @property {string} message - this is guaranteed not to have EOLs
 * @property {string} [stack]
 * @property {TJSONValue} [details]
 */

const endOfLines = /\r?\n/g

/**
 * Make any string a single-line string
 *
 * @internal
 * @param {unknown} str
 * @returns {string}
 */
function singleLineTrimmed(str) {
  return String(str).replace(endOfLines, ' ').trim()
}

/**
 * * replaces bigints with their string representation
 * * functions, symbols and `undefined`
 *   * become `undefined` in isolation
 *   * do not appear as properties in objects
 *   * are replaced with `null` in arrays
 * * infinities and NaN are replaced with `null`
 * * only enumerable own properties with a string name are taken into account for objects
 * * only the elements of arrays are taken into account, not their occasional properties
 * * circular array elements or object property values are replaced with the string `[Circular]`
 *
 * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#description
 *
 * @internal
 * @param {unknown} v
 * @param {boolean} [inArray]
 * @param {Set<Array<unknown> | Object>} [visited]
 * @returns {TJSONValue}
 */
function makeStringifiable(v, inArray, visited) {
  if (typeof v === 'bigint') {
    return v.toString()
  }

  if (v === undefined || typeof v === 'function' || typeof v === 'symbol') {
    return inArray ? null : undefined
  }

  if (v === null || v === Number.NEGATIVE_INFINITY || v === Number.POSITIVE_INFINITY || Number.isNaN(v)) {
    return null
  }

  if (typeof v !== 'object') {
    return v
  }

  const actualVisited = visited ?? new Set()

  if (actualVisited.has(v)) {
    return '[Circular]'
  }

  actualVisited.add(v)

  if (Array.isArray(v)) {
    return v.map(av => makeStringifiable(av, true, actualVisited))
  }

  return Object.entries(v).reduce((acc, [key, value]) => {
    const jsonValue = makeStringifiable(value, false, actualVisited)
    if (jsonValue !== undefined) {
      acc[key] = jsonValue
    }
    return acc
  }, {})
}

/**
 * Returns a version of an error for reporting.
 *
 * @public
 * @param {unknown} err
 * @return {TErrorReport}
 */
function errorReport(err) {
  if (typeof err !== 'object' || err === null) {
    return {
      message: singleLineTrimmed(err)
    }
  }

  const stack = stackWithCauses(err)

  const result = {
    message: singleLineTrimmed(messageWithCauses(err)),
    details: makeStringifiable(err)
  }

  if (stack !== '') {
    result.stack = stack
  }

  return result
}

module.exports = { ErrorReport, singleLineTrimmed, makeStringifiable, errorReport }
