/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { isA, isNonNegativeNumber } = require('../pre')
const { Temporal } = require('@js-temporal/polyfill')

/**
 * Convert a number of non-negative `ms` to the [ISO 8601 Durations](https://en.wikipedia.org/wiki/ISO_8601#Durations)
 * representation of hours, minutes, seconds, and a decimal fraction of seconds.
 *
 * The given `ms` is rounded to an integer value.
 *
 * @internal
 * @param {number} ms
 */
function asISODuration(ms) {
  isNonNegativeNumber(ms)

  return new Temporal.Duration(0, 0, 0, 0, 0, 0, 0, Math.round(ms))
    .round({ largestUnit: 'hour', smallestUnit: 'millisecond' })
    .toString()
}

/**
 * @typedef {object} TEndAndDuration
 * @internal
 * @property {TISODate} end
 * @property {TISODuration} duration
 */

/**
 * The given `end` as {@Link TISODate}, and the duration between `end` and the given `start` as `duration` in
 * {@link TISODuration} format.
 *
 * @internal
 * @param {Date} start
 * @param {Date} end
 * @returns {TEndAndDuration}
 */
function endAndDuration(start, end) {
  isA(Date, start)
  isA(Date, end)

  const durationMs = end.getTime() - start.getTime()
  return {
    end: end.toISOString(),
    duration: asISODuration(durationMs)
  }
}

module.exports = { asISODuration, endAndDuration }
