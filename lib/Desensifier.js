/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { isString, isNonEmptyString } = require('./pre')
const { createHash } = require('node:crypto')

const sensitiveMask = '!XXX!'
const hashLength = 10
const hashMark = '#'

/**
 * An agreed upon hash.
 *
 * @param {string} salt
 * @param {string | undefined} value
 * @returns {string}
 */
function hash(salt, value) {
  isNonEmptyString(salt)
  isString(value)

  const h = createHash('sha3-256')
    .update(salt + value)
    .digest('hex')

  return `${hashMark}${h.slice(0, hashLength)}${hashMark}`
}

class Desensifier {
  /**
   * @type {string | undefined}
   */
  #salt

  /**
   * @type {Array<string>}
   */
  #sensitives = []

  /**
   * @param {string} [salt]
   */
  constructor(salt) {
    isNonEmptyString(salt, true)

    this.#salt = salt
  }

  /**
   * @returns {string | undefined}
   */
  get salt() {
    return this.#salt
  }

  /**
   * @returns {Array<string>}
   */
  get sensitives() {
    return this.#sensitives.slice()
  }

  /**
   * @param {string} sensitive
   * @returns {void}
   */
  addSensitive(sensitive) {
    if (!this.#sensitives.includes(sensitive)) {
      this.#sensitives.unshift(sensitive)
    }
  }

  /**
   * This supports circular data structures. For {@link Array}, it only copies the array elements, not other
   * properties. For objects in general, it only copies own enumerable properties. {@link Function}s are copied by
   * reference. No deep copy is made of {@link Set}s, {@link WeakSet}s, {@link Map}s, {@link WeakMap}s and many other
   * object types. {@link Date} and {@link Error} instances are taken into account.
   *
   * @template {unknown} T
   * @parqm {T} v
   * @returns {T}
   */
  desensify(v) {
    /**
     * @param {unknown} v
     * @param {Map<Array<unknown> | Object, Array<unknown> | Object>} visited
     * @returns {unknown}
     */
    const desensifyImpl = (v, visited) => {
      if (typeof v === 'string') {
        return this.sensitives.reduce((acc, s) => acc.replaceAll(s, this.salt ? hash(this.salt, s) : sensitiveMask), v)
      }

      if (v === null || typeof v !== 'object') {
        return v
      }

      if (visited.has(v)) {
        return visited.get(v)
      }

      if (Array.isArray(v)) {
        const a = []
        visited.set(v, a)
        v.forEach((av, i) => {
          a[i] = desensifyImpl(av, visited)
        })
        return a
      }

      const o =
        v instanceof Date
          ? new Date(v)
          : v instanceof Error
            ? new Error(
                desensifyImpl(
                  v.message,
                  visited
                ) /* `visited` does not contain the Error yet, but we can assume `v.message` is a string, and there will
                      be no cicular data structures in the `message`. */
              )
            : {}
      if (v instanceof Error) {
        Object.defineProperty(o, 'stack', {
          configurable: true,
          enumerable: false,
          get: function () {
            // by the time this is called, `visited` will contain the Error
            return desensifyImpl(v.stack, visited)
          }
        })
      }
      visited.set(v, o)
      Object.entries(v).forEach(([key, value]) => {
        o[key] = desensifyImpl(value, visited)
      })
      return o
    }

    return desensifyImpl(v, new Map())
  }
}

module.exports = { sensitiveMask, hashLength, hashMark, hash, Desensifier }
