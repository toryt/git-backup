/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const yargs = require('yargs/yargs')
const { programName } = require('./programName')

const baseDirectoryOption = 'base-directory'
const credentialsOption = 'credentials'
const saltOption = 'salt'
const credentialsSeparatorOption = 'credentials-separator'
const logLevelOption = 'log-level'

// noinspection JSUnresolvedReference
/**
 * Use the exact code used by yargs to transform an option name to a propertyName
 */
const camelCase = yargs.Parser.camelCase

/**
 * Convert an CLI option into a property name.
 *
 * @type {((str: string) => string)}
 */
const propertyName = camelCase

/**
 * Environment variable names can, in general, only consist of ASCII letters, digits, and '_'.
 *
 * We convert anything that is not that to an underscore.
 *
 * By convention only uppercase letters are used.
 *
 * @type {string}
 * @returns {string}
 */
function environmentVariableFriendly(s) {
  return s.toUpperCase().replace(/[^A-Z0-9_]/g, '_')
}

const environmentVariablePrefix = environmentVariableFriendly(programName)

/**
 * We cannot ask `yargs` what the environment variables are it uses, because it doesn’t work like
 * that. It copies _all_ environment variables values it finds that start with the given prefix and
 * stores them in the parsed result as values of the `camelCase`-d environment name without the
 * prefix and 1 `'_'`. Therefor, we have to create the environment variable names ourselves with a
 * reverse reasoning if we want to document them.
 *
 * @type {string}
 * @returns {string}
 */
function environmentVariableName(argumentName) {
  return `${environmentVariablePrefix}_${environmentVariableFriendly(argumentName)}`
}

/**
 * @type {string}
 * @returns {string}
 */
function envAndConfigUsageAddition(argumentName) {
  return `(env: ${environmentVariableName(argumentName)}, config: \`${propertyName(argumentName)}: …\`)`
}

module.exports = {
  baseDirectoryOption,
  credentialsOption,
  saltOption,
  credentialsSeparatorOption,
  logLevelOption,
  propertyName,
  environmentVariablePrefix,
  environmentVariableName,
  envAndConfigUsageAddition
}
