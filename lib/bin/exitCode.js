/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { adheresTo } = require('../pre')
const { ServiceRunReport } = require('../service/ServiceRunReport')

/**
 * @param {TServiceRunReport} serviceRunReport
 * @param {boolean} couldSaveServiceRunReport
 * @returns {number}
 */
function exitCode(serviceRunReport, couldSaveServiceRunReport) {
  adheresTo(ServiceRunReport, serviceRunReport)

  let result = 0
  if (serviceRunReport.wrongCredentials.length > 0) {
    result |= 0b00000010 // set the second bit
  }
  if (serviceRunReport.nrOfAccountFailures > 0) {
    result |= 0b00000100 // set the third bit
  }
  if (serviceRunReport.repositoryRunReports.some(rrr => !rrr.success)) {
    result |= 0b00001000 // set the fourth bit
  }
  if (serviceRunReport.unsavedRepositoryRunReports.length > 0) {
    result |= 0b00010000 // set the fifth bit
  }
  if (!couldSaveServiceRunReport) {
    result |= 0b00100000 // set the sixth bit
  }

  return result
}

module.exports = {
  exitCode
}
