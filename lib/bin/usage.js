/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { longString } = require('./longString')
const { isTrimmedNonEmptyString } = require('../pre')
const { EOL } = require('node:os')
const { join } = require('node:path')
const { baseDirectoryOption, credentialsOption, saltOption } = require('./options')
const { defaultConfigSearchPlaces, configFileNameExtensions } = require('./defaultConfig')

/**
 * @internal
 * @param {TUsageConfig} usageConfig
 * @return {string}
 */
function usage({ humanServiceName, credentialsDescription, defaultConfigRootDir }) {
  isTrimmedNonEmptyString(humanServiceName)
  isTrimmedNonEmptyString(credentialsDescription)

  return (
    longString(`Usage: $0 --${baseDirectoryOption} <baseDirectory> (--${credentialsOption} <credentials>)+

Backup git repositories from ${humanServiceName} accounts described by one or more <credentials> to <baseDirectory>.

${credentialsDescription}

In the <baseDirectory> the following structure is created:`) +
    `

# <baseDirectory>
#   |
#   +--.git-backup
#   |  |
#   |  +--_runs
#   |  |  |
#   |  |  +--2024
#   |  |  |  |
#   |  |  |  +--04
#   |  |  |  |  |
#   |  |  |  |  +--07
#   |  |  |  |  |  |
#   |  |  |  |  |  +--2024-04-07T18'18'32.728Z-OK.yaml
#   |  |  |  |  |  +--2024-04-07T19'22'43.939Z-XX.yaml
#   |  |  |  |  |  +--2024-04-07T20'18'03.878Z-OK.yaml
#   |  |  |  |  |  +-- …
#   |  |  |  |  |
#   |  |  |  |  +-- …
#   |  |  |  |
#   |  |  |  +-- …
#   |  |  |
#   |  |  +-- …
#   |  |
#   |  +--xYz.git
#   |  |  |
#   |  |  +--.git-backup
#   |  |  |  |
#   |  |  |  +--2024
#   |  |  |  |  |
#   |  |  |  |  +--04
#   |  |  |  |  |  |
#   |  |  |  |  |  +--07
#   |  |  |  |  |  |  |
#   |  |  |  |  |  |  +--2024-04-07T18'18'32.728Z-OK-owner-Z_oldName.yaml
#   |  |  |  |  |  |  +--2024-04-07T19'22'43.939Z-XX-owner-Z_oldName.yaml
#   |  |  |  |  |  |  +--2024-04-07T20'18'03.878Z-OK-owner-A_myproject.yaml
#   |  |  |  |  |  |  +-- …
#   |  |  |  |  |  |
#   |  |  |  |  |  +-- …
#   |  |  |  |  |
#   |  |  |  |  +-- …
#   |  |  |  |
#   |  |  |  +-- …
#   |  |  |
#   |  |  +--hooks
#   |  |  +-- …
#   |  |  +--HEAD
#   |  |  +-- …
#   |  |
#   |  + …
#   |
#   +--owner-A
#   |  |
#   |  +--myproject.git --> ../.git-backup/xYz.git
#   |  +--  …
#   |
#   +--owner-Z
#   |  |
#   |  +--oldName.git --> ../.git-backup/xYz.git
#   |  +--  …
#   |
#   +--  …

` +
    longString(`For each repository found in the different accounts to which the given credentials give access, a
bare mirror clone is created or updated in the directory \`<baseDirectory>/.git-backup/<repositoryId>.git\`. Here,
<repositoryId> is the unchangeable id ${humanServiceName} has given to the repository (e.g., \`xYz\`).

Each run stores a repository run report for each mirror clone it creates or updates in
\`<baseDirectory>/.git-backup/<repositoryId>.git/.git-backup/<YYYY>/<MM>/<DD>/\`, where \`<YYYY>/<MM>/<DD>/\` are the
year, month, and day of the run in UTC. The file names show the time of the run in UTC, and whether the run was
successful (\`OK\` or \`XX\`). A repository run report is not saved when creation of a fresh mirror clone fails. For
this case, see the general run report below.

A symbolic link is maintained in \`<baseDirectory>/<owner>/<repository-name>.git\` for each of the processed
repositories (e.g., \`<baseDirectory>/owner-A/myproject.git\`). Note that ownership and the name of repositories hosted
at ${humanServiceName} can change, while the <repositoryId> does not. As a consequence, after several runs, it is
possible that more than 1 symbolic link exists referring to the same bare mirror clone. When the name is reused later,
previous symbolic links are overwritten. The intention of this structure is to be able to host the directory structure
under <baseDirectory> quickly with human-readable names.

Each run stores a complete run report in \`<baseDirectory>/_runs/<YYYY>/<MM>/<DD>/\`, where \`<YYYY>/<MM>/<DD>/\` are
the year, month, and day of the run in UTC. The file names show the time of the run in UTC, and whether the run was
successful (\`OK\` or \`XX\`). This run report contains repository run reports of failed creation attempts, if any.

When anything goes wrong, $0 continues to back up the repositories it can, reports on issues in the run reports or with
warnings in the console, and exits with a non-zero exit code that describes the type of the mishap.

Arguments can be given at the command line, via environment variables, or in a configuration file. Command line values
trump all other values, environment variable values trump values in a configuration file.

By default, the program looks for a configuration file at

`) +
    defaultConfigSearchPlaces.map(dsp => `- ${join(defaultConfigRootDir, dsp)}`).join(EOL) +
    longString(`.

When no extension is used, the config file can either be JSON or YAML.

If you are not happy with any of these default locations, you can specify the location of the config file to use with
the \`--config\` argument. A configuration file can be defined without an extension, or with the extensions
${configFileNameExtensions
  .filter(e => e !== '')
  .reduce((acc, e, i) => {
    const separator = i === 0 ? '' : i < configFileNameExtensions.length - 2 ? ', ' : ', or '
    return `${acc}${separator}'${e}'`
  }, '')}. When no extension is used, the config file can either be JSON or YAML.

When push comes to shuff, the <baseDirectory> or any of its parent directories might be exposed as \`git\` server over
\`ssh\`, \`HHTP\`, or any other protocol, and shared as a fallback for the original \`git\` service. Because the config
file might contain the sensitive credentials, the config file should never be placed in the <baseDirectory> or its
subdirectories, not in any of its parent directories. For this reason also, credentials are never included in the run
reports.

Furthermore, sensitive information is also never output in logs on the console.

Sensitive information is masked, or replace with a short hash if a \`--${saltOption}\` is given. With a salt, a
sensitive information instance is replaced with the same string within a run, and over different runs as long as the
same salt is given, in run reports and console logs. This makes it possible for users to discern between different
credentials in run reports and console logs, and at least see when credentials (or the salt) change over time.

Exposing a hash of the credentials exposes them to brute force attacks, but given the strength of the hash function and
the use of the salt, the risks are limited. The credentials should be rotated regularly in any case (e.g., once a year
or more often).

Since _all_ occurrences of a sensitive information string are replaced by a mask or a hash, they can be exposed when
they are an exact word that appears elsewhere in the output. E.g., if your credentials is 'is', the desensified version
of this sentence will read “if your credentials #da6e9e7ced# '#da6e9e7ced#'”. Since we know the first hash is the hash
of “is”, we know the credentials are 'is' too. Credentials given to this program must have enough entropy to avoid this.
When the credentials are generated app passwords or tokens we expect this is the case.`)
  )
}

module.exports = { usage }
