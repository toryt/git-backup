/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { EOL } = require('node:os')

/**
 * Takes a string with EOLs and transforms it into one with clear paragraphs.
 *
 * This is done to make it possible to enter long texts in JavaScript templates. The target is 1 line per paragraph,
 * separated by a blank line.
 * In the input, paragraphs are separated by a blank line (markdown style).
 *
 * @internal
 * @param {string} s
 * @return {string}
 */
function longString(s) {
  return s
    .replace(/(\r?\n){2,}/g, '<br />')
    .replace(/\r?\n/g, ' ')
    .replace(/<br \/>/g, EOL + EOL)
}

module.exports = { longString }
