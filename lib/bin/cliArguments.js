/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const { loggingLevels, getLoggingLevel } = require('../Logger')
const { usage } = require('./usage')
const { longString } = require('./longString')
const { resolve } = require('node:path')
// NOTE: DO NOT DESTRUCTURE — this form makes it possible to stub `os.homedir()`, which is not possible when destructured
const os = require('node:os')
const { cosmiconfigSync } = require('cosmiconfig')
const {
  baseDirectoryOption,
  envAndConfigUsageAddition,
  credentialsOption,
  credentialsSeparatorOption,
  logLevelOption,
  environmentVariablePrefix,
  saltOption
} = require('./options')
const { configFileNameExtensions, defaultConfigSearchPlaces } = require('./defaultConfig')

const defaultCredentialsSeparator = ';'

/**
 * Convert strings, numbers, and booleans to an absolute path.
 *
 * @internal
 * @param {string | number | boolean | undefined } arg
 * @returns {string | undefined}
 */
function argToAbsolutePath(arg) {
  return arg === undefined ? undefined : resolve(arg.toString())
}

/**
 * @internal
 * @typedef {object} TCLIArguments
 * @property {string} baseDirectoryPath
 * @property {Array<string>} credentials
 * @property {string | undefined} salt
 * @property {string} credentialsSeparator
 * @property {TLogLevel} logLevel
 */

/**
 * @internal
 * @param {TCLIConfig} cliConfig
 * @param {Array<string>} argv
 * @param {Record<string, unknown>} defaultConfig
 * @return {TCLIArguments}
 */
function cliArguments(cliConfig, defaultConfig, argv) {
  const args = yargs(hideBin(argv))
    .parserConfiguration({ 'camel-case-expansion': true, 'set-placeholder-key': true, 'strip-aliased': true })
    // IDEA: fix completion (completion does not work as expected: it still complains about required arguments)
    // .completion()
    .alias('version', 'v')
    .help('help')
    .alias('help', 'h')
    .option(baseDirectoryOption, {
      alias: 'd',
      describe: `Base directory ${envAndConfigUsageAddition(baseDirectoryOption)})`,
      type: 'string',
      demandOption: true,
      requiresArg: true
    })
    .option(credentialsOption, {
      alias: 'c',
      describe: `${cliConfig.shortCredentialsDescription} ${envAndConfigUsageAddition(credentialsOption)}`,
      type: 'string',
      array: true,
      demandOption: true,
      requiresArg: true
    })
    .option(credentialsSeparatorOption, {
      describe: longString(`Separator string for credentials. The only relevant use is when credentials are provided
via an environment variable. Yet, this *must* be set to a string that does not occur in the credentials passed in via
CLI arguments or a config file if the credentials contain the default separator '${defaultCredentialsSeparator}'.
${envAndConfigUsageAddition(credentialsSeparatorOption)}`),
      type: 'string',
      demandOption: false,
      default: defaultCredentialsSeparator,
      requiresArg: true
    })
    .option(saltOption, {
      alias: 's',
      describe: `Salt to use when hashing credentials in output. When not given, sensitive information is masked. ${envAndConfigUsageAddition(saltOption)}`,
      type: 'string',
      demandOption: false,
      requiresArg: true
    })
    .option(logLevelOption, {
      alias: 'l',
      describe: `log level ${envAndConfigUsageAddition(logLevelOption)}`,
      /* NOTE: The choices are shown in the usage output, and `exit(1)` is called when a wrong value or no value is
               given. */
      choices: loggingLevels.filter(ll => ll !== undefined),
      default: getLoggingLevel(),
      /* NOTE: `requiresArg` ensures we also `exit(1)` when the option is given without a value (otherwise we fall back
               on the default, although a message is shown). */
      requiresArg: true
    })
    .env(environmentVariablePrefix)
    .config(defaultConfig)
    .config(
      'config',
      longString(`Path to the configuration file that gives values for the <baseDirectory> and / or <credentials>, if
you are not happy with the default location.`),
      function (configPath) {
        const explorer = cosmiconfigSync('' /* not used */)
        const { config } = explorer.load(configPath)
        return config
      }
    )
    .usage(
      usage({
        ...cliConfig,
        baseDirectoryOption,
        credentialsOption,
        configFileNameExtensions,
        defaultConfigRootDir: os.homedir(),
        defaultConfigSearchPlaces
      })
    )
    .example([
      [
        longString(
          `$0 --${baseDirectoryOption} ~/git-backup/${cliConfig.humanServiceName}
--${credentialsOption} ${cliConfig.credentialsExample1} --${credentialsOption} ${cliConfig.credentialsExample2}`
        ),
        longString(
          `Backup git repositories found with the given credentials in the base directory
\`~/git-backup/${cliConfig.humanServiceName}\`.`
        )
      ]
    ])
    .epilog(`GPLv3. For more information, see <https://bitbucket.org/toryt/git-backup>.`)
    .strict()
    .parse()

  const result = {
    baseDirectoryPath: argToAbsolutePath(args[baseDirectoryOption]),
    credentials: args[credentialsOption]?.map(a => a.split(args[credentialsSeparatorOption])).flat(),
    credentialsSeparator: args[credentialsSeparatorOption],
    logLevel: args[logLevelOption]
  }

  if (args[saltOption] !== undefined && args[saltOption] !== '') {
    result.salt = args[saltOption]
  }

  return result
}

module.exports = {
  defaultCredentialsSeparator,
  argToAbsolutePath,
  cliArguments
}
