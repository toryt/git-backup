/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { format } = require('node:path')
// NOTE: DO NOT DESTRUCTURE — this form makes it possible to stub `os.homedir()`, which is not possible when destructured
const os = require('node:os')
const { cosmiconfigSync } = require('cosmiconfig')
const { programName } = require('./programName')

/* Using config files with `yargs` turns out to be rather complex.

   We want to load a config file from a default location, if it exists there, or from a location given with the
   `--config` argument. These are both exact locations, but it would be nice if the user has some freedom in the format
   and extension of the config file.

   - If no `--config` argument is given, the default location should be tried, but if there is no config file there,
     this is ok.
   - If the `--config` argument is given, the given location should be tried, but if there is no config file there, the
     program should exit with `1`.
   - If a config file is found, at whatever location, and parsing fails, the program should exit with `1`.
   - argument values should be used in order of precedence
     - command line argument values
     - environment variable values
     - values in the config file explicitly given in the `--config` argument
     - values in the default config file
   - The config file should _never_ be located in the base directory (or even a level higher in the intended usage),
     because it might contain credentials, and this directory might be shared via `ssh` when push comes to shuff.

   The `yargs` `config` option allows to set a name for the argument, a description, and a “parser”.

   However,

   - stating the default location is not supported with the `config()` method,
   - the “parser̦” function is not called if no value is given for the argument (and we would like to try the default
     location), and
   - using the `option()` method for the `config` argument does not seem to work

   Therefor, we need to

   - First try to load a config file from a default location, _before_ we parse the arguments, and
     - supply the result to `yargs` if there is one, and it can be parsed (using `config()`), and
     - exit with `1` ourselves if it cannot be parsed. (*)

       This means that we cannot show the `usage` in this case, because we did not yet prepare `yargs` for that.
       // IDEA there might be a workaround for this by preparing `yargs` in steps.

   - Let `yargs` do its work (using `config()`), during which, if the `--config` argument is given, try to load it from
     the given path, and
     - fail if it cannot be found
     - fail if it cannot be parsed
     - supply the result to `yargs` if there is one, and it can be parsed.

    Note that `config()` is used twice. */

/* NOTE: the preceding '.' is needed for Node 18, but not since Node 20 */
const configFileNameExtensions = [``, `.json`, `.yaml`, `.yml`, `.js`, `.cjs`]

const dir = '.config'

/**
 * @type {Array<string>}
 */
const defaultConfigSearchPlaces = configFileNameExtensions
  .map(ext => format({ name: `.${programName}`, ext }))
  .concat(configFileNameExtensions.map(ext => format({ dir, name: programName, ext })))

/**
 * @return {Record<string, string>}
 * @throws {error}
 */
function attemptToGetDefaultConfig() {
  const explorer = cosmiconfigSync(programName, {
    searchStrategy: 'none', // do not search in higher directories
    searchPlaces: defaultConfigSearchPlaces,
    ignoreEmptySearchPlaces: false
  })
  try {
    /* NOTE: DO NOT DESTRUCTURE `os`, do not put the config root directory in a constant
      This form makes it possible to stub `os.homedir()`, which is not possible when destructured */
    const { config } = explorer.search(os.homedir()) ?? { config: {} }
    return config
  } catch (err) {
    console.error(err.message)
    process.exit(1)
  }
}

module.exports = {
  configFileNameExtensions,
  defaultConfigSearchPlaces,
  attemptToGetDefaultConfig
}
