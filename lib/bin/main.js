/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { exitCode } = require('./exitCode')
const { setLoggingLevel, loggingLevels } = require('../Logger')
const { cliArguments } = require('./cliArguments')
const { attemptToGetDefaultConfig } = require('./defaultConfig')

/**
 * Never throws
 *
 * @internal
 * @param {Service} service
 * @returns {Promise<number>}
 */
async function run(service) {
  try {
    const runReport = await service.backup()
    return exitCode(runReport, true)
  } catch (err) {
    console.error('Could not save run report:')
    console.dir(err, { depth: 999 })
    return exitCode(err.runReport, false)
  }
}

/**
 * @callback TServiceFactory
 * @public
 * @param {string} baseDirectory - Absolute path of the directory this program works in.
 * @param {Date} runDate - The time of the start of program execution. Used as unique identification of the run.
 * @param {Array<string>} credentialsCandidates
 * @param {string | undefined} salt
 * @returns {Service}
 */

/**
 * @public
 * @param {TCLIConfig} cliConfig
 * @param {TServiceFactory} serviceFactory
 * @param {Array<string>} argv
 * @return {Promise<void>}
 */
async function main(cliConfig, serviceFactory, argv) {
  /**
   * @type {TCLIArguments}
   */
  const args = cliArguments(cliConfig, attemptToGetDefaultConfig(), argv)

  if (loggingLevels.includes(args.logLevel)) {
    /* `yargs` will have called `process.exit` if this is the case before we reach here. But we subdue that in tests.
       This if makes it possible to test this behavior with a wrong log level. */
    setLoggingLevel(args.logLevel)
  }

  const service = serviceFactory(args.baseDirectoryPath, new Date(), args.credentials, args.salt)

  const exitCode = await run(service)

  /* istanbul ignore if */
  if (exitCode !== 0) {
    process.exit(exitCode)
  }
}

module.exports = { run, main }
