/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { Desensifier, hash, hashMark, sensitiveMask, hashLength } = require('../lib/Desensifier')
const { stuffWithUndefined } = require('./_util/stuff')
const { inspect } = require('node:util')
const should = require('should')
const { saltExample } = require('./80.bin/constants')
const testName = require('./testName')(module)

const sensitiveBase = 'Sensitive'
const sensitive1 = `${sensitiveBase} 1`
const sensitive2 = `${sensitiveBase} 2`
const sensitive3 = `${sensitiveBase} 3`

describe(testName, function () {
  describe('hash', function () {
    const testStrings = [
      '',
      'hello world',
      'user@example.com',
      '12345',
      '@# $% _ + ~ !',
      'ĀāĂăĄąĆćĈĉĊċČčĐđĎďĐ',
      'a'.repeat(10000),
      ' hello world ',
      'hello\tworld',
      'hello\nworld'
    ]

    testStrings.forEach(s => {
      const displayString = s.length > 40 ? `${s.substring(0, 40)}...` : s

      describe(`hash '${displayString}'`, function () {
        it(`works with '${displayString}'`, function () {
          const result = hash(saltExample, s)
          console.log(result)

          result.should.be.a.String()
          result.should.not.be.empty()
        })
        it(`starts and ends with '${hashMark}' for '${displayString}'`, function () {
          const result = hash(saltExample, s)

          result.should.startWith(hashMark)
          result.should.endWith(hashMark)
        })
        it(`is 10 characters long for '${displayString}'`, function () {
          const result = hash(saltExample, s)

          result.replaceAll(hashMark, '').length.should.equal(hashLength)
        })
        it(`returns always returns the same hash for '${displayString}'`, function () {
          const result1 = hash(saltExample, s)
          const result2 = hash(saltExample, s)

          result1.should.equal(result2)
        })
      })
    })
  })

  describe('Desensifier', function () {
    /**
     * @param {Desensifier} subject
     * @param {string | undefined} salt
     */
    function shouldAdhereToDesensifierConstructorPostConditions(subject, salt) {
      subject.should.be.an.instanceof(Desensifier)

      // salt
      should(subject.salt).equal(salt)

      // sensitives
      subject.sensitives.should.be.an.Array()
      subject.sensitives.should.be.empty()
      const sensitives1 = subject.sensitives
      const sensitives2 = subject.sensitives
      // noinspection JSUnresolvedReference
      sensitives2.push('not a sensitive')
      subject.sensitives.should.deepEqual(sensitives1)
    }

    describe('constructor', function () {
      it('creates an object that adheres to the invariants without a salt', function () {
        const subject = new Desensifier()

        shouldAdhereToDesensifierConstructorPostConditions(subject, undefined)
      })

      it('creates an object that adheres to the invariants with a salt', function () {
        const subject = new Desensifier(saltExample)

        shouldAdhereToDesensifierConstructorPostConditions(subject, saltExample)
      })
    })

    describe('instance', function () {
      describe('methods', function () {
        beforeEach(function () {
          this.subject = new Desensifier()
        })

        describe('#addSensitive', function () {
          it('remembers added sensitives', function () {
            this.subject.addSensitive(sensitive1)

            this.subject.sensitives.should.be.an.Array()
            this.subject.sensitives.length.should.equal(1)
            this.subject.sensitives.should.containEql(sensitive1)

            this.subject.addSensitive(sensitive2)

            this.subject.sensitives.should.be.an.Array()
            this.subject.sensitives.length.should.equal(2)
            this.subject.sensitives.should.containEql(sensitive1)
            this.subject.sensitives.should.containEql(sensitive2)

            this.subject.addSensitive(sensitive3)

            this.subject.sensitives.should.be.an.Array()
            this.subject.sensitives.length.should.equal(3)
            this.subject.sensitives.should.containEql(sensitive1)
            this.subject.sensitives.should.containEql(sensitive2)
            this.subject.sensitives.should.containEql(sensitive3)
          })

          it('should quietly absorb duplicates', function () {
            this.subject.addSensitive(sensitive1)
            this.subject.addSensitive(sensitive2)
            this.subject.addSensitive(sensitive3)

            this.subject.sensitives.should.be.an.Array()
            this.subject.sensitives.length.should.equal(3)
            this.subject.sensitives.should.containEql(sensitive1)
            this.subject.sensitives.should.containEql(sensitive2)
            this.subject.sensitives.should.containEql(sensitive3)

            this.subject.addSensitive(sensitive1)
            this.subject.addSensitive(sensitive2)
            this.subject.addSensitive(sensitive3)
            this.subject.sensitives.should.be.an.Array()
            this.subject.sensitives.length.should.equal(3)
            this.subject.sensitives.should.containEql(sensitive1)
            this.subject.sensitives.should.containEql(sensitive2)
            this.subject.sensitives.should.containEql(sensitive3)
          })
        })

        describe('#desensify', function () {
          function generateTests(salt) {
            beforeEach(function () {
              this.subject = new Desensifier(salt)
              this.subject.addSensitive(sensitive1)
              this.subject.addSensitive(sensitive2)
              this.subject.addSensitive(sensitive3)
            })

            describe('non-sensitive stuff', function () {
              stuffWithUndefined.forEach(s => {
                it(`desensifies ${inspect(s)}`, function () {
                  const result = this.subject.desensify(s)

                  should(result).deepEqual(s)
                })
              })
            })

            describe('string', function () {
              it(`desensitifies a string that contains a sensitive with a ${salt ? 'hash' : 'mask'}`, function () {
                const part1 = 'this contains '
                const part2 = ' sensitive data'
                const str = `${part1}${sensitive2}${part2}`

                const result = this.subject.desensify(str)
                console.log(result)

                result.should.be.a.String()
                result.should.startWith(part1)
                result.should.endWith(part2)
                result.should.not.containEql(sensitive1)
                result.should.not.containEql(sensitive2)
                result.should.not.containEql(sensitive3)
                result.should.containEql(salt ? hashMark : sensitiveMask)
              })

              it(`desensitifies a string that contains a multiple sensitives with a ${salt ? 'hash' : 'mask'}`, function () {
                const part1 = 'this contains '
                const part2 = ' and '
                const part3 = ' sensitive data strings'
                const str = `${part1}${sensitive2}${part2}${sensitive1}${sensitive3}${part3}`

                const result = this.subject.desensify(str)
                console.log(result)

                result.should.be.a.String()
                result.should.startWith(part1)
                result.should.containEql(part2)
                result.should.endWith(part3)
                result.should.not.containEql(sensitive1)
                result.should.not.containEql(sensitive2)
                result.should.not.containEql(sensitive3)
                result.should.containEql(salt ? hashMark : sensitiveMask)
              })

              it('should return the same result every time', function () {
                const part1 = 'this contains '
                const part2 = ' and '
                const part3 = ' sensitive data strings'
                const str = `${part1}${sensitive2}${sensitive2}${part2}${sensitive1}${sensitive3}${sensitive2}${part3}`

                const result1 = this.subject.desensify(str)
                console.log(result1)

                const result2 = this.subject.desensify(str)
                result2.should.equal(result1)

                const result3 = this.subject.desensify(str)
                result3.should.equal(result1)
              })

              it('a later sensitive addition supersedes a previous one', function () {
                this.subject.addSensitive(sensitiveBase)

                const part1 = 'this contains '
                const part2 = ' and '
                const part3 = ' sensitive data strings'
                const str = `${part1}${sensitive2}${sensitive2}${part2}${sensitive1}${sensitive3}${sensitive2}${part3}`

                const result = this.subject.desensify(str)
                console.log(result)

                result.should.be.a.String()
                result.should.startWith(part1)
                result.should.containEql(part2)
                result.should.endWith(part3)
                result.should.not.containEql(sensitiveBase)
                result.should.containEql(' 1')
                result.should.containEql(' 2')
                result.should.containEql(' 3')
              })
            })

            describe('nested', function () {
              /**
               * @param {unknown} v
               * @param {Set<Array<unknown> | Object>} visited
               * @return {void}
               */
              function shouldSensitives(v, visited) {
                if (typeof v === 'string') {
                  v.should.not.containEql(sensitive1)
                  v.should.not.containEql(sensitive2)
                  v.should.not.containEql(sensitive3)
                }

                if (v === null || typeof v !== 'object' || visited.has(v)) {
                  return
                }

                visited.add(v)

                const iterable = Array.isArray(v) ? v : Object.values(v)
                iterable.forEach(item => {
                  shouldSensitives(item, visited)
                })
              }

              it('desensifies a nested structure', function () {
                const error = new Error(sensitive3)
                /** @type {Array<unknown>} */
                const c = [4, { d: { e: `e${sensitive1}e`, error }, f: `${sensitive2}f` }, `2${sensitive3}`]
                const v = {
                  a: 'a',
                  b: stuffWithUndefined,
                  c,
                  g: sensitive1
                }
                c.push(v)

                const result = this.subject.desensify(v)

                console.dir(result, { depth: 999 })

                shouldSensitives(result, new Set())
              })
            })
          }

          describe('without a salt', function () {
            generateTests(undefined)
          })

          describe('with a salt', function () {
            generateTests(saltExample)
          })
        })
      })
    })
  })
})
