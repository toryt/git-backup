/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const z = require('zod')

const { forbiddenWindowsChars, safePathSegmentString } = require('../lib/RelativePath')

/**
 * A {@link RegExp} that matches this expression is a valid path segment for this program.
 *
 * Path segments cannot contain invisible control characters and unused code points, {@link forbiddenWindowsChars} or
 * sequences of only dots, and cannot be empty.
 *
 * @type {RegExp}
 */
// NOTE: flag `v` supported in Node only from v20 upward
// NOTE: setting `g` (global) would mean that we need to reset lastIndex to 0 before every call to test
const safePathSegmentRegExp = new RegExp(`^${safePathSegmentString}$`, 'ui')

const safePathSegmentMessage =
  'Not a valid path segment. The path segment is intended to be usable on all platforms. Path segments cannot ' +
  'contain invisible control characters and unused code points, characters forbidden in Windows paths, nor be ' +
  'sequences of only dots, cannot be empty, and cannot contain path separators used in any supported OS.'

const SafePathSegment = z.string().regex(safePathSegmentRegExp, {
  message: safePathSegmentMessage
})

const safePathSegmentExamples = [
  'a-Path_segment.with.3.dots',
  'a',
  'abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789.',
  'contains a space or 4',
  '...dot..dot...',
  'what🤷ever',
  '𠮷𠮾',
  '🇺🇳'
]

const controlChars = ['\t', '\r', '\n', '\uFFFF']

/**
 * @type {Array<string>}
 */
const unsafePathSegmentStringExamples = ['', `contains-a/slash`, `contains-a\\back-slash`, '.', '..', '...........']
  .concat(forbiddenWindowsChars.map(c => `pre${c}post`))
  .concat(controlChars.map(cc => `pre${cc}post`))

module.exports = {
  safePathSegmentRegExp,
  safePathSegmentMessage,
  SafePathSegment,
  safePathSegmentExamples,
  unsafePathSegmentStringExamples
}
