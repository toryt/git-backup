/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { GitHubAccount } = require('../../lib/github/GitHubAccount')
const sinon = require('sinon')
const { shouldAdhereToAccountConstructorPostConditions } = require('../50.account/AccountCommon')
const { runInfo } = require('../constants')
const { AccountGetRepositoriesResult } = require('../../lib/account/AccountGetRepositoriesResult')
const { AccountGetRepositoriesFailure } = require('../../lib/account/AccountGetRepositoriesProgressStep')
const should = require('should')
const { GitHubCredentials } = require('../../lib/github/GitHubCredentials')
const { Logger } = require('../../lib/Logger')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { errorReport } = require('../../lib/runReport/ErrorReport')
const { Desensifier } = require('../../lib/Desensifier')

/**
 * @typedef {Object} GitHubAccountFixture
 * @property {GitHubAccount} [subject]
 * @property {SinonStub<[], Promise<GitHubViewer>>} [getGitHubDataStub]
 */

describe(testName, function () {
  /** @type {string} */
  const token = 'a token'

  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function (/** @this {GitHubAccountFixture} */) {
      const desensifier = new Desensifier()
      const subject = new GitHubAccount(runInfo, desensifier, token)

      shouldAdhereToAccountConstructorPostConditions(subject, runInfo, desensifier)
      subject.should.be.an.instanceof(GitHubAccount)

      // token
      GitHubCredentials.parse(token)
      subject.token.should.equal(token)

      // accountName
      should(subject.accountName).be.undefined()

      // logger
      subject.logger.should.be.an.instanceof(Logger)
      subject.logger.identifier.should.be.a.Function()
      subject.logger.identifier().should.equal(subject.token)
      subject.logger.desensifier.should.equal(subject.desensifier)
    })
  })

  describe('methods', function () {
    beforeEach(function (/** @this {GitHubAccountFixture} */) {
      this.subject = new GitHubAccount(runInfo, new Desensifier(), token)
      this.getGitHubDataStub = sinon.stub(this.subject, 'getGitHubData')
    })

    afterEach(function (/** @this {GitHubAccountFixture} */) {
      this.getGitHubDataStub.restore()
    })

    const viewerLogin = 'viewer-login'
    const organizationLogin1 = 'organization-login-1'
    const organizationLogin2 = 'organization-login-2'

    describe('#getRepositories', function () {
      /**
       * @param {string} title
       * @param {GitHubViewer} stubResult
       * @returns {void}
       */
      function testEmpty(title, stubResult) {
        it(`works without ${title}`, async function (/** @this {GitHubAccountFixture} */) {
          this.getGitHubDataStub.resolves(stubResult)

          /** @type {TAccountGetRepositoriesResult} */
          const result = await this.subject.getRepositories()

          AccountGetRepositoriesResult.parse(result)
          result.progressStep.name.should.equal(viewerLogin)
          result.progressStep.credentials.should.equal(token)
          result.progressStep.nrOfRepositoriesFound.should.equal(0)
          result.progressStep.should.not.have.property('error')

          result.repositories.should.be.empty()
          shouldBeStringifiable(result)

          this.subject.logger.identifier().should.containEql(viewerLogin)
        })
      }

      /**
       * @type {GitHubPageInfo}
       */
      const pageInfoLast = {
        endCursor: 'end cursor',
        hasNextPage: false
      }

      testEmpty('organizations or repositories', {
        login: viewerLogin,
        organizations: { totalCount: 0, nodes: [], pageInfo: pageInfoLast },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('organizations totalCount', {
        login: viewerLogin,
        organizations: { nodes: [], pageInfo: pageInfoLast },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('organizations nodes', {
        login: viewerLogin,
        organizations: { totalCount: 0, pageInfo: pageInfoLast },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('organizations properties', {
        login: viewerLogin,
        organizations: {},
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('organizations', {
        login: viewerLogin,
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('repositories totalCount', {
        login: viewerLogin,
        organizations: { totalCount: 0, nodes: [], pageInfo: pageInfoLast },
        repositories: { nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('repositories nodes', {
        login: viewerLogin,
        organizations: { totalCount: 0, nodes: [], pageInfo: pageInfoLast },
        repositories: { totalCount: 0, pageInfo: pageInfoLast }
      })
      testEmpty('repositories properties', {
        login: viewerLogin,
        organizations: { totalCount: 0, nodes: [], pageInfo: pageInfoLast },
        repositories: {}
      })
      testEmpty('repositories', {
        login: viewerLogin,
        organizations: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('anything', {
        login: viewerLogin
      })
      testEmpty('repositories for 1 organization', {
        login: viewerLogin,
        organizations: {
          totalCount: 0,
          nodes: [{ login: organizationLogin1, repositories: { totalCount: 0, nodes: [] } }],
          pageInfo: pageInfoLast
        },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('repositories totalCount for 1 organization', {
        login: viewerLogin,
        organizations: {
          totalCount: 0,
          nodes: [{ login: organizationLogin1, repositories: { nodes: [] } }],
          pageInfo: pageInfoLast
        },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('repositories nodes for 1 organization', {
        login: viewerLogin,
        organizations: {
          totalCount: 0,
          nodes: [{ login: organizationLogin1, repositories: { totalCount: 0 } }],
          pageInfo: pageInfoLast
        },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('repositories properties for 1 organization', {
        login: viewerLogin,
        organizations: {
          totalCount: 0,
          nodes: [{ login: organizationLogin1, repositories: {} }],
          pageInfo: pageInfoLast
        },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })
      testEmpty('repositories property for 1 organization', {
        login: viewerLogin,
        organizations: {
          totalCount: 0,
          nodes: [{ login: organizationLogin1 }],
          pageInfo: pageInfoLast
        },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })

      /**
       * @param {string} title
       * @param {GitHubViewer} stubResult
       * @returns {void}
       */
      function testWithMore(title, stubResult) {
        it(`works with more ${title}`, async function (/** @this {GitHubAccountFixture} */) {
          this.getGitHubDataStub.resolves(stubResult)
          /** @type {TAccountGetRepositoriesResult} */
          const result = await this.subject.getRepositories()

          AccountGetRepositoriesResult.parse(result)
          result.progressStep.name.should.equal(viewerLogin)
          result.progressStep.credentials.should.equal(token)
          result.progressStep.nrOfRepositoriesFound.should.equal(0)
          result.progressStep.should.not.have.property('error')

          result.repositories.should.be.empty()
          shouldBeStringifiable(result)

          this.subject.logger.identifier().should.containEql(viewerLogin)
        })
      }

      /**
       * @type {GitHubPageInfo}
       */
      const pageInfoMore = {
        endCursor: 'end cursor',
        hasNextPage: true
      }

      testWithMore('organizations', {
        login: viewerLogin,
        organizations: {
          totalCount: 1,
          nodes: [{ login: organizationLogin1, repositories: { totalCount: 0, nodes: [] } }],
          pageInfo: pageInfoMore
        },
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoLast }
      })

      testWithMore('organizations', {
        login: viewerLogin,
        organizations: {
          totalCount: 0,
          nodes: [],
          pageInfo: pageInfoLast
        },
        // essentially incorrect: there is nothing, but there is more
        repositories: { totalCount: 0, nodes: [], pageInfo: pageInfoMore }
      })

      it('contains all repositories of the viewer, and of all organizations', async function (/** @this {GitHubAccountFixture} */) {
        const commonId = '9de31ce9-725f-4cad-a7ad-6959eb9fcd2f'
        const repo1 = { id: '32fcf3a9-2db8-4300-89a2-324cbfbc84e0', name: 'repo1', owner: { login: viewerLogin } }
        const repo2 = { id: commonId, name: 'repo2', owner: { login: viewerLogin } }
        const repo3 = {
          id: '589834ad-1e8a-4c80-9e2d-e450d7e47f42',
          name: 'repo3',
          owner: { login: organizationLogin1 }
        }
        const repo4 = {
          id: '2b445e86-6467-4753-937f-27bad05ca86c',
          name: 'repo4',
          owner: { login: organizationLogin2 }
        }
        const repo5 = {
          id: commonId,
          name: 'repo5',
          owner: { login: organizationLogin2 }
        }
        const organization1 = {
          login: organizationLogin1,
          repositories: { totalCount: 1, nodes: [repo3], pageInfo: pageInfoLast }
        }
        const organization2 = {
          login: organizationLogin2,
          repositories: { totalCount: 2, nodes: [repo4, repo5], pageInfo: pageInfoLast }
        }
        const repositories = [repo1, repo2, repo3, repo4, repo5]
        this.getGitHubDataStub.resolves({
          login: viewerLogin,
          organizations: { totalCount: 2, nodes: [organization1, organization2], pageInfo: pageInfoLast },
          repositories: { totalCount: 2, nodes: [repo1, repo2], pageInfo: pageInfoLast }
        })
        /** @type {TAccountGetRepositoriesResult} */
        const result = await this.subject.getRepositories()

        AccountGetRepositoriesResult.parse(result)
        result.progressStep.name.should.equal(viewerLogin)
        result.progressStep.credentials.should.equal(token)
        result.progressStep.nrOfRepositoriesFound.should.equal(5)
        result.progressStep.should.not.have.property('error')

        result.repositories.should.be.an.Array()
        result.repositories.length.should.equal(5)

        const expectedURIUserInfo = `${viewerLogin}:${token}`
        result.repositories.forEach(repository => {
          repository.remoteURL.should.containEql(expectedURIUserInfo)
          repository.runInfo.should.equal(runInfo)
          repository.mirrorClonePath.should.startWith(runInfo.backupDirectory)
          repositories.some(({ id }) => repository.id === id).should.be.true()
          repositories
            .some(({ owner, name }) => repository.owner === owner.login && repository.name === name)
            .should.be.true()
        })
        result.repositories.filter(repository => repository.id === commonId).length.should.equal(2)

        shouldBeStringifiable(result)

        this.subject.logger.identifier().should.containEql(viewerLogin)
      })

      it('propagates failures', async function (/** @this {GitHubAccountFixture} */) {
        const error = new Error('GitHub data error')
        this.getGitHubDataStub.rejects(error)

        /** @type {TAccountGetRepositoriesFailure} */
        const thrown = await this.subject.getRepositories().should.be.rejected()
        AccountGetRepositoriesFailure.parse(thrown)
        should(thrown.name).be.undefined()
        thrown.credentials.should.equal(token)
        thrown.error.should.deepEqual(errorReport(error))

        shouldBeStringifiable(thrown)
      })
    })
  })
})
