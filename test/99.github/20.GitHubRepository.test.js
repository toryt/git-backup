/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { gitFqdn, GitHubRepository } = require('../../lib/github/GitHubRepository')
const { shouldAdhereToRepositoryConstructorPostConditions } = require('../40.repository/RepositoryCommon')
const { GitURL } = require('../../lib/repository/GitURL')
const { runInfo } = require('../constants')
const { Desensifier } = require('../../lib/Desensifier')

describe(testName, function () {
  describe('gitFqdn', function () {
    it('is a string', function () {
      gitFqdn.should.be.a.String()
      gitFqdn.should.not.be.empty()
    })
  })

  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function () {
      /** @type {string} */
      const ownerLogin = 'the owner login'

      /** @type {string} */
      const localId = 'ebccac28-cc73-4798-bfb2-707af6d37518'

      /** @type {string} */
      const name = 'the-name-of-the-repo'

      /** @type {GitHubRepositoryNode} */
      const aGitHubRepo = { id: localId, name, owner: { login: ownerLogin } }

      /** @type {string} */
      const viewerLogin = 'the viewer login'

      /** @type {string} */
      const token = 'a token'

      const desensifier = new Desensifier()
      const subject = new GitHubRepository(runInfo, desensifier, aGitHubRepo, viewerLogin, token)

      shouldAdhereToRepositoryConstructorPostConditions(subject, runInfo, desensifier, localId, ownerLogin, name)

      subject.should.be.an.instanceof(GitHubRepository)

      // remoteURL
      GitURL.parse(subject.remoteURL)
      subject.remoteURL.should.equal(`https://${viewerLogin}:${token}@${gitFqdn}/${ownerLogin}/${name}.git`)
      console.log('remoteURL:', subject.remoteURL)
    })
  })
})
