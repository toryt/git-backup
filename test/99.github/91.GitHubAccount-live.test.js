/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { GitHubAccount } = require('../../lib/github/GitHubAccount')
const { AccountGetRepositoriesResult } = require('../../lib/account/AccountGetRepositoriesResult')
const { getGitHubTokens } = require('./getGitHubTokens')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { runInfo } = require('../constants')
const { Desensifier } = require('../../lib/Desensifier')
const testName = require('../testName')(module)

describe(testName, function () {
  /**
   * @typedef {Object} GitHubAccountFixture
   * @property {GitHub} [github]
   * @property {string} [token]
   * @property {Desensifier} [desensifier]
   * @property {GitHubAccount} [account]
   * @property {SinonStub<[], Promise<GitHubViewer>>} [getGitHubDataStub]
   */

  before(function (/** @this {GitHubAccountFixture} */) {
    /* NOTE: BE AWARE — also when tests fail, these tokens should not appear in output */
    const tokens = getGitHubTokens()
    this.desensifier = new Desensifier()
    tokens.forEach(t => {
      this.desensifier.addSensitive(t)
    })
    this.token = tokens[0]
  })

  beforeEach(function (/** @this {GitHubAccountFixture} */) {
    this.account = new GitHubAccount(runInfo, this.desensifier, this.token)
  })

  it('getGitHubData returns the expected information', async function (/** @this {GitHubAccountFixture} */) {
    function shouldBeGitHubRepositories(repositories) {
      repositories.should.be.an.Object()
      repositories.totalCount.should.be.a.Number()
      repositories.totalCount.should.be.greaterThan(0) // for the test account
      repositories.nodes.should.be.an.Array()
      repositories.nodes.length.should.be.lessThanOrEqual(repositories.totalCount)
      repositories.nodes.length.should.be.lessThanOrEqual(100)
      repositories.nodes.forEach(repoNode => {
        repoNode.should.be.an.Object()
        repoNode.id.should.be.a.String()
        repoNode.id.should.not.be.empty()
        repoNode.name.should.be.a.String()
        repoNode.name.should.not.be.empty()

        const owner = repoNode.owner
        owner.should.be.an.Object()
        owner.login.should.be.a.String()
        owner.login.should.not.be.empty()
      })
    }

    const result = await this.account.getGitHubData()

    result.should.be.an.Object()
    result.login.should.be.a.String()
    result.login.should.equal('jandockx')

    shouldBeGitHubRepositories(result.repositories)

    const organizations = result.organizations
    organizations.should.be.an.Object()
    organizations.totalCount.should.be.a.Number()
    organizations.totalCount.should.be.greaterThan(0) // for the test account
    organizations.nodes.should.be.an.Array()
    organizations.nodes.length.should.be.lessThanOrEqual(organizations.totalCount)
    organizations.nodes.length.should.be.lessThanOrEqual(100)
    organizations.nodes.forEach(organizationNode => {
      organizationNode.login.should.be.a.String()
      organizationNode.login.should.not.be.empty()
      shouldBeGitHubRepositories(organizationNode.repositories)
    })

    shouldBeStringifiable(result)
  })

  it('getRepositories does not crash', async function (/** @this {GitHubAccountFixture} */) {
    /** @type {TAccountGetRepositoriesResult} */
    const result = await this.account.getRepositories()

    AccountGetRepositoriesResult.parse(result)
    result.repositories.forEach(repository => repository.runInfo.should.equal(this.account.runInfo))

    shouldBeStringifiable(result)
  })
})
