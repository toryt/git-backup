/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { TrimmedNonEmptyString } = require('../../lib/TrimmedNonEmptyString')
const { z } = require('zod')

const Tokens = z.tuple([TrimmedNonEmptyString, TrimmedNonEmptyString])

/**
 * Automated tests require 2 GitHub credentials to do live tests. If there is a `devCredentials/github.json` file,
 * this function returns its contents. Otherwise, it returns an array that contains the values of the environment
 * variables `TEST_GITHUB_TOKEN_1` and `TEST_GITHUB_TOKEN_2`.
 *
 * This throws if no tokens can be found, or they are not trimmed strings.
 *
 *  Developers should create a file `devCredentials/github.json` in this repository (the directory `devCredentials/`
 *  is git-ignored) that contains an array with 2 tokens:
 *
 * ```json
 * [
 *   "ghp_kLDksi45aq22As5NOd244KdD654KD2ISI22d",
 *   "ghp_kD30Ew02kdee58We47lllEWJ32Ew48KKjdj3"
 * ]
 *  ```
 *
 * A token should give read access to all the repositories the GitHub account it represents can access.
 *
 * The environment variables are intended for in CI.
 *
 * @returns {Array<string>}
 * @throws {Error}
 */
function getGitHubTokens() {
  try {
    const tokens = require('../../devCredentials/github.json')
    console.log(`found ${tokens.length} GitHub tokens for automated tests in \`devCredentials/github.json\``)
    return Tokens.parse(tokens)
  } catch (err) {
    console.log(
      `there is no file \`devCredentials/github.json\`; returning 2 GitHub tokens from environment variables \`TEST_GITHUB_TOKEN_1\` and \`TEST_GITHUB_TOKEN_2\``
    )
    return Tokens.parse([process.env.TEST_GITHUB_TOKEN_1, process.env.TEST_GITHUB_TOKEN_2])
  }
}

module.exports = { getGitHubTokens }
