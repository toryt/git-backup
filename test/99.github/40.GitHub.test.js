/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { GitHub } = require('../../lib/github/GitHub')
const { testBaseDirectory, runDate } = require('../constants')
const { ServiceGetAccountsResult } = require('../../lib/service/ServiceGetAccountsResult')
const { GitHubAccount } = require('../../lib/github/GitHubAccount')
const { JSONValue } = require('../../lib/JSONValue')
const { shouldAdhereToServiceConstructorPostConditions } = require('../60.service/ServiceCommon')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { saltExample } = require('../80.bin/constants')

const token1 = 'token 1'
const token2 = 'token 2'
const notAToken1 = ' not-trimmed '
const notAToken2 = ''
const tokens = [token1, notAToken1, token1, notAToken2, notAToken2, token2]

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants without a salt', function () {
      const subject = new GitHub(testBaseDirectory, runDate, tokens, undefined)

      shouldAdhereToServiceConstructorPostConditions(subject, testBaseDirectory, runDate, tokens, undefined)
      subject.should.be.an.instanceof(GitHub)
    })
    it('creates an object that adheres to the invariants with a salt', function () {
      const subject = new GitHub(testBaseDirectory, runDate, tokens, saltExample)

      shouldAdhereToServiceConstructorPostConditions(subject, testBaseDirectory, runDate, tokens, saltExample)
      subject.should.be.an.instanceof(GitHub)
    })
  })

  describe('methods', function () {
    describe('#getAccounts', function () {
      it('initializes 0 accounts when there are no credentials', function () {
        const subject = new GitHub(testBaseDirectory, runDate, [])

        const result = subject.getAccounts()

        ServiceGetAccountsResult.parse(result)
        result.wrongCredentials.should.be.an.Array()
        result.wrongCredentials.should.be.empty()
        result.accounts.should.be.an.Array()
        result.accounts.should.be.empty()
        shouldBeStringifiable(result)
      })
      it('initializes the accounts as expected with correct credentials, without duplicates, and reports the wrong tokens', function () {
        const subject = new GitHub(testBaseDirectory, runDate, tokens)

        const result = subject.getAccounts()

        ServiceGetAccountsResult.parse(result)

        result.wrongCredentials.should.be.an.Array()
        result.wrongCredentials.forEach(wc => {
          JSONValue.parse(wc)
        })
        result.wrongCredentials.length.should.equal(2)
        result.wrongCredentials.find(wc => wc === notAToken1).should.not.be.undefined()
        result.wrongCredentials.find(wc => wc === notAToken2).should.not.be.undefined()

        result.accounts.should.be.an.Array()
        result.accounts.forEach(a => {
          a.should.be.instanceof(GitHubAccount)
        })
        result.accounts.length.should.equal(2)
        result.accounts.find(a => a.token === token1).should.not.be.undefined()
        result.accounts.find(a => a.token === token2).should.not.be.undefined()

        shouldBeStringifiable(result)
      })
    })
  })
})
