/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { dirname, join } = require('node:path')
const { GitHubAccount } = require('../../lib/github/GitHubAccount')
const { GitHubRepository } = require('../../lib/github/GitHubRepository')
const { create } = require('../../lib/repository/GitCreateProgressStep')
const { update } = require('../../lib/repository/GitUpdateProgressStep')
const { ensureOrigin } = require('../../lib/repository/GitEnsureOriginProgressStep')
const { symLink } = require('../../lib/repository/GitSymLinkProgressStep')
const { remove } = require('fs-extra')
const { setupOutsideBaseFixture } = require('../setupOutsideBaseFixture')
const { getGitHubTokens } = require('./getGitHubTokens')
const { RepositoryRunReport } = require('../../lib/repository/RepositoryRunReport')
const { runReportSuccessFileName, isoYMDPath } = require('../../lib/runReport/saveRunReport')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { RunInformation } = require('../../lib/RunInformation')
const { shouldBeExpectedRunReport } = require('../30.runReport/RunReportCommon')
const { Desensifier } = require('../../lib/Desensifier')
const testName = require('../testName')(module)

describe(testName, function () {
  setupOutsideBaseFixture()

  before(function (/** @this {GitHubAccountFixture} */) {
    /* NOTE: BE AWARE — also when tests fail, these tokens should not appear in output */
    const tokens = getGitHubTokens()
    this.desensifier = new Desensifier()
    tokens.forEach(t => {
      this.desensifier.addSensitive(t)
    })
    this.token = tokens[0]
    this.cleanup = new Set()
  })

  after(async function () {
    return Promise.all([...this.cleanup.values()].map(path => remove(path)))
  })

  /**
   * @param {string} outsideBase
   * @param {string} token
   * @param {Desensifier} desensifier
   * @returns {Promise<{progress: TSuccessProgress, mirrorCloneDirectory: string, ownerDirectory: string}>}
   */
  async function createOrUpdateRun(outsideBase, token, desensifier) {
    // work in the outside base, to prevent failures to infect the repository origin remoteURL of this repository
    const account = new GitHubAccount(new RunInformation(outsideBase, new Date()), desensifier, token)

    const { repositories } = await account.getRepositories()
    const repository = repositories.find(ghr => ghr.name === 'git-backup-test-subject')
    repository.should.be.an.instanceOf(GitHubRepository)

    console.log('id:', repository.id)
    console.log('mirrorClonePath:', repository.mirrorClonePath)
    console.log('runReportDirectory:', repository.runReportDirectory)
    console.log('gitSymLinkPath:', repository.gitSymLinkPath)
    const mirrorCloneDirectory = repository.runInfo.absolutePath(repository.mirrorClonePath)
    const ownerDirectory = dirname(repository.runInfo.absolutePath(repository.gitSymLinkPath))

    const runReport = await repository.backup()

    runReport.id.should.equal(repository.id)
    runReport.mirrorClone.should.equal(repository.mirrorClonePath)
    /* NOTE: do not test remoteURL — if the test fails, true credentials will be exposed in output
    runReport.remoteURL.should.equal(repository.remoteURL)
     */
    runReport.gitSymLink.should.equal(repository.gitSymLinkPath)
    runReport.success.should.equal(true)
    await shouldBeExpectedRunReport(
      repository.runInfo.absolutePath(
        join(
          repository.runReportDirectory,
          isoYMDPath(repository.runInfo),
          runReportSuccessFileName(repository.runInfo, `${repository.owner}_${repository.name}`)
        )
      ),
      runReport,
      repository.desensifier,
      RepositoryRunReport
    )

    shouldBeStringifiable(runReport)

    return { progress: runReport.progress, mirrorCloneDirectory, ownerDirectory }
  }

  function shouldBeUpdateProgress(progress) {
    progress.length.should.equal(3)
    progress[0].action.should.equal(ensureOrigin)
    progress[0].success.should.equal(true)
    progress[1].action.should.equal(update)
    progress[1].success.should.equal(true)
    progress[2].action.should.equal(symLink)
    progress[2].success.should.equal(true)
    progress[2].overwritten.should.equal(true)
  }

  it('can create a GitHub repository, and update it twice', async function (/** @this {GitHubAccountFixture} */) {
    this.timeout(40000)

    const {
      progress: progress0,
      mirrorCloneDirectory: mirrorCloneDirectory0,
      ownerDirectory: ownerDirectory0
    } = await createOrUpdateRun(this.outsideBase, this.token, this.desensifier)
    this.cleanup.add(mirrorCloneDirectory0)
    this.cleanup.add(ownerDirectory0)

    progress0.length.should.equal(3)
    progress0[0].action.should.equal(ensureOrigin)
    progress0[0].success.should.equal(false)
    progress0[0].pathExists.should.equal(false)
    progress0[1].action.should.equal(create)
    progress0[1].success.should.equal(true)
    progress0[2].action.should.equal(symLink)
    progress0[2].success.should.equal(true)
    progress0[2].overwritten.should.equal(false)

    const {
      progress: progress1,
      mirrorCloneDirectory: mirrorCloneDirectory1,
      ownerDirectory: ownerDirectory1
    } = await createOrUpdateRun(this.outsideBase, this.token, this.desensifier)
    this.cleanup.add(mirrorCloneDirectory1)
    this.cleanup.add(ownerDirectory1)
    mirrorCloneDirectory1.should.equal(mirrorCloneDirectory0)
    ownerDirectory1.should.equal(ownerDirectory0)
    shouldBeUpdateProgress(progress1)

    const {
      progress: progress2,
      mirrorCloneDirectory: mirrorCloneDirectory2,
      ownerDirectory: ownerDirectory2
    } = await createOrUpdateRun(this.outsideBase, this.token, this.desensifier)
    this.cleanup.add(mirrorCloneDirectory2)
    this.cleanup.add(ownerDirectory2)
    mirrorCloneDirectory2.should.equal(mirrorCloneDirectory0)
    ownerDirectory2.should.equal(ownerDirectory0)
    shouldBeUpdateProgress(progress2)
  })
})
