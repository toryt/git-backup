/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined, notAnEmptyArray } = require('../_util/stuff')
const { accountGetRepositoriesResults } = require('./AccountGetRepositoriesResultCommon')
const { AccountGetRepositoriesResult } = require('../../lib/account/AccountGetRepositoriesResult')

describe(testName, function () {
  validateSchema(
    AccountGetRepositoriesResult,
    accountGetRepositoriesResults,
    stuffWithUndefined
      .concat(stuffWithUndefined.map(progressStep => ({ ...accountGetRepositoriesResults[0], progressStep })))
      .concat(
        stuffWithUndefined
          .filter(notAnEmptyArray)
          .map(repositories => ({ ...accountGetRepositoriesResults[1], repositories }))
      )
      .concat(
        stuffWithUndefined.map(repository => ({ ...accountGetRepositoriesResults[0], repositories: [repository] }))
      )
  )
})
