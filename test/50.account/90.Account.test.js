/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { Account } = require('../../lib/account/Account')
const { shouldBeAbstractAsyncMethod } = require('../_util/abstract')
const testName = require('../testName')(module)
const { runInfo } = require('../constants')
const { shouldAdhereToAccountConstructorPostConditions } = require('./AccountCommon')
const { Desensifier } = require('../../lib/Desensifier')

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function () {
      const desensifier = new Desensifier()

      const subject = new Account(runInfo, desensifier)

      shouldAdhereToAccountConstructorPostConditions(subject, runInfo, desensifier)
    })
  })

  describe('methods', function () {
    beforeEach(function () {
      this.subject = new Account(runInfo, new Desensifier())
    })

    shouldBeAbstractAsyncMethod('getRepositories')
  })
})
