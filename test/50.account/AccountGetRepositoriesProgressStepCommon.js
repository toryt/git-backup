/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { errorReports } = require('../30.runReport/ErrorReportCommon')

/**
 * @type {Omit<TAccountGetRepositoriesBase, 'success'>}
 */
const accountGetRepositoriesBaseWithoutName = {
  credentials: 'a credentials hash',
  start: '2024-01-07T14:29:26.419Z',
  end: '2024-04-14T14:03:37.848Z',
  duration: 'PT354H49M1.938S'
}

/**
 * @type {Omit<TAccountGetRepositoriesBase, 'success'>}
 */
const accountGetRepositoriesBaseWithName = {
  ...accountGetRepositoriesBaseWithoutName,
  name: 'an account name'
}

/**
 * @type {Array<Omit<TAccountGetRepositoriesBase, 'success'>>}
 */
const accountGetRepositoriesBases = [accountGetRepositoriesBaseWithoutName, accountGetRepositoriesBaseWithName]

/**
 * @type {Array<TAccountGetRepositoriesSuccess>}
 */
const accountGetRepositoriesSuccesses = accountGetRepositoriesBases.map(
  base =>
    /** @type {TAccountGetRepositoriesSuccess} */ ({
      ...base,
      success: true,
      nrOfRepositoriesFound: 137
    })
)

/**
 * @type {Array<TAccountGetRepositoriesFailure>}
 */
const accountGetRepositoriesFailures = accountGetRepositoriesBases.map(
  (base, i) =>
    /** @type {TAccountGetRepositoriesFailure} */ ({
      ...base,
      success: false,
      error: errorReports[i]
    })
)

// noinspection JSCheckFunctionSignatures
/**
 * @type {Array<TAccountGetRepositoriesProgressStep>}
 */
const accountGetRepositoriesProgressSteps = accountGetRepositoriesSuccesses.concat(accountGetRepositoriesFailures)

module.exports = {
  accountGetRepositoriesSuccesses,
  accountGetRepositoriesFailures,
  accountGetRepositoriesProgressSteps
}
