/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const {
  accountGetRepositoriesFailures,
  accountGetRepositoriesSuccesses,
  accountGetRepositoriesProgressSteps
} = require('./AccountGetRepositoriesProgressStepCommon')
const {
  stuffWithUndefined,
  notATrimmedString,
  stuff,
  notANonNegativeSafeInteger,
  notABoolean,
  notASimpleError
} = require('../_util/stuff')
const {
  AccountGetRepositoriesFailure,
  AccountGetRepositoriesSuccess,
  AccountGetRepositoriesProgressStep
} = require('../../lib/account/AccountGetRepositoriesProgressStep')

describe(testName, function () {
  describe('AccountGetRepositoriesSuccess', function () {
    validateSchema(
      AccountGetRepositoriesSuccess,
      accountGetRepositoriesSuccesses,
      stuffWithUndefined
        .concat(stuff.filter(notATrimmedString).map(name => ({ ...accountGetRepositoriesSuccesses[0], name })))
        .concat(
          stuffWithUndefined
            .filter(notATrimmedString)
            .map(credentials => ({ ...accountGetRepositoriesSuccesses[0], credentials }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...accountGetRepositoriesSuccesses[0], start })))
        .concat(stuffWithUndefined.map(end => ({ ...accountGetRepositoriesSuccesses[0], end })))
        .concat(stuffWithUndefined.map(duration => ({ ...accountGetRepositoriesSuccesses[0], duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== true)
            .map(success => ({ ...accountGetRepositoriesSuccesses[0], success }))
        )
        .concat(
          stuffWithUndefined
            .filter(notANonNegativeSafeInteger)
            .map(nrOfRepositoriesFound => ({ ...accountGetRepositoriesSuccesses[0], nrOfRepositoriesFound }))
        )
        .concat(stuffWithUndefined.map(end => ({ ...accountGetRepositoriesSuccesses[0], end })))
    )
  })

  describe('AccountGetRepositoriesFailure', function () {
    validateSchema(
      AccountGetRepositoriesFailure,
      accountGetRepositoriesFailures,
      stuff
        .concat(stuff.filter(notATrimmedString).map(name => ({ ...accountGetRepositoriesFailures[1], name })))
        .concat(
          stuffWithUndefined
            .filter(notATrimmedString)
            .map(credentials => ({ ...accountGetRepositoriesFailures[1], credentials }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...accountGetRepositoriesFailures[1], start })))
        .concat(stuffWithUndefined.map(end => ({ ...accountGetRepositoriesFailures[1], end })))
        .concat(stuffWithUndefined.map(duration => ({ ...accountGetRepositoriesFailures[1], duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== false)
            .map(success => ({ ...accountGetRepositoriesFailures[1], success }))
        )
        .concat(
          stuffWithUndefined.filter(notASimpleError).map(error => ({ ...accountGetRepositoriesFailures[1], error }))
        )
        .concat(stuffWithUndefined.map(end => ({ ...accountGetRepositoriesSuccesses[0], end })))
    )
  })

  describe('AccountGetRepositoriesProgressStep', function () {
    validateSchema(
      AccountGetRepositoriesProgressStep,
      accountGetRepositoriesProgressSteps,
      stuff
        .concat(stuff.filter(notATrimmedString).map(name => ({ ...accountGetRepositoriesFailures[1], name })))
        .concat(
          stuffWithUndefined
            .filter(notATrimmedString)
            .map(credentials => ({ ...accountGetRepositoriesFailures[1], credentials }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...accountGetRepositoriesFailures[1], start })))
        .concat(stuffWithUndefined.map(end => ({ ...accountGetRepositoriesFailures[1], end })))
        .concat(stuffWithUndefined.map(duration => ({ ...accountGetRepositoriesFailures[1], duration })))
        .concat(
          stuffWithUndefined.filter(notABoolean).map(success => ({ ...accountGetRepositoriesFailures[1], success }))
        )
        .concat(stuffWithUndefined.map(end => ({ ...accountGetRepositoriesSuccesses[0], end })))
    )
  })
})
