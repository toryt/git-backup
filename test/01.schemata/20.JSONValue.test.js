/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { JSONLiteral, JSONValue, isCircular, NotCircular } = require('../../lib/JSONValue')
const { jsonLiterals, notJSONLiterals, notJSONValues, jsonValues } = require('./JSONValueCommon')
const { stuffWithoutCircularWithUndefined, circularStuff } = require('../_util/stuff')
const { inspect } = require('node:util')

describe(testName, function () {
  describe('JSONLiteral', function () {
    validateSchema(JSONLiteral, jsonLiterals, notJSONLiterals)
  })

  describe('isCircular', function () {
    describe('not circular', function () {
      stuffWithoutCircularWithUndefined.forEach(s => {
        it(`says ${inspect(s)} is not circular`, function () {
          const result = isCircular(s)
          result.should.be.false()
        })
      })
    })

    describe('circular', function () {
      circularStuff.forEach(s => {
        it(`says ${inspect(s)} is circular`, function () {
          const result = isCircular(s)
          result.should.be.true()
        })
      })
    })
  })

  describe('NotCircular', function () {
    validateSchema(NotCircular, stuffWithoutCircularWithUndefined, circularStuff)
  })

  describe('JSONValue', function () {
    validateSchema(JSONValue, jsonValues, notJSONValues)
  })
})
