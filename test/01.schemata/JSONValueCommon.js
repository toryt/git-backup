/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { stuff, stuffWithUndefined, circularObject, circularError } = require('../_util/stuff')

/**
 * @param {unknown} v
 * @returns {boolean}
 */
function isJSONLiteral(v) {
  return (
    typeof v === 'string' ||
    // infinities and `NaN` become `null` when stringified, but Zod does not allow `NaN` as a number
    (typeof v === 'number' && !Number.isNaN(v)) ||
    typeof v === 'boolean' ||
    v === null
  )
}

/** @type {Array<TJSONLiteral>} */
const jsonLiterals = stuff.filter(isJSONLiteral)
const notJSONLiterals = stuffWithUndefined
  .filter(s => !isJSONLiteral(s))
  .concat([circularObject, circularError, [circularObject], [circularObject]])

/** @type {Array<TJSONValue>} */
const jsonArrays = jsonLiterals.map(l => [l]).concat([])
/** @type {Array<TJSONValue>} */
const jsonObjects = jsonLiterals
  .map(property => ({
    property
  }))
  .concat([{}])
const jsonValues1 = jsonLiterals.concat(jsonArrays).concat(jsonObjects)
/** @type {Array<TJSONValue>} */
const jsonArrays2 = jsonValues1.map(l => [l])
/** @type {Array<TJSONValue>} */
const jsonObjects2 = jsonValues1.map(property => ({
  property
}))

const notJSONLiteralsThatAreLiterals = notJSONLiterals.filter(s => typeof s !== 'object')

/** @type {Array<TJSONValue>} */
const jsonValues = jsonLiterals.concat(jsonArrays2).concat(jsonObjects2)
// noinspection JSCheckFunctionSignatures
const notJSONValues = notJSONLiteralsThatAreLiterals
  .concat([notJSONLiteralsThatAreLiterals])
  .concat(
    notJSONLiteralsThatAreLiterals.map(property => ({
      property
    }))
  )
  .concat([circularObject, circularError, [circularObject], [circularObject]])

module.exports = { jsonLiterals, notJSONLiterals, jsonValues, notJSONValues }
