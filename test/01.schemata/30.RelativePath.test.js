/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const {
  relativePathRegExp,
  platformSpecificRelativeDirPathString,
  RelativePath,
  relativePathMessage,
  forbiddenWindowsChars,
  safePathSegmentString
} = require('../../lib/RelativePath')
const { ZodError } = require('zod')
const { inspect } = require('node:util')
const { stuffWithUndefined, notANonEmptyString } = require('../_util/stuff')
const { sep } = require('node:path')
const { safePathSegmentExamples, unsafePathSegmentStringExamples } = require('../SafePathSegmentCommon')

describe(testName, function () {
  const cases = {
    nix: {
      ok: [
        { path: 'one-segment-relative-path', description: 'consists of a single segment' },
        { path: 'a', description: 'consists of a one-character single segment' },
        { path: 'a/relative/path/that/should/be/allowed', description: 'that does not go up' },
        {
          path: 'a/relative/path/that/should/be/allowed.with.a.dot',
          description: 'does not go up, with a file extension'
        },
        {
          path: 'a/relative/path/that.should.be/allowed/intermediate/dots',
          description: 'does not go up, with dots in a directory name'
        },
        {
          path: 'a/relative/path/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOP/QRSTUVWXYZ/0123456789./with/all-allowed_characters',
          description: 'contains all allowed characters'
        },
        {
          path: 'a/relative/path/that.should.be/allowed/intermediate/dots',
          description: 'does not go up, with dots in a directory name'
        },
        {
          path: 'a/relative/..path.with..segments/that.....contain/series..of/dots.....',
          description: 'that has segments that contain series of dots'
        },
        {
          path: 'a/relative/ path with  segments/some spaces',
          description: 'that has segments that contain spaces'
        },
        {
          path: 'a/relative/pàth/wéth/uniçode',
          description: 'that contains unicode characters'
        },
        {
          path: 'a/relative/😱Ϡ⩟ℙath/with/more/unicode',
          description: 'that contains more unicode characters and an emoji'
        },
        {
          path: 'c-/a/relative/path/that/does/not/start/with/a/drive',
          description: 'does not start with a drive letter (no colon)'
        }
      ],
      nok: [
        { path: '/an/absolute/path', description: 'is absolute' },
        { path: 'c:/an/absolute/path', description: 'starts with a lower case drive letter' },
        { path: 'C:/an/absolute/path', description: 'starts with an upper case drive letter' },
        { path: './this/directory', description: "starts with a '.'" },
        { path: '../up/path', description: "starts with a '..'" },
        { path: 'contains/an//empty/segment', description: 'contains an empty segment' },
        { path: 'should/be/./normalized', description: "has an intermediate '.' directory" },
        { path: 'should/be/../normalized', description: "has an intermediate '..' directory" },
        { path: 'do/not/end/with/.', description: "ends with a '.' directory" },
        { path: 'do/not/end/with/..', description: "ends with a '..' directory" },
        { path: 'has/a/......./segment/that/is/all/dots', description: 'has a segment that is all dots (Windows)' }
      ].concat(
        forbiddenWindowsChars
          .filter(c => !['/', '\\'].includes(c))
          .map(c => ({ path: `contains/an/illegal-${c}-character`, description: `contains '${c}'` }))
      )
    }
  }
  function addWithWindowsPathSeparator(acc, { path, description }) {
    return acc.concat({ path: path.replace(/\//g, `\\`), description })
  }

  cases.windows = {
    ok: cases.nix.ok.reduce(addWithWindowsPathSeparator, []),
    nok: cases.nix.nok.reduce(addWithWindowsPathSeparator, [])
  }

  describe('safePathSegmentString', function () {
    const safePathSegmentRegExp = new RegExp(`^${safePathSegmentString}$`, 'ui')

    describe('ok', function () {
      safePathSegmentExamples.forEach(sps => {
        it(`accepts '${sps}'`, function () {
          safePathSegmentRegExp.test(sps).should.be.true()
        })
      })
    })

    describe('nok', function () {
      unsafePathSegmentStringExamples.forEach(ups => {
        it(`rejects '${inspect(ups)}'`, function () {
          safePathSegmentRegExp.test(ups).should.be.false()
        })
      })
    })
  })

  describe('platformSpecificRelativeDirPathString', function () {
    Object.keys(cases).forEach(platform => {
      describe(platform, function () {
        const [platformSep, otherSep] = platform === 'nix' ? ['/', '\\'] : ['\\', '/']
        // NOTE: flag `v` supported yet in Node only from v20 upward
        // NOTE: setting `g` (global) would mean that we need to reset lastIndex to 0 before every call to test
        const platformSpecificDirPath = new RegExp(`^${platformSpecificRelativeDirPathString(platformSep)}$`, 'ui')

        describe('ok', function () {
          cases[platform].ok.forEach(c => {
            it(`passes a valid relative ${platform} path that ${c.description}`, function () {
              platformSpecificDirPath.test(c.path + platformSep).should.be.true()
            })
          })
        })
        describe('nok', function () {
          cases[platform].nok.forEach(c => {
            it(`fails a path that ${c.description}`, function () {
              platformSpecificDirPath.test(c.path + platformSep).should.be.false()
            })
          })
          it(`fails when a segment contains '${otherSep}', the separator of the other platform`, function () {
            platformSpecificDirPath.test(`contains/an/the/other-${otherSep}-separator/`).should.be.false()
          })
        })
      })
    })
  })

  describe('relativePathRegExp', function () {
    Object.keys(cases).forEach(platform => {
      describe(platform, function () {
        describe('ok', function () {
          cases[platform].ok.forEach(c => {
            it(`passes a valid relative ${platform} path that ${c.description}`, function () {
              relativePathRegExp.test(c.path).should.be.true()
            })
          })
        })
        describe('nok', function () {
          cases[platform].nok.forEach(c => {
            it(`fails a path that ${c.description}`, function () {
              relativePathRegExp.test(c.path).should.be.false()
            })
          })
        })
      })
    })
    it(`fails when separators are mixed`, function () {
      relativePathRegExp.test(`contains/both\\separators/in\\one/path`).should.be.false()
    })
  })

  describe('RelativePath', function () {
    Object.keys(cases).forEach(platform => {
      describe(platform, function () {
        describe('ok', function () {
          cases[platform].ok.forEach(c => {
            it(`passes a valid relative ${platform} path that ${c.description}`, function () {
              console.log(c.path)
              const result = RelativePath.parse(c.path)
              console.log(result)
              const expected = c.path.split(/[/\\]/).join(sep)
              result.should.equal(expected)
            })
          })
        })
        describe('nok', function () {
          stuffWithUndefined.filter(notANonEmptyString).forEach(s => {
            it(`fails for the ${typeof s} \`${inspect(s)}\``, function () {
              try {
                RelativePath.parse(s).should.throw()
              } catch (err) {
                err.should.be.instanceof(ZodError)
                err.issues.length.should.equal(1)
                if (s === undefined) {
                  err.issues[0].message.should.equal('Required')
                } else if (s === null) {
                  err.issues[0].message.should.equal('Expected string, received null')
                } else if (Number.isNaN(s)) {
                  err.issues[0].message.should.equal('Expected string, received nan')
                } else if (typeof s === 'string') {
                  err.issues[0].message.should.equal(relativePathMessage)
                } else if (Array.isArray(s)) {
                  err.issues[0].message.should.equal('Expected string, received array')
                } else if (s instanceof Date) {
                  err.issues[0].message.should.equal(`Expected string, received date`)
                } else {
                  err.issues[0].message.should.equal(`Expected string, received ${typeof s}`)
                }
              }
            })
          })
          cases[platform].nok.forEach(c => {
            it(`fails a path that ${c.description}`, function () {
              try {
                RelativePath.parse(c.path).should.throw()
              } catch (err) {
                err.should.be.instanceof(ZodError)
                err.issues.length.should.equal(1)
                err.issues[0].message.should.equal(relativePathMessage)
              }
            })
          })
        })
      })
    })
    it(`fails when separators are mixed`, function () {
      try {
        RelativePath.parse(`contains/both\\separators/in\\one/path`).should.throw()
      } catch (err) {
        err.should.be.instanceof(ZodError)
        err.issues.length.should.equal(1)
        err.issues[0].message.should.equal(relativePathMessage)
      }
    })
  })
})
