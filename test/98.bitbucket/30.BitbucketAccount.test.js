/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { BitbucketAccount, bitbucketWorkspacesURI, BitbucketWorkspace } = require('../../lib/bitbucket/BitbucketAccount')
const { shouldAdhereToAccountConstructorPostConditions } = require('../50.account/AccountCommon')
const { BitbucketUsername, BitbucketPassword } = require('../../lib/bitbucket/BitbucketCredentials')
const { Logger } = require('../../lib/Logger')
const { stub } = require('sinon')
const { AccountGetRepositoriesFailure } = require('../../lib/account/AccountGetRepositoriesProgressStep')
const { AccountGetRepositoriesResult } = require('../../lib/account/AccountGetRepositoriesResult')
const { BitbucketRepository } = require('../../lib/bitbucket/BitbucketRepository')
const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined, notATrimmedString } = require('../_util/stuff')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { errorReport } = require('../../lib/runReport/ErrorReport')
const { runInfo } = require('../constants')
const { Desensifier } = require('../../lib/Desensifier')

describe(testName, function () {
  /** @type {Array<TBitbucketWorkspace>} */
  const workspaces = Array(3)
    .fill(undefined)
    .map((_, i) => ({ slug: `workspace${i}`, repositoriesURI: `https://www.example.org/repositories/${i}` }))

  /**
   * @param {TBitbucketWorkspace} ws
   * @returns {Array<TBitbucketRepository>}
   */
  function bitbucketRepositories(ws) {
    return Array(3)
      .fill(undefined)
      .map((_, i) => {
        const uuid = `uuid-${ws.slug}-${i}`
        const slug = `${ws.slug}-repository-${i}`
        return {
          uuid,
          workspaceSlug: ws.slug,
          slug,
          cloneURI: `https://www.example.org/${ws.slug}/${slug}`
        }
      })
  }

  describe('BitbucketWorkspace', function () {
    validateSchema(
      BitbucketWorkspace,
      [workspaces[0]],
      stuffWithUndefined
        .concat(stuffWithUndefined.filter(notATrimmedString).map(slug => ({ ...workspaces[1], slug })))
        .concat(stuffWithUndefined.map(repositoriesURI => ({ ...workspaces[1], repositoriesURI })))
    )
  })

  describe('BitbucketAccount', function () {
    /** @type {string} */
    const username = 'a-user'
    const password = 'apassword'

    describe('constructor', function () {
      it('creates an object that adheres to the invariants', function () {
        const desensifier = new Desensifier()
        const subject = new BitbucketAccount(runInfo, desensifier, username, password)
        shouldAdhereToAccountConstructorPostConditions(subject, runInfo, desensifier)
        subject.should.be.an.instanceof(BitbucketAccount)

        // username
        BitbucketUsername.parse(subject.username)
        subject.username.should.equal(username)

        // password
        BitbucketPassword.parse(subject.password)
        subject.password.should.equal(password)

        // logger
        subject.logger.should.be.an.instanceof(Logger)
        subject.logger.identifier.should.be.a.Function()
        subject.logger.identifier().should.equal(subject.username)
        subject.logger.desensifier.should.equal(subject.desensifier)

        // fetchRequest
        subject.fetchRequest.should.be.an.Object()
        subject.fetchRequest.should.have.property('method', 'GET')
        subject.fetchRequest.headers.should.be.an.Object()
        subject.fetchRequest.headers.should.have.property('accept', 'application/json')
        subject.fetchRequest.headers.should.have.property('authorization')
        subject.fetchRequest.headers.authorization.should.startWith('Basic')
        const credentials = subject.fetchRequest.headers.authorization.split('Basic ')[1]
        const decoded = Buffer.from(credentials, 'base64').toString()
        const [decodedUsername, decodedPassword] = decoded.split(':')
        decodedUsername.should.equal(subject.username)
        decodedPassword.should.equal(subject.password)
      })
    })

    describe('methods', function () {
      beforeEach(function () {
        this.subject = new BitbucketAccount(runInfo, new Desensifier(), username, password)
      })

      describe('#getFromBitbucket', function () {
        // can only test the case without a `nextURI` without going live (unless we introduce proxyquire to stub fetch)
        it('returns the empty array when no `nextURI` is given', async function () {
          const result = await this.subject.getFromBitbucket('some things', undefined).should.be.resolved()

          result.should.be.an.Array()
          result.should.be.empty()
        })
      })

      describe('with getFromBitbucket stub', function () {
        beforeEach(function () {
          this.getFromBitbucketStub = stub(this.subject, 'getFromBitbucket')
        })

        afterEach(function () {
          this.getFromBitbucketStub.restore()
        })

        describe('#getWorkspaces', function () {
          it('maps as expected', async function () {
            const extendedWorkspaces = workspaces.map(ws => ({
              slug: ws.slug,
              links: { repositories: { href: ws.repositoriesURI } }
            }))
            this.getFromBitbucketStub.resolves(extendedWorkspaces)

            const result = await this.subject.getWorkspaces().should.be.resolved()

            this.getFromBitbucketStub.should.be.calledOnce()
            this.getFromBitbucketStub.should.be.calledWithExactly('workspaces', bitbucketWorkspacesURI)

            result.should.deepEqual(workspaces)
          })

          it('propagates communication errors', async function () {
            const error = new Error('communication error')
            this.getFromBitbucketStub.rejects(error)

            await this.subject.getWorkspaces().should.be.rejectedWith(error)
            this.getFromBitbucketStub.should.be.calledOnce()
            this.getFromBitbucketStub.should.be.calledWithExactly('workspaces', bitbucketWorkspacesURI)
          })
        })

        describe('#getRepositoriesForWorkspace', function () {
          beforeEach(function () {
            this.workspace = workspaces[0]
          })

          it('maps as expected', async function () {
            const ws0Repositories = bitbucketRepositories(workspaces[0])
            const extendedRepositories = ws0Repositories.map(repository => ({
              uuid: repository.uuid,
              slug: repository.slug,
              workspace: { slug: repository.workspaceSlug },
              links: { clone: [{ name: 'https', href: repository.cloneURI }] }
            }))
            this.getFromBitbucketStub.resolves(extendedRepositories)

            const result = await this.subject.getRepositoriesForWorkspace(this.workspace).should.be.resolved()

            this.getFromBitbucketStub.should.be.calledOnce()
            this.getFromBitbucketStub.should.be.calledWithExactly('repositories', this.workspace.repositoriesURI)

            result.should.deepEqual(ws0Repositories)
          })

          it('propagates communication errors', async function () {
            const error = new Error('communication error')
            this.getFromBitbucketStub.rejects(error)

            await this.subject.getRepositoriesForWorkspace(this.workspace).should.be.rejectedWith(error)
            this.getFromBitbucketStub.should.be.calledOnce()
            this.getFromBitbucketStub.should.be.calledWithExactly('repositories', this.workspace.repositoriesURI)
          })
        })
      })

      describe('#getRepositories', function () {
        /**
         * @typedef {object} TBitbucketAccountGetRepositoriesFixtureBase
         * @property {BitbucketAccount} subject
         * @property {SinonStub<[string, string?], Promise<Array<unknown>>>} getFromBitbucketStub
         * @property {SinonStub<[], Promise<Array<TBitbucketWorkspace>>>} getWorkspacesStub
         * @property {SinonStub<[TBitbucketWorkspace], Promise<Array<TBitbucketRepository>>>} getRepositoriesForWorkspaceStub
         */

        /**
         * @typedef {Context & TBitbucketAccountGetRepositoriesFixtureBase} TBitbucketAccountGetRepositoriesFixture
         */

        beforeEach(
          /** @this {TBitbucketAccountGetRepositoriesFixture} */ function () {
            this.getWorkspacesStub = stub(this.subject, 'getWorkspaces')
            this.getRepositoriesForWorkspaceStub = stub(this.subject, 'getRepositoriesForWorkspace')
          }
        )

        afterEach(
          /** @this {TBitbucketAccountGetRepositoriesFixture} */ function () {
            this.getRepositoriesForWorkspaceStub.restore()
            this.getWorkspacesStub.restore()
          }
        )

        it('returns no repositories when no workspaces were found', /** @this {TBitbucketAccountGetRepositoriesFixture} */ async function () {
          this.getWorkspacesStub.resolves([])

          /** @type {TAccountGetRepositoriesResult} */
          const result = await this.subject.getRepositories().should.be.resolved()

          AccountGetRepositoriesResult.parse(result)
          result.progressStep.name.should.equal(this.subject.username)
          result.progressStep.credentials.should.equal(this.subject.password)
          result.progressStep.nrOfRepositoriesFound.should.equal(0)
          result.progressStep.should.not.have.property('error')
          result.repositories.should.be.an.Array()
          result.repositories.should.be.empty()

          shouldBeStringifiable(result)

          this.getWorkspacesStub.should.have.been.calledOnce()
          this.getRepositoriesForWorkspaceStub.should.not.have.been.called()
        })

        it('returns no repositories when no found workspaces return any repositories', /** @this {TBitbucketAccountGetRepositoriesFixture} */ async function () {
          this.getWorkspacesStub.resolves(workspaces)
          this.getRepositoriesForWorkspaceStub.resolves([])

          /** @type {TAccountGetRepositoriesResult} */
          const result = await this.subject.getRepositories().should.be.resolved()

          AccountGetRepositoriesResult.parse(result)
          result.progressStep.name.should.equal(this.subject.username)
          result.progressStep.credentials.should.equal(this.subject.password)
          result.progressStep.nrOfRepositoriesFound.should.equal(0)
          result.progressStep.should.not.have.property('error')
          result.repositories.should.be.an.Array()
          result.repositories.should.be.empty()

          shouldBeStringifiable(result)

          this.getWorkspacesStub.should.have.been.calledOnce()
          this.getRepositoriesForWorkspaceStub.should.have.been.calledThrice()
          workspaces.forEach(ws => this.getRepositoriesForWorkspaceStub.should.have.been.calledWithExactly(ws))
        })

        it('returns all repositories returned by found workspaces', /** @this {TBitbucketAccountGetRepositoriesFixture} */ async function () {
          const ws0Repositories = bitbucketRepositories(workspaces[0])
          const ws1Repositories = []
          const ws2Repositories = bitbucketRepositories(workspaces[2])
          const allRepositories = ws0Repositories.concat(ws1Repositories).concat(ws2Repositories)

          this.getWorkspacesStub.resolves(workspaces)
          this.getRepositoriesForWorkspaceStub
            .withArgs(workspaces[0])
            .resolves(ws0Repositories)
            .withArgs(workspaces[1])
            .resolves(ws1Repositories)
            .withArgs(workspaces[2])
            .resolves(ws2Repositories)

          /** @type {TAccountGetRepositoriesResult} */
          const result = await this.subject.getRepositories().should.be.resolved()

          AccountGetRepositoriesResult.parse(result)
          result.progressStep.name.should.equal(this.subject.username)
          result.progressStep.credentials.should.equal(this.subject.password)
          result.progressStep.nrOfRepositoriesFound.should.equal(allRepositories.length)
          result.progressStep.should.not.have.property('error')
          result.repositories.should.be.an.Array()

          shouldBeStringifiable(result)

          this.getWorkspacesStub.should.have.been.calledOnce()
          this.getRepositoriesForWorkspaceStub.should.have.been.calledThrice()
          workspaces.forEach(ws => this.getRepositoriesForWorkspaceStub.should.have.been.calledWithExactly(ws))

          result.repositories.forEach(repository => {
            allRepositories.map(({ uuid }) => uuid).should.containEql(repository.id)
          })
          allRepositories.forEach(repository => {
            const matches = result.repositories.filter(repo => repo.id === repository.uuid)
            matches.length.should.equal(1)
            matches[0].should.be.instanceof(BitbucketRepository)
            // noinspection JSValidateTypes
            /** @type {BitbucketRepository} */ const repo = matches[0]
            repo.owner.should.equal(repository.workspaceSlug)
            repo.name.should.equal(repository.slug)
            repo.cloneURI.should.equal(repo.cloneURI)
            repo.appPassword.should.equal(this.subject.password)
          })
        })

        it('propagates failures from getWorkspaces', /** @this {TBitbucketAccountGetRepositoriesFixture} */ async function () {
          const error = new Error('Bitbucket getWorkspaces error')
          this.getWorkspacesStub.rejects(error)

          /** @type {TAccountGetRepositoriesFailure} */
          const thrown = await this.subject.getRepositories().should.be.rejected()
          AccountGetRepositoriesFailure.parse(thrown)
          thrown.name.should.equal(this.subject.username)
          thrown.credentials.should.equal(this.subject.password)
          thrown.error.should.deepEqual(errorReport(error))

          shouldBeStringifiable(thrown)

          this.getWorkspacesStub.should.have.been.calledOnce()
          this.getRepositoriesForWorkspaceStub.should.not.have.been.called()
        })

        it('propagates failures from getRepositoriesForWorkspace', /** @this {TBitbucketAccountGetRepositoriesFixture} */ async function () {
          const error = new Error('Bitbucket getRepositoriesForWorkspace error')
          this.getWorkspacesStub.resolves(workspaces)
          this.getRepositoriesForWorkspaceStub
            .onFirstCall()
            .resolves([])
            .onSecondCall()
            .rejects(error)
            .onThirdCall()
            .resolves([])

          /** @type {TAccountGetRepositoriesFailure} */
          const thrown = await this.subject.getRepositories().should.be.rejected()
          AccountGetRepositoriesFailure.parse(thrown)
          thrown.name.should.equal(this.subject.username)
          thrown.credentials.should.equal(this.subject.password)
          thrown.error.should.deepEqual(errorReport(error))

          shouldBeStringifiable(thrown)

          this.getWorkspacesStub.should.have.been.calledOnce()
          this.getRepositoriesForWorkspaceStub.should.have.been.calledThrice()
          workspaces.forEach(ws => this.getRepositoriesForWorkspaceStub.should.have.been.calledWithExactly(ws))
        })
      })
    })
  })
})
