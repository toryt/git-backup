/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { testBaseDirectory, runDate } = require('../constants')
const { shouldAdhereToServiceConstructorPostConditions } = require('../60.service/ServiceCommon')
const { Bitbucket } = require('../../lib/bitbucket/Bitbucket')
const { ServiceGetAccountsResult } = require('../../lib/service/ServiceGetAccountsResult')
const { JSONValue } = require('../../lib/JSONValue')
const { BitbucketAccount } = require('../../lib/bitbucket/BitbucketAccount')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { saltExample } = require('../80.bin/constants')

const username1 = 'a-username'
const password1 = 'apassword1'
const username2 = 'a-username2'
const password2 = 'apassword2'
const credentials1 = `${username1}:${password1}`
const credentials2 = `${username2}:${password2}`
const notACredentials1 = 'contains-no-colon'
const notACredentials2 = '' // empty string
const credentialsCandidates = [
  credentials1,
  notACredentials1,
  credentials1,
  notACredentials2,
  notACredentials2,
  credentials2
]

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function () {
      const subject = new Bitbucket(testBaseDirectory, runDate, credentialsCandidates, saltExample)

      shouldAdhereToServiceConstructorPostConditions(
        subject,
        testBaseDirectory,
        runDate,
        credentialsCandidates,
        saltExample
      )
      subject.should.be.an.instanceof(Bitbucket)
    })
  })

  describe('methods', function () {
    describe('#getAccounts', function () {
      it('initializes 0 accounts when there are no credentials', function () {
        const subject = new Bitbucket(testBaseDirectory, runDate, [], saltExample)

        const result = subject.getAccounts()

        ServiceGetAccountsResult.parse(result)
        result.wrongCredentials.should.be.an.Array()
        result.wrongCredentials.should.be.empty()
        result.accounts.should.be.an.Array()
        result.accounts.should.be.empty()

        shouldBeStringifiable(result)
      })
      it('initializes the accounts as expected with correct credentials, without duplicates, and reports the wrong tokens', function () {
        const subject = new Bitbucket(testBaseDirectory, runDate, credentialsCandidates, undefined)

        const result = subject.getAccounts()

        ServiceGetAccountsResult.parse(result)

        result.wrongCredentials.should.be.an.Array()
        result.wrongCredentials.forEach(wc => {
          JSONValue.parse(wc)
        })
        result.wrongCredentials.length.should.equal(2)
        result.wrongCredentials.find(wc => wc === notACredentials1).should.not.be.undefined()
        result.wrongCredentials.find(wc => wc === notACredentials2).should.not.be.undefined()

        result.accounts.should.be.an.Array()
        result.accounts.forEach(a => {
          a.should.be.instanceof(BitbucketAccount)
        })
        result.accounts.length.should.equal(2)
        result.accounts.find(a => a.username === username1).should.not.be.undefined()
        result.accounts.find(a => a.username === username1).password.should.equal(password1)
        result.accounts.find(a => a.username === username2).should.not.be.undefined()
        result.accounts.find(a => a.username === username2).password.should.equal(password2)

        shouldBeStringifiable(result)
      })
    })
  })
})
