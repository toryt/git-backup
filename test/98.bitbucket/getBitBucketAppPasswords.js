/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { z } = require('zod')
const { TrimmedNonEmptyString } = require('../../lib/TrimmedNonEmptyString')

const BitBucketAppPasswordCredentials = z.object({
  username: TrimmedNonEmptyString,
  password: TrimmedNonEmptyString
})

/**
 * @typedef {object} TBitBucketAppPasswordCredentials
 * @property {TBitbucketUsername} username
 * @property {TBitbucketPassword} password
 */

const BitBucketAppPasswords = z.tuple([BitBucketAppPasswordCredentials, BitBucketAppPasswordCredentials])

/**
 * @typedef {[TBitBucketAppPasswordCredentials, TBitBucketAppPasswordCredentials]} TBitBucketAppPasswords
 */

/**
 * Automated tests require 2 BitBucket credentials to do live tests. If there is a `devCredentials/bitbucket.json` file,
 * this function returns its contents. Otherwise, it returns an array that contains the values of the environment
 * variables `TEST_BITBUCKET_USERNAME_1`, `TEST_BITBUCKET_PASSWORD_1`, `TEST_BITBUCKET_USERNAME_2`, and
 * `TEST_BITBUCKET_PASSWORD_2`.
 *
 * This throws if no credentials can be found, or they are not trimmed strings.
 *
 *  Developers should create a file `devCredentials/bitbucket.json` in this repository (the directory `devCredentials/`
 *  is git-ignored) that contains an array with 2 tokens:
 *
 * ```json
 * [
 *   {
 *     "username": "thefirstuser",
 *     "password": "iGOdNMQJ5pCuCIyaSxAm"
 *   },
 *   {
 *     "username": "seconduser",
 *     "password": "I9F2KfC1wNv6YpNXvjBx"
 *   }
 * ]
 *  ```
 *
 * An app password should give read access to all the repositories the BitBucket account it represents can access.
 *
 * The environment variables are intended for in CI.
 *
 * @returns {TBitBucketAppPasswords}
 * @throws {Error}
 */
function getBitBucketAppPasswords() {
  try {
    const credentials = require('../../devCredentials/bitbucket.json')
    console.log(
      `found ${credentials.length} BitBucket credentials for automated tests in \`devCredentials/bitbucket.json\``
    )
    return BitBucketAppPasswords.parse(credentials)
  } catch (err) {
    console.log(
      `there is no file \`devCredentials/bitbucket.json\`; returning 2 BitBucket credentials from environment variables \`TEST_BITBUCKET_USERNAME_1\`, \`TEST_BITBUCKET_PASSWORD_1\`, \`TEST_BITBUCKET_USERNAME_2\`, and \`TEST_BITBUCKET_PASSWORD_2\``
    )
    const credentials = [
      { username: process.env.TEST_BITBUCKET_USERNAME_1, password: process.env.TEST_BITBUCKET_PASSWORD_1 },
      { username: process.env.TEST_BITBUCKET_USERNAME_2, password: process.env.TEST_BITBUCKET_PASSWORD_2 }
    ]
    return BitBucketAppPasswords.parse(credentials)
  }
}

module.exports = { getBitBucketAppPasswords }
