/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined } = require('../_util/stuff')
const {
  BitbucketCredentials,
  BitbucketUsername,
  BitbucketPassword
} = require('../../lib/bitbucket/BitbucketCredentials')
const testName = require('../testName')(module)

describe(testName, function () {
  describe('BitbucketCredentials', function () {
    validateSchema(
      BitbucketCredentials,
      ['me-as_user:kDrRR4ldD0EE9DK'],
      stuffWithUndefined.concat([':nousername', 'no-password:', 'no_colon'])
    )
  })

  describe('BitbucketUsername', function () {
    validateSchema(BitbucketUsername, ['me-as_user'], stuffWithUndefined.concat(['contains:colon', '!', '🛑']))
  })

  describe('BitbucketPassword', function () {
    validateSchema(
      BitbucketPassword,
      ['kDrRR4ldD0EE9DK'],
      stuffWithUndefined.concat(['with-a-dash', 'with_an_underscore', 'contains:colon', '!', '🛑'])
    )
  })
})
