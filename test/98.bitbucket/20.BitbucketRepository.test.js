/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { BitbucketRepository } = require('../../lib/bitbucket/BitbucketRepository')
const { shouldAdhereToRepositoryConstructorPostConditions } = require('../40.repository/RepositoryCommon')
const { GitURL } = require('../../lib/repository/GitURL')
const { owner, runInfo } = require('../constants')
const { TrimmedNonEmptyString } = require('../../lib/TrimmedNonEmptyString')
const { Desensifier } = require('../../lib/Desensifier')

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function () {
      /** @type {string} */
      const uuid = 'ebccac28-cc73-4798-bfb2-707af6d37518'

      /** @type {string} */
      const slug = 'the-name-of-the-repo'

      /** @type {string} */
      const username = `theuser`

      /** @type {string} */
      const fqdn = `www.example.org`

      /** @type {string} */
      const cloneURI = `https://${username}@${fqdn}${owner}/${slug}.git`

      /** @type {TBitbucketRepository} */
      const aBitbucketRepo = { uuid, workspaceSlug: owner, slug, cloneURI }

      /** @type {string} */
      const appPassword = 'an app password'

      const desensifier = new Desensifier()

      const subject = new BitbucketRepository(runInfo, desensifier, aBitbucketRepo, appPassword)

      shouldAdhereToRepositoryConstructorPostConditions(subject, runInfo, desensifier, uuid, owner, slug)

      subject.should.be.an.instanceof(BitbucketRepository)

      // cloneURI
      GitURL.parse(subject.cloneURI)
      subject.cloneURI.should.equal(cloneURI)

      // appPassword
      TrimmedNonEmptyString.parse(subject.appPassword)
      subject.appPassword.should.equal(appPassword)

      // remoteURL
      GitURL.parse(subject.remoteURL)
      subject.remoteURL.should.equal(
        `https://${username}:${encodeURIComponent(subject.appPassword)}@${fqdn}${subject.owner}/${subject.name}.git`
      )
      console.log('remoteURL:', subject.remoteURL)
    })
  })
})
