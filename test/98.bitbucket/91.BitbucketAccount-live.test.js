/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { BitbucketAccount } = require('../../lib/bitbucket/BitbucketAccount')
const { getBitBucketAppPasswords } = require('./getBitBucketAppPasswords')
const { AccountGetRepositoriesResult } = require('../../lib/account/AccountGetRepositoriesResult')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { runInfo } = require('../constants')
const { Desensifier } = require('../../lib/Desensifier')
const testName = require('../testName')(module)

describe(testName, function () {
  /**
   * @typedef {Object} BitbucketAccountFixture
   * @property {Bitbucket} [bitbucket]
   * @property {TBitbucketUsername} [username]
   * @property {TBitbucketPassword} [password]
   * @property {Desensifier} [desensifier]
   * @property {BitbucketAccount} [account]
   */

  before(function (/** @this {BitbucketAccountFixture} */) {
    /* NOTE: BE AWARE — also when tests fail, these app passwords should not appear in output */
    const appPasswords = getBitBucketAppPasswords()
    this.desensifier = new Desensifier()
    appPasswords.forEach(({ password }) => {
      this.desensifier.addSensitive(password)
    })
    this.username = appPasswords[0].username
    this.password = appPasswords[0].password
  })

  beforeEach(function (/** @this {BitbucketAccountFixture} */) {
    this.account = new BitbucketAccount(runInfo, this.desensifier, this.username, this.password)
  })

  describe('getWorkspaces', function () {
    it('returns the expected information', async function (/** @this {BitbucketAccountFixture} */) {
      /**
       * @param {Array<TBitbucketWorkspace>} workspaces
       */
      function shouldBeBitbucketWorkspaces(workspaces) {
        workspaces.should.be.an.Array()
        workspaces.forEach(ws => {
          ws.should.be.an.Object()
          ws.slug.should.be.a.String()
          ws.slug.should.not.be.empty()
          ws.repositoriesURI.should.be.a.String()
          ws.repositoriesURI.should.not.be.empty()
        })
      }

      const result = await this.account.getWorkspaces()

      shouldBeBitbucketWorkspaces(result)
    })
  })

  describe('getRepositoriesForWorkspace', function () {
    before(async function () {
      const workspaces = await this.account.getWorkspaces()
      this.workspace = workspaces[0]
    })

    it('returns the expected information', async function (/** @this {BitbucketAccountFixture} */) {
      /**
       * @param {Array<TBitbucketRepository>} repositories
       * @param {string} workspaceSlug
       * @return {void}
       */
      function shouldBeBitbucketRepositories(repositories, workspaceSlug) {
        repositories.should.be.an.Array()
        repositories.forEach(r => {
          r.should.be.an.Object()
          r.uuid.should.be.a.String()
          r.workspaceSlug.should.be.a.String()
          r.workspaceSlug.should.equal(workspaceSlug)
          r.slug.should.be.a.String()
          r.slug.should.not.be.empty()
          r.cloneURI.should.be.a.String()
          r.cloneURI.should.not.be.empty()
          r.cloneURI.should.startWith('https')
          r.cloneURI.should.containEql(workspaceSlug)
          r.cloneURI.should.containEql(r.slug)
        })
      }

      const result = await this.account.getRepositoriesForWorkspace(this.workspace)

      shouldBeBitbucketRepositories(result, this.workspace.slug)
    })
  })

  describe('getRepositories', function () {
    it('getRepositories does not crash', async function (/** @this {BitbucketAccountFixture} */) {
      /** @type {TAccountGetRepositoriesResult} */
      const result = await this.account.getRepositories()

      AccountGetRepositoriesResult.parse(result)
      result.repositories.forEach(repository => repository.runInfo.should.equal(this.account.runInfo))

      shouldBeStringifiable(result)
    })
  })
})
