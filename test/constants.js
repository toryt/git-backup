/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { resolve } = require('node:path')
const { RunInformation } = require('../lib/RunInformation')

const runYear = 2023
const runMonth = 10 // 1-based
const runDay = 7 // 1-based
const runDate = new Date(Date.UTC(runYear, runMonth - 1, runDay, 11, 3, 29, 829))

const testBaseDirectoryName = 'test-base-directory'
const testBaseDirectory = resolve(testBaseDirectoryName)

const runInfo = new RunInformation(testBaseDirectory, runDate)

const owner = 'test-owner'

const idInService = `anId`

const credentialsCandidates = ['firstCredential', 'secondCredential']

module.exports = {
  runYear,
  runMonth,
  runDay,
  runDate,
  testBaseDirectoryName,
  testBaseDirectory,
  runInfo,
  owner,
  idInService,
  credentialsCandidates
}
