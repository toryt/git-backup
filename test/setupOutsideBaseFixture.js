/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { join } = require('node:path')
const { remove } = require('fs-extra')

const thisProjectRoot = join(module.path, '..') // we are some levels down

function setupOutsideBaseFixture() {
  /* NOTE: Any directory in this project will use the origin with which this project is checked out as origin if
           something goes wrong. */

  before(async function () {
    this.outsideBase = join(thisProjectRoot, '..', 'git-backup-base') // a peer to this project directory
    console.log('outside base:', this.outsideBase)
  })

  after(async function () {
    await remove(this.outsideBase)
  })
}

module.exports = {
  setupOutsideBaseFixture
}
