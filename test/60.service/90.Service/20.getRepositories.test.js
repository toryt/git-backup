/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../../testName')(module)
const { testBaseDirectory, runDate, credentialsCandidates } = require('../../constants')
const { stub } = require('sinon')
const { Account } = require('../../../lib/account/Account')
const { createAccountWithRepositories } = require('../createAccountWithRepositories')
const { accountGetRepositoriesFailures } = require('../../50.account/AccountGetRepositoriesProgressStepCommon')
const { Repository } = require('../../../lib/repository/Repository')
const { ServiceGetRepositoriesResult } = require('../../../lib/service/ServiceGetRepositoriesResult')
const { wrongCredentials } = require('../ServiceGetAccountsResultCommon')
const { Service } = require('../../../lib/service/Service')
const { shouldBeStringifiable } = require('../../_util/shouldBeStringifiable')

describe(testName, function () {
  beforeEach(function () {
    this.subject = new Service(testBaseDirectory, runDate, credentialsCandidates)
    this.getAccountsStub = stub(this.subject, 'getAccounts')
  })

  afterEach(function () {
    this.getAccountsStub.restore()
  })

  it('returns the empty array without accounts, without wrong credentials', async function () {
    this.getAccountsStub.returns({ wrongCredentials: [], accounts: [] })

    const result = await this.subject.getRepositories()

    ServiceGetRepositoriesResult.parse(result)
    result.wrongCredentials.should.be.empty()
    result.progress.should.be.empty()
    result.nrOfAccountFailures.should.equal(0)
    result.nrOfAccountSuccesses.should.equal(0)
    result.nrOfRepositoriesFound.should.equal(0)
    result.nrOfUniqueRepositoriesFound.should.equal(0)
    result.repositories.length.should.equal(result.nrOfUniqueRepositoriesFound)

    shouldBeStringifiable(result)
  })

  it('returns the empty array without accounts, with wrong credentials', async function () {
    this.getAccountsStub.returns({ wrongCredentials, accounts: [] })

    const result = await this.subject.getRepositories()

    ServiceGetRepositoriesResult.parse(result)
    result.wrongCredentials.should.deepEqual(wrongCredentials)
    result.progress.should.be.empty()
    result.nrOfAccountFailures.should.equal(0)
    result.nrOfAccountSuccesses.should.equal(0)
    result.nrOfRepositoriesFound.should.equal(0)
    result.nrOfUniqueRepositoriesFound.should.equal(0)
    result.repositories.length.should.equal(result.nrOfUniqueRepositoriesFound)

    shouldBeStringifiable(result)
  })

  it('returns the empty array with accounts that hold no repositories', async function () {
    const accountA = createAccountWithRepositories(this.subject.runInfo, this.subject.desensifier, 'A', [])
    const accountB = createAccountWithRepositories(this.subject.runInfo, this.subject.desensifier, 'B', [])
    this.getAccountsStub.returns({ wrongCredentials: [], accounts: [accountA, accountB] })

    const result = await this.subject.getRepositories()

    ServiceGetRepositoriesResult.parse(result)
    result.wrongCredentials.should.be.empty()
    result.progress.length.should.equal(2)
    result.nrOfAccountFailures.should.equal(0)
    result.nrOfAccountSuccesses.should.equal(2)
    result.nrOfRepositoriesFound.should.equal(0)
    result.nrOfUniqueRepositoriesFound.should.equal(0)
    result.repositories.length.should.equal(result.nrOfUniqueRepositoriesFound)

    shouldBeStringifiable(result)
  })

  it('returns the expected array with accounts that hold repositories', async function () {
    const accountA = createAccountWithRepositories(this.subject.runInfo, this.subject.desensifier, 'A', ['0', '1', '2'])
    const accountB = createAccountWithRepositories(this.subject.runInfo, this.subject.desensifier, 'B', ['3', '4', '0'])
    this.getAccountsStub.returns({ wrongCredentials: [], accounts: [accountA, accountB] })

    const result = await this.subject.getRepositories()

    ServiceGetRepositoriesResult.parse(result)
    result.wrongCredentials.should.be.empty()
    result.progress.length.should.equal(2)
    result.nrOfAccountFailures.should.equal(0)
    result.nrOfAccountSuccesses.should.equal(2)
    result.nrOfRepositoriesFound.should.equal(6)
    result.nrOfUniqueRepositoriesFound.should.equal(5)

    result.repositories.length.should.equal(result.nrOfUniqueRepositoriesFound) // 1 duplicate
    result.repositories.forEach(r => {
      r.should.be.instanceof(Repository)
    })
    const repositories0 = result.repositories.filter(repository => repository.id === `0`)
    repositories0.length.should.equal(1)
    result.repositories.filter(repository => repository.id === `1`).length.should.equal(1)
    result.repositories.filter(repository => repository.id === `2`).length.should.equal(1)
    result.repositories.filter(repository => repository.id === `3`).length.should.equal(1)
    result.repositories.filter(repository => repository.id === `4`).length.should.equal(1)
    /* `accountId` was added to `runInfo.baseDirectoryName` be able to recognize the account this came from in this
        test: first one wins */
    repositories0[0].runInfo.baseDirectoryName.should.endWith(` (A)`)

    shouldBeStringifiable(result)
  })

  it('returns the expected array with accounts that hold repositories, if 1 fails', async function () {
    const accountA = createAccountWithRepositories(this.subject.runInfo, this.subject.desensifier, 'A', [
      '0',
      '1',
      '2',
      '1'
    ])
    const rejectingAccount = new Account(this.subject.runInfo, this.subject.desensifier)
    stub(rejectingAccount, 'getRepositories').rejects(accountGetRepositoriesFailures[1])
    this.getAccountsStub.returns({ wrongCredentials: [], accounts: [accountA, rejectingAccount] })

    const result = await this.subject.getRepositories()

    ServiceGetRepositoriesResult.parse(result)
    result.wrongCredentials.should.be.empty()
    result.progress.length.should.equal(2)
    result.nrOfAccountFailures.should.equal(1)
    result.nrOfAccountSuccesses.should.equal(1)
    result.nrOfRepositoriesFound.should.equal(4)
    result.nrOfUniqueRepositoriesFound.should.equal(3)

    result.repositories.length.should.equal(result.nrOfUniqueRepositoriesFound) // 1 duplicate
    result.repositories.forEach(r => {
      r.should.be.instanceof(Repository)
    })
    result.repositories.filter(repository => repository.id === `0`).length.should.equal(1)
    result.repositories.filter(repository => repository.id === `1`).length.should.equal(1)
    result.repositories.filter(repository => repository.id === `2`).length.should.equal(1)

    shouldBeStringifiable(result)
  })
})
