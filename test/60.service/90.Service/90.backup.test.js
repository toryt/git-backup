/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { Service } = require('../../../lib/service/Service')
const testName = require('../../testName')(module)
const { testBaseDirectory, runDate, credentialsCandidates } = require('../../constants')
const { stub } = require('sinon')
const { serviceRunReports } = require('../ServiceRunReportCommon')
const { errorReport } = require('../../../lib/runReport/ErrorReport')

describe(testName, function () {
  describe('instance', function () {
    beforeEach(function () {
      this.subject = new Service(testBaseDirectory, runDate, credentialsCandidates)
      this.backupAllRepositoriesStub = stub(this.subject, 'backupAllRepositories')
      this.saveRunReportStub = stub(this.subject, 'saveRunReport')
    })

    afterEach(function () {
      this.saveRunReportStub.restore()
      this.backupAllRepositoriesStub.restore()
    })

    describe('can save run report', function () {
      beforeEach(function () {
        this.saveRunReportStub.resolves()
      })

      it('calls backupAllRepositories and saves the run report when successful', async function () {
        const runReport = serviceRunReports.find(srr => srr.success)
        this.backupAllRepositoriesStub.resolves(runReport)

        await this.subject.backup().should.be.resolvedWith(runReport)

        this.backupAllRepositoriesStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledWithExactly(runReport)
      })

      it('calls backupAllRepositories and saves the run report on failure', async function () {
        const runReport = serviceRunReports.find(srr => !srr.success)
        this.backupAllRepositoriesStub.resolves(runReport)

        await this.subject.backup().should.be.resolvedWith(runReport)

        this.backupAllRepositoriesStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledWithExactly(runReport)
      })
    })

    describe('cannot save run report', function () {
      beforeEach(function () {
        this.errorReport = errorReport(new Error('cannot save service run report'))
      })

      it('calls backupAllRepositories successfully and reports when it cannot save the run report', async function () {
        const runReport = serviceRunReports.find(srr => srr.success)
        const urrt = { runReport, error: this.errorReport }
        this.backupAllRepositoriesStub.resolves(runReport)
        this.saveRunReportStub.rejects(urrt)

        await this.subject.backup().should.be.rejectedWith(urrt)

        this.backupAllRepositoriesStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledWithExactly(runReport)
      })

      it('calls backupAllRepositories with a failure and reports when it cannot save the run report', async function () {
        const runReport = serviceRunReports.find(srr => !srr.success)
        const urrt = { runReport, error: this.errorReport }
        this.backupAllRepositoriesStub.resolves(runReport)
        this.saveRunReportStub.rejects(urrt)

        await this.subject.backup().should.be.rejectedWith(urrt)

        this.backupAllRepositoriesStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledOnce()
        this.saveRunReportStub.should.have.been.calledWithExactly(runReport)
      })
    })
  })
})
