/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { Service } = require('../../../lib/service/Service')
const { shouldBeAbstractMethod } = require('../../_util/abstract')
const testName = require('../../testName')(module)
const { testBaseDirectory, runDate, credentialsCandidates } = require('../../constants')
const { shouldAdhereToServiceConstructorPostConditions } = require('../ServiceCommon')
const { saltExample } = require('../../80.bin/constants')

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants without a salt', function () {
      const subject = new Service(testBaseDirectory, runDate, credentialsCandidates)

      shouldAdhereToServiceConstructorPostConditions(
        subject,
        testBaseDirectory,
        runDate,
        credentialsCandidates,
        undefined
      )
    })
    it('creates an object that adheres to the invariants with a salt', function () {
      const subject = new Service(testBaseDirectory, runDate, credentialsCandidates, saltExample)

      shouldAdhereToServiceConstructorPostConditions(
        subject,
        testBaseDirectory,
        runDate,
        credentialsCandidates,
        saltExample
      )
    })
  })

  describe('instance', function () {
    beforeEach(function () {
      this.subject = new Service(testBaseDirectory, runDate, credentialsCandidates)
    })

    describe('stubbed', function () {
      describe('abstract methods', function () {
        shouldBeAbstractMethod('getAccounts')
      })
    })
  })
})
