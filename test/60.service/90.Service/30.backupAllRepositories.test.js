/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../../testName')(module)
const { owner, testBaseDirectory, runDate, credentialsCandidates } = require('../../constants')
const { stub } = require('sinon')
const {
  accountGetRepositoriesProgressSteps,
  accountGetRepositoriesSuccesses
} = require('../../50.account/AccountGetRepositoriesProgressStepCommon')
const { Repository } = require('../../../lib/repository/Repository')
const {
  successRepositoryRunReports,
  failureRepositoryRunReports
} = require('../../40.repository/RepositoryRunReportCommon')
const { ServiceRunReport } = require('../../../lib/service/ServiceRunReport')
const { cartesianProduct } = require('../../_util/cartesianProduct')
const { Service } = require('../../../lib/service/Service')
const { shouldBeStringifiable } = require('../../_util/shouldBeStringifiable')
const { errorReports } = require('../../30.runReport/ErrorReportCommon')

describe(testName, function () {
  /**
   * @typedef {object} TRepositoryWithRR
   * @property {Repository} repository
   * @property {TRepositoryRunReport} rrr
   * @property {TUnsavedRunReportThrowable<TRepositoryRunReport>} [urrt]
   */

  /**
   * @typedef {object} TRepositoriesWithRRMap
   * @property {TRepositoryWithRR} repositorySuccess
   * @property {TRepositoryWithRR} repositoryFail
   * @property {TRepositoryWithRR} repositoryCannotSave
   */

  /**
   * @typedef {object} TServiceBackAllRepositoriesFixtureBase
   * @property {Service} subject
   * @property {Array<SinonStub<[], TRepositoryRunReport>>} backupStubs
   * @property {SinonStub<[], TServiceGetRepositoriesResult>} getRepositoriesStub
   * @property { TRepositoriesWithRRMap } repositoriesWithRR
   */

  /**
   * @typedef {TServiceBackAllRepositoriesFixtureBase & Context} TServiceBackAllRepositoriesFixture
   */

  beforeEach(function (/** @this {TServiceBackAllRepositoriesFixture} */) {
    this.subject = new Service(testBaseDirectory, runDate, credentialsCandidates)

    this.backupStubs = []

    const repositorySuccess = new Repository(
      this.subject.runInfo,
      this.subject.desensifier,
      'successRepository',
      owner,
      'success-repository'
    )
    const rrrSuccess = { ...successRepositoryRunReports[0], id: repositorySuccess.id }
    const repositoryFail = new Repository(
      this.subject.runInfo,
      this.subject.desensifier,
      'failingRepository',
      owner,
      'failing-repository'
    )
    const rrrFail = { ...failureRepositoryRunReports[0], id: repositoryFail.id }
    const repositoryCannotSave = new Repository(
      this.subject.runInfo,
      this.subject.desensifier,
      'cannotSaveRepository',
      owner,
      'cannot-save-repository'
    )
    const rrrCannotSave = { ...successRepositoryRunReports[0], id: repositoryCannotSave.id }

    this.repositoriesWithRR = {
      repositorySuccess: { repository: repositorySuccess, rrr: rrrSuccess },
      repositoryFail: { repository: repositoryFail, rrr: rrrFail },
      repositoryCannotSave: {
        repository: repositoryCannotSave,
        rrr: rrrCannotSave,
        urrt: {
          runReport: rrrCannotSave,
          error: errorReports[8]
        }
      }
    }
  })

  afterEach(function (/** @this {TServiceBackAllRepositoriesFixture} */) {
    this.getRepositoriesStub.restore()
    this.backupStubs.forEach(s => s.restore())
  })

  /**
   * @param {TServiceBackAllRepositoriesFixture} fixture
   * @param {Array<string>} wrongCredentials
   * @param {Array<TAccountGetRepositoriesProgressStep>} accountGetRepositoriesProgressSteps
   * @param {Array<TRepositoryWithRR>} repositoriesWithRR
   * @returns {Promise<void>}
   */
  async function testBackupAllRepositories(
    fixture,
    wrongCredentials,
    accountGetRepositoriesProgressSteps,
    repositoriesWithRR
  ) {
    const { repositories, repositoryRunReports, unsavedRepositoryRunReports } = repositoriesWithRR.reduce(
      (acc, repoWithRR) => {
        acc.repositories.push(repoWithRR.repository)
        acc.repositoryRunReports.push(repoWithRR.rrr)
        if (repoWithRR.urrt) {
          acc.unsavedRepositoryRunReports.push(repoWithRR.urrt)
          fixture.backupStubs.push(stub(repoWithRR.repository, 'backup').rejects(repoWithRR.urrt))
        } else {
          fixture.backupStubs.push(stub(repoWithRR.repository, 'backup').resolves(repoWithRR.rrr))
        }

        return acc
      },
      { repositories: [], repositoryRunReports: [], unsavedRepositoryRunReports: [] }
    )

    /**
     * @type {TServiceGetRepositoriesResult}
     */
    const getRepositoriesResult = {
      wrongCredentials,
      progress: accountGetRepositoriesProgressSteps,
      nrOfAccountFailures: accountGetRepositoriesProgressSteps.filter(
        accountGetRepositoriesProgressStep => accountGetRepositoriesProgressStep.success === false
      ).length,
      nrOfAccountSuccesses: accountGetRepositoriesProgressSteps.filter(
        accountGetRepositoriesProgressStep => accountGetRepositoriesProgressStep.success === true
      ).length,
      nrOfRepositoriesFound: 137,
      nrOfUniqueRepositoriesFound: repositories.length,
      repositories
    }
    fixture.getRepositoriesStub = stub(fixture.subject, 'getRepositories').resolves(getRepositoriesResult)

    const result = await fixture.subject.backupAllRepositories()

    ServiceRunReport.parse(result)
    result.service.should.equal(fixture.subject.runInfo.baseDirectoryName)
    result.backupDirectory.should.equal(fixture.subject.runInfo.backupDirectory)
    result.wrongCredentials.should.deepEqual(getRepositoriesResult.wrongCredentials)
    result.progress.should.deepEqual(getRepositoriesResult.progress)
    result.nrOfAccountFailures.should.equal(getRepositoriesResult.nrOfAccountFailures)
    result.nrOfAccountSuccesses.should.equal(getRepositoriesResult.nrOfAccountSuccesses)
    result.nrOfRepositoriesFound.should.equal(getRepositoriesResult.nrOfRepositoriesFound)
    result.nrOfUniqueRepositoriesFound.should.equal(getRepositoriesResult.nrOfUniqueRepositoriesFound)
    result.repositoryRunReports.should.deepEqual(repositoryRunReports)
    result.unsavedRepositoryRunReports.should.deepEqual(unsavedRepositoryRunReports)
    result.success.should.equal(
      result.wrongCredentials.length <= 0 &&
        result.nrOfAccountFailures <= 0 &&
        result.repositoryRunReports.every(rrr => rrr.success) &&
        result.unsavedRepositoryRunReports.length <= 0
    )

    fixture.getRepositoriesStub.should.have.been.calledOnce()
    fixture.backupStubs.forEach(s => s.should.have.been.calledOnce())

    shouldBeStringifiable(result)
  }

  /**
   * @typedef {object} TCombination
   * @property {Array<string>} wrongCredentials
   * @property {Array<TAccountGetRepositoriesProgressStep>} accountGetRepositoriesProgressSteps
   * @property {Array<'repositorySuccess' | 'repositoryFail' | 'repositoryCannotSave'>} repositoriesToSelect
   */

  /**
   * @type {Array<TCombination>}
   */
  const combinations = cartesianProduct({
    wrongCredentials: [[], ['some wrong credential']],
    accountGetRepositoriesProgressSteps: [[], accountGetRepositoriesSuccesses, accountGetRepositoriesProgressSteps],
    repositoriesToSelect: [
      [],
      ['repositorySuccess'],
      ['repositorySuccess', 'repositoryFail'],
      ['repositorySuccess', 'repositoryFail', 'repositoryCannotSave'],
      ['repositorySuccess', 'repositoryCannotSave'],
      ['repositoryFail', 'repositoryCannotSave'],
      ['repositoryCannotSave']
    ]
  })

  combinations.forEach(({ wrongCredentials, accountGetRepositoriesProgressSteps, repositoriesToSelect }) => {
    const nrOfAccountFailures = accountGetRepositoriesProgressSteps.filter(
      accountGetRepositoriesProgressStep => accountGetRepositoriesProgressStep.success === false
    ).length

    it(`returns the expected service run report for ${wrongCredentials.length} wrong credentials, ${nrOfAccountFailures} of account failures, and repositories [${repositoriesToSelect}]`, async function (/** @this {TServiceBackAllRepositoriesFixture} */) {
      const repositoriesWithRR = repositoriesToSelect.map(name => this.repositoriesWithRR[name])

      return testBackupAllRepositories(this, [], accountGetRepositoriesProgressSteps, repositoriesWithRR)
    })
  })
})
