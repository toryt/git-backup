/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined, notAnEmptyArray, notANonNegativeSafeInteger } = require('../_util/stuff')
const { ServiceGetRepositoriesResult } = require('../../lib/service/ServiceGetRepositoriesResult')
const { serviceGetRepositoriesResult } = require('./ServiceGetRepositoriesResultCommon')
const { serviceGetAccountsResults } = require('./ServiceGetAccountsResultCommon')
const { createRepositories } = require('../40.repository/createRepositories')
const { runInfo } = require('../constants')
const { Desensifier } = require('../../lib/Desensifier')

const example = serviceGetRepositoriesResult(createRepositories(runInfo, new Desensifier(), ['i', 'j', 'k']))

describe(testName, function () {
  validateSchema(
    ServiceGetRepositoriesResult,
    [example],
    stuffWithUndefined
      .concat(
        stuffWithUndefined
          .filter(notAnEmptyArray)
          .map(wrongCredentials => ({ ...serviceGetAccountsResults[0], wrongCredentials }))
      )
      .concat(stuffWithUndefined.filter(notAnEmptyArray).map(progress => ({ ...example, progress })))
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfAccountFailures => ({ ...example, nrOfAccountFailures }))
      )
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfAccountSuccesses => ({ ...example, nrOfAccountSuccesses }))
      )
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfRepositoriesFound => ({ ...example, nrOfRepositoriesFound }))
      )
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfUniqueRepositoriesFound => ({ ...example, nrOfUniqueRepositoriesFound }))
      )
      .concat(stuffWithUndefined.filter(notAnEmptyArray).map(repositories => ({ ...example, repositories })))
      .concat(stuffWithUndefined.map(repository => ({ ...example, repositories: [repository] })))
  )
})
