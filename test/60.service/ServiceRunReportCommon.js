/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { testBaseDirectoryName } = require('../constants')
const { backupDirectoryName } = require('../../lib/RunInformation')
const { repositoryRunReports } = require('../40.repository/RepositoryRunReportCommon')
const { unsavedRunReportThrowables } = require('../30.runReport/UnsavedRunReportThrowableCommon')
const { cartesianProduct } = require('../_util/cartesianProduct')
const { environmentInfo } = require('../30.runReport/EnvironmentInfoCommon')
const { accountGetRepositoriesProgressSteps } = require('../50.account/AccountGetRepositoriesProgressStepCommon')

/**
 * @typedef {Object} TBaseValues
 * @property {Array<string>} wrongCredentials
 * @property {Array<TAccountGetRepositoriesProgressStep>} progress
 * @property {number} nrOfAccountFailures
 * @property {Array<TRepositoryRunReport>} repositoryRunReports
 * @property {Array<TUnsavedRunReportThrowable<TRepositoryRunReport>>} unsavedRepositoryRunReports
 */

/**
 * @type {Array<TBaseValues>}
 */
const allCombinations = cartesianProduct({
  wrongCredentials: [[], ['some wrong credential']],
  progress: [[], accountGetRepositoriesProgressSteps],
  nrOfAccountFailures: [0, 137],
  repositoryRunReports: [[], [repositoryRunReports[0]], repositoryRunReports],
  unsavedRepositoryRunReports: [[], unsavedRunReportThrowables]
})

/**
 * @type {Array<TServiceRunReport>}
 */
const serviceRunReports = allCombinations.map(
  ({ wrongCredentials, progress, nrOfAccountFailures, repositoryRunReports, unsavedRepositoryRunReports }) => ({
    v: 1, // assuming version 1
    environment: environmentInfo,
    service: testBaseDirectoryName,
    backupDirectory: backupDirectoryName,
    start: '2024-04-08T19:01:08.873Z',
    wrongCredentials,
    progress,
    nrOfAccountFailures,
    nrOfAccountSuccesses: 5, // arbitrary number
    nrOfRepositoriesFound: repositoryRunReports.length + unsavedRepositoryRunReports.length + 3,
    nrOfUniqueRepositoriesFound: repositoryRunReports.length + unsavedRepositoryRunReports.length, // arbitrary number
    repositoryRunReports,
    unsavedRepositoryRunReports,
    success:
      wrongCredentials.length <= 0 &&
      nrOfAccountFailures <= 0 &&
      repositoryRunReports.every(rrr => rrr.success) &&
      unsavedRepositoryRunReports.length <= 0,
    end: '2024-04-08T19:01:31.852Z',
    duration: 'PT22.979S'
  })
)

module.exports = { serviceRunReports }
