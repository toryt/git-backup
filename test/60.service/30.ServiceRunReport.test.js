/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const {
  stuffWithUndefined,
  notANonEmptyString,
  notABoolean,
  notATrimmedString,
  notAnEmptyArray,
  notANonNegativeSafeInteger
} = require('../_util/stuff')
const { validateSchema } = require('../_util/validateSchema')
const { ServiceRunReport } = require('../../lib/service/ServiceRunReport')
const { serviceRunReports } = require('./ServiceRunReportCommon')
const { notJSONValues } = require('../01.schemata/JSONValueCommon')

describe(testName, function () {
  validateSchema(
    ServiceRunReport,
    serviceRunReports,
    stuffWithUndefined
      .concat(stuffWithUndefined.filter(s => s !== 1).map(v => ({ ...serviceRunReports[0], v })))
      .concat(
        stuffWithUndefined.filter(notATrimmedString).map(environment => ({ ...serviceRunReports[0], environment }))
      )
      .concat(stuffWithUndefined.filter(notATrimmedString).map(service => ({ ...serviceRunReports[0], service })))
      .concat(
        stuffWithUndefined
          .filter(notANonEmptyString)
          .map(backupDirectory => ({ ...serviceRunReports[0], backupDirectory }))
      )
      .concat(stuffWithUndefined.filter(notANonEmptyString).map(start => ({ ...serviceRunReports[0], start })))
      .concat(
        stuffWithUndefined
          .filter(notAnEmptyArray)
          .map(wrongCredentials => ({ ...serviceRunReports[0], wrongCredentials }))
      )
      .concat(notJSONValues.map(wrongCredential => ({ ...serviceRunReports[0], wrongCredentials: [wrongCredential] })))
      .concat(stuffWithUndefined.filter(notAnEmptyArray).map(progress => ({ ...serviceRunReports[0], progress })))
      .concat(
        stuffWithUndefined
          .filter(notAnEmptyArray)
          .map(progressStep => ({ ...serviceRunReports[0], progress: [progressStep] }))
      )
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfAccountSuccesses => ({ ...serviceRunReports[0], nrOfAccountSuccesses }))
      )
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfRepositoriesFound => ({ ...serviceRunReports[0], nrOfRepositoriesFound }))
      )
      .concat(
        stuffWithUndefined
          .filter(notANonNegativeSafeInteger)
          .map(nrOfUniqueRepositoriesFound => ({ ...serviceRunReports[0], nrOfUniqueRepositoriesFound }))
      )
      .concat(
        stuffWithUndefined
          .filter(notAnEmptyArray)
          .map(repositoryRunReports => ({ ...serviceRunReports[0], repositoryRunReports }))
      )
      .concat(
        stuffWithUndefined.map(repositoryRunReport => ({
          ...serviceRunReports[0],
          repositoryRunReports: [repositoryRunReport]
        }))
      )
      .concat(
        stuffWithUndefined
          .filter(notAnEmptyArray)
          .map(unsavedRepositoryRunReports => ({ ...serviceRunReports[0], unsavedRepositoryRunReports }))
      )
      .concat(
        stuffWithUndefined.map(unsavedRepositoryRunReport => ({
          ...serviceRunReports[0],
          unsavedRepositoryRunReports: [unsavedRepositoryRunReport]
        }))
      )
      .concat(stuffWithUndefined.filter(notANonEmptyString).map(end => ({ ...serviceRunReports[0], end })))
      .concat(stuffWithUndefined.filter(notANonEmptyString).map(duration => ({ ...serviceRunReports[0], duration })))
      .concat(stuffWithUndefined.filter(notABoolean).map(success => ({ ...serviceRunReports[0], success })))
  )
})
