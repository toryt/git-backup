/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Account } = require('../../lib/account/Account')
const { stub } = require('sinon')
const { createRepositories } = require('../40.repository/createRepositories')

/**
 * @param {RunInformation} runInfo
 * @param {Desensifier} desensifier
 * @param {string} accountId
 * @param {Array<string>} repoIds
 * @returns {Account}
 */
function createAccountWithRepositories(runInfo, desensifier, accountId, repoIds) {
  const account = new Account(runInfo, desensifier)
  const repositories = createRepositories(
    // `accountId` added to `baseDirectoryName` be able to recognize the account this came from in tests
    Object.create(runInfo, {
      baseDirectoryName: { value: `${runInfo.baseDirectoryName} (${accountId})` }
    }),
    desensifier,
    repoIds
  )
  /** @type {TAccountGetRepositoriesResult} */
  const getRepositoriesResult = {
    progressStep: {
      name: accountId,
      credentials: 'a credentials hash',
      start: '2024-04-14T13:48:13.044Z',
      nrOfRepositoriesFound: repositories.length,
      end: '2024-04-14T14:03:37.848Z',
      duration: 'PT15M24.804S',
      success: true
    },
    repositories
  }
  stub(account, 'getRepositories').resolves(getRepositoriesResult)
  return account
}

module.exports = { createAccountWithRepositories }
