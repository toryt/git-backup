/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { Logger } = require('../../lib/Logger')
const { Service, runReportsDirectoryName } = require('../../lib/service/Service')
const { RelativePath } = require('../../lib/RelativePath')
const { join, basename } = require('node:path')
const { shouldBeGoodAbsolutePath } = require('../_util/shouldBeGoodAbsolutePath')
const { RunInformation } = require('../../lib/RunInformation')
const { ServiceRunReport } = require('../../lib/service/ServiceRunReport')
const { Desensifier } = require('../../lib/Desensifier')
const should = require('should')

/**
 * @param {Service} subject
 * @param {string} baseDirectory
 * @param {Date} runDate
 * @param {Array<string>} credentialsCandidates
 * @param {string | undefined} salt
 * @returns {void}
 */
function shouldAdhereToServiceConstructorPostConditions(subject, baseDirectory, runDate, credentialsCandidates, salt) {
  subject.should.be.an.instanceof(Service)

  // runReportSchema
  subject.runReportSchema.should.equal(ServiceRunReport)

  // runInfo
  subject.runInfo.should.be.instanceof(RunInformation)
  subject.runInfo.baseDirectory.should.equal(baseDirectory)
  subject.runInfo.runDate.should.equal(runDate)
  subject.runInfo.baseDirectoryName.should.equal(basename(baseDirectory))

  // credentialsCandidates
  subject.credentialsCandidates.forEach(cc => credentialsCandidates.should.containEql(cc))
  credentialsCandidates.forEach(cc => subject.credentialsCandidates.should.containEql(cc))
  Array.from(new Set(subject.credentialsCandidates)).should.deepEqual(subject.credentialsCandidates)
  subject.credentialsCandidates.should.not.equal(credentialsCandidates)

  // desensifier
  subject.desensifier.should.be.instanceof(Desensifier)
  should(subject.desensifier.salt).equal(salt)
  credentialsCandidates.forEach(cc => {
    subject.desensifier.sensitives.should.containEql(cc)
  })
  subject.desensifier.sensitives.forEach(s => {
    credentialsCandidates.should.containEql(s)
  })

  // logger
  subject.logger.should.be.instanceof(Logger)
  subject.logger.identifier.should.be.a.Function()
  subject.logger.identifier().should.equal(subject.runInfo.baseDirectoryName)
  subject.logger.desensifier.should.equal(subject.desensifier)

  // runReportDirectory
  console.log('runReportDirectory:', subject.runReportDirectory)
  RelativePath.parse(subject.runReportDirectory)
  subject.runReportDirectory.should.equal(join(subject.runInfo.backupDirectory, runReportsDirectoryName))
  shouldBeGoodAbsolutePath(subject.runInfo.absolutePath(subject.runReportDirectory))
}

module.exports = { shouldAdhereToServiceConstructorPostConditions }
