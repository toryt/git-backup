/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { getLoggingLevel, setLoggingLevel, loggingLevels, Logger } = require('../lib/Logger')
const should = require('should')
const { Desensifier } = require('../lib/Desensifier')
const { saltExample } = require('./80.bin/constants')
const testName = require('./testName')(module)

const sensitive = 'SENSITIVE'
const identifier = `a logging identifier with ${sensitive} information`
const identifierFunction = () => identifier
const message = `log message with ${sensitive} data`
const methods = ['error', 'warn', 'debug', 'trace']

describe(testName, function () {
  before(function () {
    this.originalLevel = getLoggingLevel()
  })

  afterEach(function () {
    setLoggingLevel(this.originalLevel)
  })

  describe('loggingLevel', function () {
    loggingLevels.forEach(level => {
      it(`can set the logging level to ${level}`, function () {
        setLoggingLevel(level)
        should(getLoggingLevel()).equal(level)
      })
    })
  })

  describe('Logger', function () {
    describe('constructor', function () {
      it('can construct a logger', function () {
        const desensifier = new Desensifier()
        const subject = new Logger(identifierFunction, desensifier)

        subject.should.be.instanceof(Logger)
        subject.identifier.should.equal(identifierFunction)
        subject.prefix.should.containEql(identifier)
        subject.desensifier.should.equal(desensifier)
      })
    })

    describe('methods', function () {
      beforeEach(function () {
        const desensifier = new Desensifier(saltExample)
        desensifier.addSensitive(sensitive)
        this.subject = new Logger(identifierFunction, desensifier)
      })

      describe('shouldLog', function () {
        loggingLevels.forEach(level => {
          describe(`with logging level ${level}`, function () {
            const levelIndex = loggingLevels.indexOf(level)

            beforeEach(function () {
              setLoggingLevel(level)
            })

            loggingLevels.forEach(messageLevel => {
              const expected = levelIndex >= loggingLevels.indexOf(messageLevel)

              it(`returns ${expected} for a message with level ${messageLevel} when the logging level is ${level}`, function () {
                const result = this.subject.shouldLog(messageLevel)

                result.should.be[expected ? 'true' : 'false']()
              })
            })
          })
        })
      })

      methods.forEach(method => {
        describe(`#${method}`, function () {
          loggingLevels.forEach(level => {
            it(`level ${level}`, function () {
              setLoggingLevel(level)
              this.subject[method](`${message} (level set: ${level})`)
            })
          })
        })
      })

      describe(`#success`, function () {
        loggingLevels.forEach(level => {
          it(`level ${level}`, function () {
            setLoggingLevel(level)
            this.subject.success('🔰', `${message} (level set: ${level})`)
          })
        })
      })
    })
  })
})
