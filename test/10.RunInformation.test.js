/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('./testName')(module)
const { backupDirectoryName, RunInformation } = require('../lib/RunInformation')
const { runDate, testBaseDirectory, testBaseDirectoryName } = require('./constants')
const { RelativePath } = require('../lib/RelativePath')
const { join, relative } = require('node:path')

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function () {
      const subject = new RunInformation(testBaseDirectory, runDate)

      subject.should.be.an.instanceof(RunInformation)

      // baseDirectory
      console.log('baseDirectory:', subject.baseDirectory)
      subject.baseDirectory.should.be.a.String()
      subject.baseDirectory.should.equal(testBaseDirectory)

      // runDate
      console.log('runDate:', subject.runDate)
      subject.runDate.should.be.a.Date()
      subject.runDate.should.equal(runDate)

      // baseDirectoryName
      console.log('baseDirectoryName:', subject.baseDirectoryName)
      subject.baseDirectoryName.should.be.a.String()
      subject.baseDirectoryName.should.equal(testBaseDirectoryName)

      // backupDirectory
      console.log('backupDirectory:', subject.backupDirectory)
      RelativePath.parse(subject.backupDirectory)
      subject.backupDirectory.should.equal(backupDirectoryName)
    })
  })

  describe('instance', function () {
    beforeEach(function () {
      this.subject = new RunInformation(testBaseDirectory, runDate)
    })

    describe('methods', function () {
      describe('#absolutePath', function () {
        it('transforms a path that is a single segment', function () {
          const segment = 'relative-path-segment'
          const result = this.subject.absolutePath(segment)
          relative(result, this.subject.baseDirectory).should.equal('..')
          relative(this.subject.baseDirectory, result).should.equal(segment)
        })
        it('transforms a path that is multi-segment', function () {
          const relativePath = join('first-path-segment', 'second-part', 'third-path-segment')
          const result = this.subject.absolutePath(relativePath)
          relative(result, this.subject.baseDirectory).should.equal(join('..', '..', '..'))
          relative(this.subject.baseDirectory, result).should.equal(relativePath)
        })
      })
    })
  })
})
