/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2022 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const path = require('node:path')
const assert = require('assert')

const namePattern = /^(.*)\.test$/

/**
 * Return the name of a test from the module name.
 * Only use when this makes sense.
 *
 * @param {object} testModule
 * @returns {string}
 */
function testName(testModule) {
  let parts = path.parse(testModule.filename)
  const nameSplit = namePattern.exec(parts.name)
  assert(nameSplit.length === 2)
  let name = nameSplit[1]
  while (path.basename(parts.dir) !== 'test') {
    parts = path.parse(parts.dir)
    name = `${parts.base}/${name}`
  }
  return name
}

module.exports = testName
