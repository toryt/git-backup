/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { resolve, join } = require('node:path')
const { cartesianProduct } = require('../_util/cartesianProduct')
const { loggingLevels, getLoggingLevel } = require('../../lib/Logger')
const {
  baseDirectoryOption,
  credentialsOption,
  logLevelOption,
  credentialsSeparatorOption,
  saltOption
} = require('../../lib/bin/options')
const { testBaseDirectory } = require('../constants')

const humanServiceName = 'humanServiceName'
const shortCredentialsDescription = 'shortCredentialsDescription'
const credentialsDescription = 'credentialsDescription'
const credentialsExample1 = 'credentialsExample1'
const credentialsExample2 = 'credentialsExample2'

const baseArgv = [join('engine', 'path'), join('program', 'path', 'program-name')]

const relativeBaseDirectory = 'this/is/the/base/directory'
const absoluteBaseDirectory = resolve(relativeBaseDirectory)
const credentials1 = 'credentials1'
const credentials2 = 'credentials2'

/**
 * @type {TCLIConfig}
 */
const cliConfig = {
  humanServiceName,
  shortCredentialsDescription,
  credentialsDescription,
  credentialsExample1,
  credentialsExample2
}

const saltExample = 'Is er al zout op de patatten?'
const credentialsSeparatorExample = 'A-SePARAtor'

/**
 * @typedef {object} TArgsCombination
 * @property {string} selectedBaseDirectoryOption
 * @property {string} baseDirectoryValue
 * @property {string} credentialsOption
 * @property {string | undefined} salt
 * @property {string | undefined} credentialsSeparator
 * @property {string | undefined} logLevelOption
 * @property {string} [logLevelValue]
 * @property {Array<string>} argv
 */

/**
 * @type {Array<Omit<TArgsCombination, 'argv'>>}
 */
const combinationsBase = cartesianProduct({
  selectedBaseDirectoryOption: [`--${baseDirectoryOption}`, '-d'],
  baseDirectoryValue: [relativeBaseDirectory, absoluteBaseDirectory],
  credentialsOption: [`--${credentialsOption}`, '-c'],
  salt: [undefined, saltExample],
  credentialsSeparator: [undefined, credentialsSeparatorExample],
  logLevelOption: [`--${logLevelOption}`, '-l', undefined],
  logLevelValue: loggingLevels.filter(ll => ll !== undefined)
}).filter(({ logLevelOption, logLevelValue }) => logLevelOption !== undefined || logLevelValue === getLoggingLevel())

/**
 * @type {Array<TArgsCombination>}
 */
const combinations = combinationsBase.map(c => {
  /** @type {Array<string>} */ const argv = [
    c.selectedBaseDirectoryOption,
    c.baseDirectoryValue,
    c.credentialsOption,
    credentials1,
    c.credentialsOption,
    credentials2
  ]
    .concat(c.salt === undefined ? [] : [`--${saltOption}`, c.salt])
    .concat(c.credentialsSeparator === undefined ? [] : [`--${credentialsSeparatorOption}`, c.credentialsSeparator])
    .concat(c.logLevelOption === undefined ? [] : [c.logLevelOption, c.logLevelValue])

  return /** @type {TArgsCombination} */ {
    ...c,
    argv
  }
})

const nonFunctionalArguments = ['--version', '-v', '--help', '-h']

const argumentFailureCases = [
  [`--${baseDirectoryOption}`, absoluteBaseDirectory, `--${credentialsOption}`, `--${logLevelOption}`, 'error'],
  [`--${baseDirectoryOption}`, absoluteBaseDirectory, `--${logLevelOption}`, 'error'],
  [`--${baseDirectoryOption}`, `--${credentialsOption}`, credentials1, `--${logLevelOption}`, 'error'],
  [`--${credentialsOption}`, credentials1, `--${logLevelOption}`, 'error'],
  [`--${logLevelOption}`, 'error'],
  [],
  [`--${baseDirectoryOption}`, absoluteBaseDirectory, `--${credentialsOption}`, credentials1, `--${logLevelOption}`],
  [
    `--${baseDirectoryOption}`,
    absoluteBaseDirectory,
    `--${credentialsOption}`,
    credentials1,
    `--${logLevelOption}`,
    'wrong-level'
  ]
]

const configDir = join(testBaseDirectory, '..', 'test-config')
const home = resolve(join(testBaseDirectory, '..', 'test-home'))
const configBaseDirectory = 'base/directory'
const configCredentials = ['credentialsA', 'credentialsB']

// TODO credentialsExampleN = credentialsN
module.exports = {
  humanServiceName,
  shortCredentialsDescription,
  credentialsDescription,
  credentialsExample1,
  credentialsExample2,
  baseArgv,
  relativeBaseDirectory,
  absoluteBaseDirectory,
  credentials1,
  credentials2,
  saltExample,
  credentialsSeparatorExample,
  cliConfig,
  combinations,
  nonFunctionalArguments,
  argumentFailureCases,
  configDir,
  home,
  configBaseDirectory,
  configCredentials
}
