/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { exitCode } = require('../../lib/bin/exitCode')
const { inspect } = require('node:util')
const { serviceRunReports } = require('../60.service/ServiceRunReportCommon')

describe(testName, function () {
  /**
   * @param {number} result
   * @param {number} mask
   * @param {boolean} shouldBeSet
   * @returns {void}
   */
  function shouldBitBeSet(result, mask, shouldBeSet) {
    const verification = (result & mask) !== 0
    verification.should.equal(shouldBeSet)
  }

  serviceRunReports.forEach(srr => {
    describe(`service run report ${inspect(srr)}`, function () {
      it(`calculates the exit code when the service run report can be saved`, function () {
        const result = exitCode(srr, true)
        console.log(result)

        result.should.be.a.Number()
        result.should.be.greaterThanOrEqual(0)
        result.should.not.equal(1)
        result.should.be.lessThanOrEqual(255)
        ;(result % 2).should.equal(0)
        shouldBitBeSet(result, 0b00000001, false)
        shouldBitBeSet(result, 0b00000010, srr.wrongCredentials.length > 0)
        shouldBitBeSet(result, 0b00000100, srr.nrOfAccountFailures > 0)
        shouldBitBeSet(
          result,
          0b00001000,
          srr.repositoryRunReports.some(rrr => !rrr.success)
        )
        shouldBitBeSet(result, 0b00010000, srr.unsavedRepositoryRunReports.length > 0)
        shouldBitBeSet(result, 0b00100000, false)
      })

      it(`calculates the exit code when the service run report cannot be saved`, function () {
        const result = exitCode(srr, false)
        console.log(result)

        result.should.be.a.Number()
        result.should.be.greaterThanOrEqual(0)
        result.should.not.equal(1)
        result.should.be.lessThanOrEqual(255)
        ;(result % 2).should.equal(0)
        shouldBitBeSet(result, 0b00000001, false)
        shouldBitBeSet(result, 0b00000010, srr.wrongCredentials.length > 0)
        shouldBitBeSet(result, 0b00000100, srr.nrOfAccountFailures > 0)
        shouldBitBeSet(
          result,
          0b00001000,
          srr.repositoryRunReports.some(rrr => !rrr.success)
        )
        shouldBitBeSet(result, 0b00010000, srr.unsavedRepositoryRunReports.length > 0)
        shouldBitBeSet(result, 0b00100000, true)
      })
    })
  })
})
