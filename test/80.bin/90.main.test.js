/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { Service } = require('../../lib/service/Service')
const { testBaseDirectory, runDate, credentialsCandidates } = require('../constants')
const { stub } = require('sinon')
const { run, main } = require('../../lib/bin/main')
const { serviceRunReports } = require('../60.service/ServiceRunReportCommon')
const { exitCode } = require('../../lib/bin/exitCode')
const { getLoggingLevel, setLoggingLevel } = require('../../lib/Logger')
const { stuffWithUndefined } = require('../_util/stuff')
const testName = require('../testName')(module)
const { inspect } = require('node:util')
const {
  baseArgv,
  absoluteBaseDirectory,
  credentials1,
  cliConfig,
  combinations,
  credentials2,
  argumentFailureCases,
  nonFunctionalArguments
} = require('./constants')
const { errorReport } = require('../../lib/runReport/ErrorReport')
const { adheresTo } = require('../../lib/pre')
const { ServiceRunReport } = require('../../lib/service/ServiceRunReport')
const { environmentVariablePrefix } = require('../../lib/bin/options')
const should = require('should')

const wrongCredentials = 'at least 1 supplied credential was invalid'
const accountFailures = 'at least 1 account could not retrieve repositories'
const mirrorCloneFailures = 'at least 1 repository could not be backed up'
const unsavedRepositoryRunReports = 'at least 1 run report could not be saved'
const unsavedServiceRunReport = 'the service run report could not be saved'

/**
 * @param {TServiceRunReport} serviceRunReport
 * @param {boolean} couldSaveServiceRunReport
 * @returns {Array<string>}
 */
function determineFailures(serviceRunReport, couldSaveServiceRunReport) {
  adheresTo(ServiceRunReport, serviceRunReport)

  const result = []
  if (serviceRunReport.wrongCredentials.length > 0) {
    result.push(wrongCredentials)
  }
  if (serviceRunReport.nrOfAccountFailures > 0) {
    result.push(accountFailures)
  }
  if (serviceRunReport.repositoryRunReports.some(rrr => !rrr.success)) {
    result.push(mirrorCloneFailures)
  }
  if (serviceRunReport.unsavedRepositoryRunReports.length > 0) {
    result.push(unsavedRepositoryRunReports)
  }
  if (!couldSaveServiceRunReport) {
    result.push(unsavedServiceRunReport)
  }

  return result
}

describe(testName, function () {
  describe('run', function () {
    beforeEach(function () {
      this.service = new Service(testBaseDirectory, runDate, credentialsCandidates)
      this.backupStub = stub(this.service, 'backup')
    })

    afterEach(function () {
      this.backupStub.restore()
    })

    describe('can save', function () {
      serviceRunReports.forEach((srr, i) => {
        it(`calls the service \`backup\` method and returns 0 if there are no failures (${i})`, async function () {
          this.backupStub.resolves(srr)

          const result = await run(this.service).should.be.resolved()

          result.should.be.a.Number()
          result.should.equal(exitCode(srr, true))
        })
      })
    })

    describe('cannot save', function () {
      stuffWithUndefined.forEach(error => {
        describe(inspect(error), function () {
          serviceRunReports.forEach((runReport, i) => {
            it(`calls the service \`backup\` method and returns no failures if there are none (${i})`, async function () {
              this.backupStub.rejects({ runReport, error })

              const result = await run(this.service).should.be.resolved()

              result.should.be.a.Number()
              result.should.equal(exitCode(runReport, false))
            })
          })
        })
      })
    })
  })

  describe('main', function () {
    /**
     * @typedef {object} TMainFixtureBase
     * @property {typeof Service} ServiceExample
     * @property {string} originalLoggingLevel
     * @property {SinonStub<[number], void>} exitStub
     * @property {Record<string, string>} existingEnvVariables
     * @property {SinonStub<[], Promise<TServiceRunReport>>} [backupStub]
     * @property {TErrorReport} [errorReport]
     */

    /**
     * @typedef {Context & TMainFixtureBase} TMainFixture
     */

    beforeEach(
      /** @this {TMainFixture} */ function () {
        const self = this
        this.ServiceExample = class ServiceExample extends Service {
          constructor(baseDirectory, runDate, credentialsCandidates, salt) {
            super(baseDirectory, runDate, credentialsCandidates, salt)
            self.serviceExample = this
          }

          async backup() {
            return serviceRunReports[0]
          }
        }
        this.originalLoggingLevel = getLoggingLevel()
        this.exitStub = stub(process, 'exit')

        /* In tests, the program name is 'MOCHA'. In some environments (legacy) 'MOCHA_…' environment variables
           (e.g., 'MOCHA_COLORS') are added, which interfere with the tests. */
        this.existingEnvVariables = Object.entries(process.env).reduce((acc, [name, value]) => {
          if (name.startsWith(`${environmentVariablePrefix}_`)) {
            acc[name] = value
            delete process.env[name]
          }
          return acc
        }, {})
      }
    )

    afterEach(
      /** @this {TMainFixture} */ function () {
        Object.entries(this.existingEnvVariables).forEach(([name, value]) => {
          process.env[name] = value
        })

        setLoggingLevel(this.originalLoggingLevel)
        this.exitStub.restore()
      }
    )

    describe('functional', function () {
      combinations.forEach(({ salt, logLevelValue, argv }) => {
        it(`works for ${argv.join(' ')}`, /** @this {TMainFixture} */ async function () {
          await main(
            cliConfig,
            (baseDirectory, runDate, credentialsCandidates, salt) => {
              baseDirectory.should.equal(absoluteBaseDirectory)
              runDate.should.be.a.Date()
              this.runDate = runDate
              credentialsCandidates.should.deepEqual([credentials1, credentials2])
              getLoggingLevel().should.equal(logLevelValue)

              return new this.ServiceExample(baseDirectory, runDate, credentialsCandidates, salt)
            },
            [...baseArgv, ...argv]
          ).should.be.resolved()

          getLoggingLevel().should.equal(logLevelValue)

          this.serviceExample.runInfo.baseDirectory.should.equal(absoluteBaseDirectory)
          this.serviceExample.runInfo.runDate.should.equal(this.runDate)
          this.serviceExample.credentialsCandidates.should.deepEqual([credentials1, credentials2])
          should(this.serviceExample.desensifier.salt).equal(salt)

          this.exitStub.should.not.have.been.called()
        })

        it(`calls exit(1) with ${argv.join(' ')} and unknown arguments`, /** @this {TMainFixture} */ async function () {
          await main(
            cliConfig,
            (_, runDate) => {
              this.exitStub.should.have.been.calledOnce()
              this.exitStub.should.have.been.calledWithExactly(1)

              /* Because yargs will have called `process.exit`, this will never be reached in production. Here, we do
                 reach it because `process.exit` was stubbed with a NOP. We return something so the process can
                 continue for the test to complete. */
              return new this.ServiceExample(testBaseDirectory, runDate, [])
            },
            [...baseArgv, '-u', ...argv, '-x']
          ).should.be.resolved()
        })
      })
    })

    describe('non-functional', function () {
      nonFunctionalArguments.forEach(c => {
        describe(c, function () {
          it(`calls exit(0) when called with ${c}`, /** @this {TMainFixture} */ async function () {
            await main(
              cliConfig,
              (_, runDate) => {
                this.exitStub.should.have.been.calledOnce()
                this.exitStub.should.have.been.calledWithExactly(0)

                /* Because yargs will have called `process.exit`, this will never be reached in production. Here, we do
                   reach it because `process.exit` was stubbed with a NOP. We return something so the process can
                   continue for the test to complete. */
                return new this.ServiceExample(testBaseDirectory, runDate, [])
              },
              [...baseArgv, c, '-d']
            ).should.be.resolved()
          })

          it(`calls exit(0) when called with ${c}, even with extra arguments`, /** @this {TMainFixture} */ async function () {
            await main(
              cliConfig,
              (_, runDate) => {
                this.exitStub.should.have.been.calledOnce()
                this.exitStub.should.have.been.calledWithExactly(0)

                /* Because yargs will have called `process.exit`, this will never be reached in production. Here, we do
                   reach it because `process.exit` was stubbed with a NOP. We return something so the process can
                   continue for the test to complete. */
                return new this.ServiceExample(testBaseDirectory, runDate, [])
              },
              [...baseArgv, '-y', 'something', c, '-x']
            ).should.be.resolved()
          })
        })
      })
    })

    describe('argument failures', function () {
      argumentFailureCases.forEach(c => {
        it(`fails with insufficient or wrong arguments '${c.join(' ')}'`, /** @this {TMainFixture} */ async function () {
          await main(
            cliConfig,
            (_, runDate) => {
              this.exitStub.should.have.been.calledOnce()
              this.exitStub.should.have.been.calledWithExactly(1)

              /* Because yargs will have called `process.exit`, this will never be reached in production. Here, we do
                 reach it because `process.exit` was stubbed with a NOP. We return something so the process can
                 continue for the test to complete. */
              return new this.ServiceExample(testBaseDirectory, runDate, [])
            },
            [...baseArgv, ...c]
          ).should.be.resolved()
        })
      })
    })

    describe('backup failures', function () {
      const { argv: validArguments } = combinations[0]

      beforeEach(
        /** @this {TMainFixture} */ function () {
          this.errorReport = errorReport(new Error('cannot save service run report'))
        }
      )

      serviceRunReports
        .filter(srr => !srr.success)
        .forEach(srr => {
          const failures = determineFailures(srr, true)
          const exitCodeCanSave = exitCode(srr, true)
          const exitCodeCannotSave = exitCode(srr, false)

          describe(`with failures “${failures.join(', ')}”`, function () {
            it(`exist with error code ${exitCodeCanSave} when the run report can be saved`, /** @this {TMainFixture} */ async function () {
              await main(
                cliConfig,
                (baseDirectory, runDate, credentialsCandidates) => {
                  baseDirectory.should.equal(absoluteBaseDirectory)
                  runDate.should.be.a.Date()
                  this.runDate = runDate
                  credentialsCandidates.should.deepEqual([credentials1, credentials2])

                  const serviceInstance = new this.ServiceExample(baseDirectory, runDate, credentialsCandidates)
                  this.backupStub = stub(serviceInstance, 'backup').resolves(srr)
                  return serviceInstance
                },
                [...baseArgv, ...validArguments]
              ).should.be.resolved()

              this.serviceExample.runInfo.baseDirectory.should.equal(absoluteBaseDirectory)
              this.serviceExample.runInfo.runDate.should.equal(this.runDate)
              this.serviceExample.credentialsCandidates.should.deepEqual([credentials1, credentials2])

              this.exitStub.should.have.been.calledOnce()
              this.exitStub.should.have.been.calledWithExactly(exitCodeCanSave)

              this.backupStub.restore()
            })
            it(`exist with error code ${exitCodeCannotSave} when the run report cannot be saved`, /** @this {TMainFixture} */ async function () {
              await main(
                cliConfig,
                (baseDirectory, runDate, credentialsCandidates) => {
                  baseDirectory.should.equal(absoluteBaseDirectory)
                  runDate.should.be.a.Date()
                  this.runDate = runDate
                  credentialsCandidates.should.deepEqual([credentials1, credentials2])

                  const serviceInstance = new this.ServiceExample(baseDirectory, runDate, credentialsCandidates)
                  /** @type {TUnsavedRunReportThrowable<TServiceRunReport>} */
                  const throwable = { runReport: srr, error: this.errorReport }
                  // noinspection JSCheckFunctionSignatures — type checker selects wrong overload
                  this.backupStub = stub(serviceInstance, 'backup').rejects(throwable)
                  return serviceInstance
                },
                [...baseArgv, ...validArguments]
              ).should.be.resolved()

              this.serviceExample.runInfo.baseDirectory.should.equal(absoluteBaseDirectory)
              this.serviceExample.runInfo.runDate.should.equal(this.runDate)
              this.serviceExample.credentialsCandidates.should.deepEqual([credentials1, credentials2])

              this.exitStub.should.have.been.calledOnce()
              this.exitStub.should.have.been.calledWithExactly(exitCodeCannotSave)

              this.backupStub.restore()
            })
          })
        })
    })
  })
})
