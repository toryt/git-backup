/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const {
  baseDirectoryOption,
  credentialsOption,
  credentialsSeparatorOption,
  logLevelOption,
  environmentVariablePrefix,
  environmentVariableName,
  envAndConfigUsageAddition,
  propertyName
} = require('../../lib/bin/options')
const yargs = require('yargs/yargs')
const testName = require('../testName')(module)

describe(testName, function () {
  const options = [baseDirectoryOption, credentialsOption, credentialsSeparatorOption, logLevelOption]

  describe('options', function () {
    options.forEach(opt => {
      it(`'${opt}' is a valid option`, function () {
        opt.should.be.a.String()
        opt.should.match(/[-a-z0-9]+/)
      })
    })
  })

  describe('propertyName', function () {
    options.forEach(opt => {
      it(`generates a valid property name for ${opt}`, function () {
        const result = propertyName(opt)

        console.log(result)

        result.should.be.a.String()
        result.should.match(/[a-z][A-Za-z0-9]+/)
      })
    })
  })

  describe('environmentVariablePrefix', function () {
    it('is a valid prefix', function () {
      environmentVariablePrefix.should.be.a.String()
      environmentVariablePrefix.should.match(/[A-Z0-9_]+/)
      environmentVariablePrefix.should.not.endWith('_')
      environmentVariablePrefix.should.not.endWith('_JS')
    })
  })

  describe('environmentVariableName', function () {
    const fullPrefix = `${environmentVariablePrefix}_`

    options.forEach(opt => {
      it(`generates an environment friendly name for ${opt} that will be recognized by \`yargs\` as aimed at that option`, function () {
        const result = environmentVariableName(opt)

        console.log(result)

        result.should.be.a.String()
        result.should.match(/[A-Z0-9_]+/)
        result.should.startWith(fullPrefix)
        result.should.not.endWith('_')

        const withoutPrefix = result.replace(fullPrefix, '')
        // noinspection JSUnresolvedReference
        const pName = yargs.Parser.camelCase(withoutPrefix)
        pName.should.equal(propertyName(opt))
      })
    })
  })

  describe('envAndConfigUsageAddition', function () {
    options.forEach(opt => {
      it(`generates a string that tells the name of the option in config files and environment variables`, function () {
        const result = envAndConfigUsageAddition(opt)

        console.log(result)

        result.should.be.a.String()
        result.should.containEql(propertyName(opt))
        result.should.containEql(environmentVariableName(opt))
      })
    })
  })
})
