/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { longString } = require('../../lib/bin/longString')
const { EOL } = require('node:os')
const testName = require('../testName')(module)

const xEol = '\n'
const wEol = '\r\n'

/**
 * @param {Array<string>} strings
 * @param {string} eol
 * @return {string}
 */
function buildMultiLine(strings, eol) {
  return strings.join(eol)
}

/**
 * @param {Array<string>} strings
 * @return {string}
 */
function expected(strings) {
  return strings.reduce((acc, s, i) => acc + (i === 0 ? s ?? '' : s ? (strings[i - 1] ? ' ' : '') + s : EOL + EOL), '')
}

const multiLineString = ['a', 'multi-line', 'string']
const paragraphString = [
  'a',
  'first paragraph',
  undefined,
  'and a second one',
  undefined,
  'followed by a third',
  'paragraph',
  'that is a bit',
  'longer'
]

describe(testName, function () {
  it('can deal with an empty string', function () {
    const result = longString('')
    result.should.equal('')
    result.should.not.containEql('NaN')
  })

  it('can deal with a string without EOLs', function () {
    const s = 'a string without EOLs'
    const result = longString(s)
    console.log(result)
    result.should.equal(s)
  })

  it('converts a multi-line template string to a single line for *x', function () {
    const result = longString(buildMultiLine(multiLineString, xEol))
    console.log(result)
    result.should.equal(expected(multiLineString))
  })

  it('converts a multi-line template string to a single line for windows', function () {
    const result = longString(buildMultiLine(multiLineString, wEol))
    console.log(result)
    result.should.equal(expected(multiLineString))
  })

  it('converts a multi-line template string with blank lines into a multiple lines without blank lines for *x', function () {
    const result = longString(buildMultiLine(paragraphString, xEol))
    console.log(result)
    result.should.equal(expected(paragraphString))
  })

  it('converts a multi-line template string with blank lines into a multiple lines without blank lines for windows', function () {
    const result = longString(buildMultiLine(paragraphString, wEol))
    console.log(result)
    result.should.equal(expected(paragraphString))
  })
})
