/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { stub } = require('sinon')
const os = require('node:os')
const { join, parse, format } = require('node:path')
const { defaultConfigSearchPlaces, attemptToGetDefaultConfig } = require('../../lib/bin/defaultConfig')
const { copy, remove } = require('fs-extra')
const { home, configDir, configBaseDirectory, configCredentials } = require('./constants')
const should = require('should')
const { inspect } = require('node:util')
const {
  baseDirectoryOption,
  credentialsOption,
  credentialsSeparatorOption,
  logLevelOption,
  propertyName
} = require('../../lib/bin/options')
const testName = require('../testName')(module)

describe(testName, function () {
  beforeEach(function () {
    this.exitStub = stub(process, 'exit')
    this.homeStub = stub(os, 'homedir').returns(home)
  })

  afterEach(function () {
    this.exitStub.restore()
    this.homeStub.restore()
    return remove(home)
  })

  describe('no default config file', function () {
    it(`find no default config in ${home}`, function () {
      const result = attemptToGetDefaultConfig()

      console.log(inspect(result))

      result.should.be.an.Object()
      result.should.not.have.property(propertyName(baseDirectoryOption))
      result.should.not.have.property(propertyName(credentialsOption))
      result.should.not.have.property(propertyName(credentialsSeparatorOption))
      result.should.not.have.property(propertyName(logLevelOption))
      result.should.be.empty()

      this.exitStub.should.not.have.been.called()
    })
  })

  describe('when there is a valid default config file', function () {
    defaultConfigSearchPlaces.forEach(searchPlace => {
      describe(`default config file '${searchPlace}'`, function () {
        beforeEach(async function () {
          const defaultConfigFilePath = join(home, searchPlace)
          const { ext } = parse(defaultConfigFilePath)
          const sourceConfigFilePath = format({ dir: configDir, name: 'config', ext })
          return copy(sourceConfigFilePath, defaultConfigFilePath)
        })

        it(`recognizes the default config file and loads it`, function () {
          const result = attemptToGetDefaultConfig()

          console.log(inspect(result))

          result.should.be.an.Object()
          result[propertyName(baseDirectoryOption)].should.equal(configBaseDirectory)
          result[propertyName(credentialsOption)].should.deepEqual(configCredentials)
          result.should.not.have.property(propertyName(credentialsSeparatorOption))
          result.should.not.have.property(propertyName(logLevelOption))

          this.exitStub.should.not.have.been.called()
        })
      })
    })
  })

  describe('existing config file that cannot be parsed', function () {
    beforeEach(async function () {
      const defaultConfigFilePath = join(
        home,
        defaultConfigSearchPlaces.find(dcsp => dcsp.endsWith('.yaml'))
      )
      const sourceConfigFilePath = format({
        dir: configDir,
        name: 'not-a-config-file',
        /* NOTE: the '.' in `ext` is needed for Node 18, but not since Node 20 */
        ext: '.yaml'
      })

      return copy(sourceConfigFilePath, defaultConfigFilePath)
    })

    it(`fails when it fails a default config file it cannot parse`, function () {
      const result = attemptToGetDefaultConfig()

      console.log(result)
      should(result).be.undefined()

      this.exitStub.should.have.been.called()
      this.exitStub.should.have.been.calledWithExactly(1)
    })
  })
})
