/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { argToAbsolutePath, cliArguments, defaultCredentialsSeparator } = require('../../lib/bin/cliArguments')
const { getLoggingLevel, loggingLevels } = require('../../lib/Logger')
const { stub } = require('sinon')
const {
  baseArgv,
  absoluteBaseDirectory,
  credentials1,
  credentials2,
  combinations,
  cliConfig,
  argumentFailureCases,
  nonFunctionalArguments,
  relativeBaseDirectory,
  configDir,
  configBaseDirectory,
  configCredentials,
  credentialsSeparatorExample,
  saltExample
} = require('./constants')
const testName = require('../testName')(module)
const { inspect } = require('node:util')
const { resolve, join, format } = require('node:path')
const should = require('should')
const { cartesianProduct } = require('../_util/cartesianProduct')
const {
  environmentVariablePrefix,
  environmentVariableName,
  baseDirectoryOption,
  credentialsOption,
  credentialsSeparatorOption,
  logLevelOption,
  saltOption
} = require('../../lib/bin/options')
const { configFileNameExtensions } = require('../../lib/bin/defaultConfig')

describe(testName, function () {
  describe('argToAbsolutePath', function () {
    const cwd = resolve('')

    const stuff = [
      { arg: '', path: cwd },
      { arg: '.', path: cwd },
      { arg: '..', path: join(cwd, '..') },
      { arg: './', path: cwd },
      { arg: '/lala', path: resolve('/lala') },
      { arg: '/lala/li', path: resolve('/lala/li') },
      { arg: './lala', path: join(cwd, 'lala') },
      { arg: '../lala', path: join(cwd, '..', 'lala') },
      { arg: '../lala/../lolo/li', path: join(cwd, '..', 'lolo', 'li') },
      { arg: 4, path: join(cwd, '4') },
      { arg: -4, path: join(cwd, '-4') },
      { arg: Math.PI, path: join(cwd, Math.PI.toString()) },
      { arg: 0, path: join(cwd, '0') },
      { arg: true, path: join(cwd, 'true') },
      { arg: false, path: join(cwd, 'false') },
      { arg: undefined, path: undefined }
    ]

    stuff.forEach(({ arg, path }) => {
      it(`works for \`${arg}\``, function () {
        const result = argToAbsolutePath(arg)
        if (arg !== undefined) {
          result.should.be.a.String()
          result.should.equal(path)
        } else {
          should(result).be.undefined()
        }
      })
    })
  })

  describe('cliArguments', function () {
    /**
     * @typedef {object} TCliArgumentsFixtureBase
     * @property {SinonStub<[number], void>} exitStub
     * @property {Record<string, string>} existingEnvVariables
     */

    /**
     * @typedef {Context & TCliArgumentsFixtureBase} TCliArgumentsFixture
     */

    beforeEach(
      /** @this {TCliArgumentsFixture} */ function () {
        this.exitStub = stub(process, 'exit')

        /* In tests, the program name is 'MOCHA'. In some environments (legacy) 'MOCHA_…' environment variables
           (e.g., 'MOCHA_COLORS') are added, which interfere with the tests. */
        this.existingEnvVariables = Object.entries(process.env).reduce((acc, [name, value]) => {
          if (name.startsWith(`${environmentVariablePrefix}_`)) {
            acc[name] = value
            delete process.env[name]
          }
          return acc
        }, {})
      }
    )

    afterEach(
      /** @this {TCliArgumentsFixture} */ function () {
        Object.entries(this.existingEnvVariables).forEach(([name, value]) => {
          process.env[name] = value
        })

        this.exitStub.restore()
      }
    )

    describe('functional', function () {
      describe('no config file, no env variables', function () {
        combinations.forEach(({ salt, credentialsSeparator, logLevelOption, logLevelValue, argv }) => {
          it(`works for ${argv.join(' ')}`, function () {
            const result = cliArguments(cliConfig, {}, [...baseArgv, ...argv])

            result.baseDirectoryPath.should.equal(absoluteBaseDirectory)
            result.logLevel.should.equal(logLevelOption === undefined ? getLoggingLevel() : logLevelValue)
            result.credentials.should.deepEqual([credentials1, credentials2])
            result.credentials.should.deepEqual([credentials1, credentials2])
            should(result.salt).equal(salt)
            result.credentialsSeparator.should.equal(credentialsSeparator ?? defaultCredentialsSeparator)

            this.exitStub.should.not.have.been.called()
          })

          it(`fails with ${argv.join(' ')} and unknown arguments`, function () {
            cliArguments(cliConfig, {}, [...baseArgv, '-u', ...argv, '-x'])

            this.exitStub.should.have.been.calledOnce()
            this.exitStub.should.have.been.calledWithExactly(1)
          })
        })

        it('works when the base directory and account credentials are numbers', function () {
          const result = cliArguments(cliConfig, {}, [...baseArgv, '-d', 137, '-c', 42])

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath('137'))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual(['42'])
          result.should.not.have.property('salt')
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })

        it('works when the base directory and account credentials are booleans', function () {
          const result = cliArguments(cliConfig, {}, [...baseArgv, '-d', true, '-c', false])

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath('true'))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual(['false'])
          result.should.not.have.property('salt')
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })

        it('works when the base directory and account credentials are empty strings', function () {
          const result = cliArguments(cliConfig, {}, [...baseArgv, '-d', '', '-c', ''])

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath(''))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual([''])
          result.should.not.have.property('salt')
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })

        it('works when the base directory and account credentials are only double quotes (which is just a regular string)', function () {
          const result = cliArguments(cliConfig, {}, [...baseArgv, '-d', '""', '-c', '""'])

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath('""'))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual(['""'])
          result.should.not.have.property('salt')
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })

        it('works when the salt is the empty string', function () {
          const result = cliArguments(cliConfig, {}, [...baseArgv, '-d', absoluteBaseDirectory, '-c', credentials1])

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(absoluteBaseDirectory)
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual([credentials1])
          result.should.not.have.property('salt')
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })
      })

      describe('when there is a valid default config file, no env variables', function () {
        it(`recognizes the default config file and loads it`, function () {
          const result = cliArguments(
            cliConfig,
            {
              baseDirectory: configBaseDirectory,
              credentials: configCredentials,
              salt: saltExample
            },
            [...baseArgv]
          )

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath(configBaseDirectory))
          result.credentials.should.deepEqual(configCredentials)
          result.salt.should.equal(saltExample)
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })
      })

      describe('with config file argument, no env variables', function () {
        configFileNameExtensions.forEach(ext => {
          it(`recognizes when a config file is given with extension '${ext}', and loads it`, function () {
            const result = cliArguments(cliConfig, {}, [
              ...baseArgv,
              '--config',
              format({ dir: configDir, name: 'config', ext })
            ])

            console.log(inspect(result))
            result.baseDirectoryPath.should.equal(argToAbsolutePath(configBaseDirectory))
            result.credentials.should.deepEqual(configCredentials)
            result.salt.should.equal(saltExample)
            result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

            this.exitStub.should.not.have.been.called()
          })
        })
      })

      describe('env variables, no config file, no arguments', function () {
        const baseDirectoryEnvVariableName = environmentVariableName(baseDirectoryOption)
        const credentialsEnvVariableName = environmentVariableName(credentialsOption)
        const saltEnvVariableName = environmentVariableName(saltOption)
        const credentialsSeparatorEnvVariableName = environmentVariableName(credentialsSeparatorOption)
        const logLevelEnvVariableName = environmentVariableName(logLevelOption)

        afterEach(function () {
          delete process.env[baseDirectoryEnvVariableName]
          delete process.env[credentialsEnvVariableName]
          delete process.env[saltEnvVariableName]
          delete process.env[credentialsSeparatorEnvVariableName]
          delete process.env[logLevelEnvVariableName]
        })

        const cases = cartesianProduct({
          baseDirectoryValue: [relativeBaseDirectory, absoluteBaseDirectory],
          salt: [undefined, saltExample],
          credentialsSeparator: [undefined, credentialsSeparatorExample],
          logLevelValue: loggingLevels.filter(ll => ll !== undefined)
        })

        cases.forEach(({ baseDirectoryValue, salt, credentialsSeparator, logLevelValue }) => {
          it(`works for ${baseDirectoryValue} with credentialsSeparator '${credentialsSeparator}' and log level ${logLevelValue}`, function () {
            process.env[baseDirectoryEnvVariableName] = baseDirectoryValue
            process.env[credentialsEnvVariableName] = [credentials1, credentials2].join(
              credentialsSeparator ?? defaultCredentialsSeparator
            )
            if (salt !== undefined) {
              process.env[saltEnvVariableName] = salt
            }
            console.log('accountEnvVariable', process.env[credentialsEnvVariableName])
            if (credentialsSeparator !== undefined) {
              process.env[credentialsSeparatorEnvVariableName] = credentialsSeparator
            }
            process.env[logLevelEnvVariableName] = logLevelValue

            const result = cliArguments(cliConfig, {}, baseArgv)

            result.baseDirectoryPath.should.equal(absoluteBaseDirectory)
            result.logLevel.should.equal(logLevelValue)
            result.credentials.should.deepEqual([credentials1, credentials2])
            should(result.salt).equal(salt)
            result.credentialsSeparator.should.equal(credentialsSeparator ?? defaultCredentialsSeparator)

            this.exitStub.should.not.have.been.called()
          })
        })

        it('works when the base directory and account credentials are numbers', function () {
          process.env[baseDirectoryEnvVariableName] = '137'
          process.env[credentialsEnvVariableName] = '42'

          const result = cliArguments(cliConfig, {}, baseArgv)

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath('137'))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual(['42'])
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })

        it('works when the base directory and account credentials are booleans', function () {
          process.env[baseDirectoryEnvVariableName] = 'true'
          process.env[credentialsEnvVariableName] = 'false'

          const result = cliArguments(cliConfig, {}, baseArgv)

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath('true'))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual(['false'])
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })

        it('works when the base directory and account credentials are empty strings', function () {
          process.env[baseDirectoryEnvVariableName] = ''
          process.env[credentialsEnvVariableName] = ''

          const result = cliArguments(cliConfig, {}, baseArgv)

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath(''))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual([''])

          this.exitStub.should.not.have.been.called()
        })

        it('works when the base directory and account credentials are only double quotes (which is just a regular string)', function () {
          process.env[baseDirectoryEnvVariableName] = '""'
          process.env[credentialsEnvVariableName] = '""'

          const result = cliArguments(cliConfig, {}, baseArgv)

          console.log(inspect(result))
          result.baseDirectoryPath.should.equal(argToAbsolutePath('""'))
          result.logLevel.should.equal(getLoggingLevel())
          result.credentials.should.deepEqual(['""'])
          result.credentialsSeparator.should.equal(defaultCredentialsSeparator)

          this.exitStub.should.not.have.been.called()
        })
      })
    })

    describe('non-functional', function () {
      nonFunctionalArguments.forEach(c => {
        describe(c, function () {
          it(`returns undefined and calls exit(0) when called with ${c}`, function () {
            cliArguments(cliConfig, {}, [...baseArgv, c, '-d'])

            // the result does not interest us. `exit` has been called
            // might have been called several times, but unstubbed only the first call would have effect
            this.exitStub.should.have.been.called()
            this.exitStub.should.have.been.calledWithExactly(0)
          })

          it(`returns undefined and calls exit(0) when called with ${c}, even with extra arguments`, function () {
            cliArguments(cliConfig, {}, [...baseArgv, '-y', 'something', c, '-x'])

            // the result does not interest us. `exit` has been called
            // might have been called several times, but unstubbed only the first call would have effect
            this.exitStub.should.have.been.called()
            this.exitStub.should.have.been.calledWithExactly(0)
          })
        })
      })
    })

    describe('failures', function () {
      describe('arguments', function () {
        argumentFailureCases.forEach(c => {
          it(`fails with insufficient or wrong arguments '${c.join(' ')}'`, function () {
            cliArguments(cliConfig, {}, [...baseArgv, ...c])

            // the result does not interest us. `exit` has been called
            // might have been called several times, but unstubbed only the first call would have effect
            this.exitStub.should.have.been.called()
            this.exitStub.should.have.been.calledWithExactly(1)
          })
        })
      })

      describe('config', function () {
        it('recognizes when a config file is given, and fails if that file is not found', function () {
          const result = cliArguments(cliConfig, {}, [
            ...baseArgv,
            '--config',
            join('this', 'file', 'does', 'not', 'exist')
          ])

          // TODO this shows an ugly NOENT message
          console.log(inspect(result))
          this.exitStub.should.have.been.called()
          this.exitStub.should.have.been.calledWithExactly(1)
        })

        it('recognizes when a config file is given, and fails when it has an unsupported extension', function () {
          const result = cliArguments(cliConfig, {}, [...baseArgv, '--config', join(configDir, 'not-a-config-file.md')])

          console.log(inspect(result))
          this.exitStub.should.have.been.called()
          this.exitStub.should.have.been.calledWithExactly(1)
        })

        it('recognizes when a config file is given, and fails when it has a supported extension but cannot be parsed', function () {
          const result = cliArguments(cliConfig, {}, [
            ...baseArgv,
            '--config',
            join(configDir, 'not-a-config-file.yaml')
          ])

          console.log(inspect(result))
          this.exitStub.should.have.been.called()
          this.exitStub.should.have.been.calledWithExactly(1)
        })
      })
    })
  })
})
