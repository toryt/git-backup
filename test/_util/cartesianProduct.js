/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/**
 * Function that calculates the Cartesian product of the provided arrays.
 *
 * Each instance in the result is an object with properties corresponding to keys of the input object. Values are picked
 * from the arrays associated with the keys in the `values` argument.
 *
 * @template {object} T
 * @param { { [P in keyof T]: Array<T[P]> } } baseValuesCandidates
 * @returns {Array<T>}
 */
function cartesianProduct(baseValuesCandidates) {
  const keys = Object.keys(baseValuesCandidates)
  const arrays = Object.values(baseValuesCandidates)
  return arrays.reduce((a, b, i) => a.flatMap(x => b.map(y => ({ ...x, [keys[i]]: y }))), [{}])
}

module.exports = { cartesianProduct }
