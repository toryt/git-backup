/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/**
 * @type {Record<string, unknown>}
 */
const circularObject = {
  aProperty: 'a property',
  anotherProperty: 137,
  anObjectProperty: { anObjectPropertyProperty: 'an object property' },
  anArrayProperty: [{ anArrayObjectPropertyProperty: 'an array object property' }]
}
circularObject.anObjectProperty.cicrularObject = circularObject
circularObject.anArrayProperty.push(circularObject.anObjectProperty)

const circularError = new Error('an error with a circular data structure')
circularError.details = circularObject

const simpleErrorMessage = 'a simple error'

/**
 * @type {Array<unknown>}
 */
const stuffWithoutCircular = [
  null,
  Number.NEGATIVE_INFINITY,
  Number.MIN_VALUE,
  Number.MIN_SAFE_INTEGER,
  -4.15,
  -1,
  0,
  Number.EPSILON,
  1,
  Math.E,
  Math.PI,
  Number.MAX_SAFE_INTEGER,
  Number.MAX_VALUE,
  Number.POSITIVE_INFINITY,
  Number.NaN,
  '',
  'a string',
  '  untrimmed string',
  'untrimmed string  ',
  `a
multilined
string`,
  true,
  false,
  77489299904084890200984800029003004n,
  Symbol('symbol'),
  function aStuffFunction() {},
  {},
  [],
  new Date(663, 2, 23, 14, 23, 34, 4343),
  new Error(simpleErrorMessage)
]

/**
 * @type {Array<unknown>}
 */
const stuffWithoutCircularWithUndefined = stuffWithoutCircular.concat([undefined])

/**
 * @type {Array<unknown>}
 */
const circularStuff = [circularObject, circularError, [circularObject], [circularObject]]

/**
 * @type {Array<unknown>}
 */
const stuff = stuffWithoutCircular.concat(circularStuff)

/**
 * @type {Array<unknown>}
 */
const stuffWithUndefined = [undefined].concat(stuff)

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notAString(s) {
  return typeof s !== 'string'
}

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notANonEmptyString(s) {
  return typeof s !== 'string' || s === ''
}

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notATrimmedString(s) {
  return typeof s !== 'string' || s.trim() !== s || s === ''
}

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notAnEmptyArray(s) {
  return !Array.isArray(s) || s.length > 0
}

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notABoolean(s) {
  return typeof s !== 'boolean'
}

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notANonNegativeSafeInteger(s) {
  return typeof s !== 'number' || !Number.isFinite(s) || !Number.isSafeInteger(s) || s < 0
}

/**
 * @param {unknown} s
 * @returns {boolean}
 */
function notASimpleError(s) {
  return !(s instanceof Error) || s.message !== simpleErrorMessage
}

module.exports = {
  circularObject,
  circularError,
  stuffWithoutCircularWithUndefined,
  circularStuff,
  stuff,
  stuffWithUndefined,
  notAString,
  notANonEmptyString,
  notATrimmedString,
  notAnEmptyArray,
  notABoolean,
  notANonNegativeSafeInteger,
  notASimpleError
}
