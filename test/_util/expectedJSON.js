/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/**
 * See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify#description
 *
 * @param {unknown} v
 * @param {boolean} [inArray]
 * @returns {TJSONValue}
 */
function expectedJSON(v, inArray) {
  if (typeof v === 'bigint') {
    throw new Error('bigint cannot be stringified')
  }

  if (v === undefined || typeof v === 'function' || typeof v === 'symbol') {
    return inArray ? null : undefined
  }

  if (v === null || v === Number.NEGATIVE_INFINITY || v === Number.POSITIVE_INFINITY || Number.isNaN(v)) {
    return null
  }

  if (Array.isArray(v)) {
    return v.map(av => expectedJSON(av, true))
  }

  if (typeof v === 'object') {
    return Object.entries(v).reduce((acc, [key, value]) => {
      const jsonValue = expectedJSON(value)
      if (jsonValue !== undefined) {
        acc[key] = jsonValue
      }
      return acc
    }, {})
  }

  return v
}

module.exports = { expectedJSON }
