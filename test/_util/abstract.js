/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { abstractPropertyMessage, abstractMethodMessage } = require('../../lib/abstract')
const should = require('should')

/**
 * Expects `this.subject` to have an abstract property `propertyName`.
 *
 * @param {string} propertyName
 * @returns {void}
 */
function shouldBeAbstractProperty(propertyName) {
  it(`${propertyName} is abstract`, function () {
    let failed = true
    let value
    try {
      value = this.subject[propertyName]
    } catch (err) {
      failed = false
      err.should.be.an.Error()
      err.message.should.equal(abstractPropertyMessage)
    }
    if (failed) {
      // noinspection JSUnresolvedReference
      const assertion = new should.Assertion(propertyName)
      assertion.params = {
        operator: `to be an abstract property (value: ${JSON.stringify(value)})`
      }
      assertion.fail()
    }
  })
}

/**
 * Expects `this.subject` to have an abstract sync method `methodName`.
 *
 * @param {string} propertyName
 * @returns {void}
 */
function shouldBeAbstractMethod(propertyName) {
  it(`${propertyName} is abstract`, async function () {
    return this.subject[propertyName].should.throwError(abstractMethodMessage)
  })
}

/**
 * Expects `this.subject` to have an abstract async method `methodName`.
 *
 * @param {string} methodName
 * @returns {void}
 */
function shouldBeAbstractAsyncMethod(methodName) {
  it(`${methodName} is abstract`, async function () {
    // noinspection JSCheckFunctionSignatures — d.ts is wrong
    return this.subject[methodName]().should.be.rejectedWith(Error, { message: abstractMethodMessage })
  })
}

module.exports = { shouldBeAbstractProperty, shouldBeAbstractMethod, shouldBeAbstractAsyncMethod }
