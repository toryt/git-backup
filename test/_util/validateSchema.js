/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { ZodType, ZodError } = require('zod')
const { inspect } = require('node:util')

/**
 * @param {ZodType} schema
 * @param {Array<unknown>} examples
 * @param {Array<unknown>} failures
 */
function validateSchema(schema, examples, failures) {
  it('is a Zod schema', function () {
    schema.should.be.instanceof(ZodType)
  })
  describe('examples', function () {
    examples.forEach(eg => {
      it(`should pass ${inspect(eg)}`, function () {
        const result = schema.parse(eg)
        console.log(inspect(result))
      })
    })
  })
  describe('failures', function () {
    failures.forEach(f => {
      it(`should not pass ${inspect(f)}`, function () {
        const result = schema.safeParse(f)
        result.should.be.an.Object()
        result.success.should.be.false()
        result.error.should.be.an.instanceof(ZodError)
        try {
          const message = result.error.message
          message.should.be.a.String()
          console.log(message)
        } catch (err) {
          /* There is a bug in Zod.
             https://github.com/colinhacks/zod/issues/3422
             This works around that in tests, but this might crash our code when reporting on Zod errors in reports. */
          err.should.be.an.instanceof(TypeError)
          console.log(err)
        }
      })
    })
  })
}

module.exports = { validateSchema }
