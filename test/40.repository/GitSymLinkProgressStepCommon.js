/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { symLink } = require('../../lib/repository/GitSymLinkProgressStep')
const { errorReports } = require('../30.runReport/ErrorReportCommon')

/**
 * @type {TGitSymLinkBase}
 */
const gitSymLinkBase = {
  action: symLink,
  start: '2023-11-04T18:27:27.758Z',
  end: '2023-11-04T18:27:30.758Z',
  duration: 'PT3S',
  success: true
}

/**
 * @type {TGitSymLinkSuccess}
 */
const gitSymLinkSuccessOverwrite = { ...gitSymLinkBase, overwritten: true, success: true }

/**
 * @type {TGitSymLinkSuccess}
 */
const gitSymLinkSuccessNoOverwrite = { ...gitSymLinkBase, overwritten: false, success: true }

/**
 * @type {Array<TGitSymLinkSuccess>}
 */
const gitSymLinkSuccesses = [gitSymLinkSuccessOverwrite, gitSymLinkSuccessNoOverwrite]

/**
 * @type {TGitSymLinkFailure}
 */
const gitSymLinkFailure = { ...gitSymLinkBase, success: false, error: errorReports[5] }

module.exports = {
  gitSymLinkSuccessOverwrite,
  gitSymLinkSuccessNoOverwrite,
  gitSymLinkSuccesses,
  gitSymLinkFailure
}
