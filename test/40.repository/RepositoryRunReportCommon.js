/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { owner } = require('../constants')
const { idInService, baseRelativeEmptyPath, repositoryName, gitSymLink, testRepoRemoteURL } = require('./constants')
const { runReportBase } = require('../30.runReport/RunReportCommon')
const { successProgresses, failureProgresses } = require('./ProgressCommon')

/**
 * @type {Omit<TRepositoryRunReport, 'success' | 'progress'>}}
 */
const repositoryRunReportBase = {
  ...runReportBase,
  id: idInService,
  owner,
  name: repositoryName,
  mirrorClone: baseRelativeEmptyPath,
  remoteURL: testRepoRemoteURL,
  gitSymLink
}

/**
 * @type {Array<TSuccessRepositoryRunReport>}
 */
const successRepositoryRunReports = successProgresses.map(progress => ({
  ...repositoryRunReportBase,
  progress,
  success: true
}))

/**
 * @type {Array<TFailureRepositoryRunReport>}
 */
const failureRepositoryRunReports = failureProgresses.map(progress => ({
  ...repositoryRunReportBase,
  progress,
  success: false
}))

/**
 * @type {Array<TRepositoryRunReport>}
 */
const repositoryRunReports = /** @type {Array<TRepositoryRunReport>} */ successRepositoryRunReports.concat(
  /** @type {Array<TRepositoryRunReport>} */ failureRepositoryRunReports
)

module.exports = {
  successRepositoryRunReports,
  failureRepositoryRunReports,
  repositoryRunReports
}
