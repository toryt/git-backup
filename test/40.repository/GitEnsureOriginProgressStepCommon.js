/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { ensureOrigin } = require('../../lib/repository/GitEnsureOriginProgressStep')
const { errorReports } = require('../30.runReport/ErrorReportCommon')

/**
 * @type {TGitEnsureOriginBase}
 */
const gitEnsureOriginBase = {
  action: ensureOrigin,
  start: '2024-01-07T14:29:26.419Z',
  end: '2024-01-07T14:29:27.419Z',
  duration: 'PT1S',
  success: true
}

/**
 * @type {TGitEnsureOriginSuccess}
 */
const gitEnsureOriginSuccess = {
  ...gitEnsureOriginBase,
  success: true
}

/**
 * @type {TGitEnsureOriginFailure}
 */
const gitEnsureOriginFailurePathExists = {
  ...gitEnsureOriginBase,
  success: false,
  pathExists: true,
  error: errorReports[0]
}

/**
 * @type {TGitEnsureOriginFailure}
 */
const gitEnsureOriginFailurePathDoesNotExist = {
  ...gitEnsureOriginBase,
  success: false,
  pathExists: false,
  error: errorReports[0]
}

/**
 * @type {Array<TGitEnsureOriginFailure>}
 */
const gitEnsureOriginFailures = [gitEnsureOriginFailurePathExists, gitEnsureOriginFailurePathDoesNotExist]

module.exports = {
  gitEnsureOriginSuccess,
  gitEnsureOriginFailurePathExists,
  gitEnsureOriginFailurePathDoesNotExist,
  gitEnsureOriginFailures
}
