/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { stuffWithUndefined, notANonEmptyString, notATrimmedString, notABoolean } = require('../_util/stuff')
const { validateSchema } = require('../_util/validateSchema')
const {
  SuccessRepositoryRunReport,
  FailureRepositoryRunReport,
  RepositoryRunReport
} = require('../../lib/repository/RepositoryRunReport')
const { successProgresses, failureProgresses } = require('./ProgressCommon')
const {
  successRepositoryRunReports,
  failureRepositoryRunReports,
  repositoryRunReports
} = require('./RepositoryRunReportCommon')

/**
 * @param {TRepositoryRunReport} base
 * @returns {Array<unknown>}
 */
function badBases(base) {
  return stuffWithUndefined
    .concat(stuffWithUndefined.filter(s => s !== 1).map(v => ({ ...base, v })))
    .concat(stuffWithUndefined.filter(notATrimmedString).map(service => ({ ...base, service })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(id => ({ ...base, id })))
    .concat(stuffWithUndefined.filter(notATrimmedString).map(owner => ({ ...base, owner })))
    .concat(stuffWithUndefined.filter(notATrimmedString).map(name => ({ ...base, name })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(mirrorClone => ({ ...base, mirrorClone })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(remoteURL => ({ ...base, remoteURL })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(gitSymLink => ({ ...base, gitSymLink })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(start => ({ ...base, start })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(end => ({ ...base, end })))
    .concat(stuffWithUndefined.filter(notANonEmptyString).map(duration => ({ ...base, duration })))
    .concat(stuffWithUndefined.filter(notABoolean).map(success => ({ ...base, success })))
}

describe(testName, function () {
  describe('SuccessRepositoryRunReport', function () {
    validateSchema(
      SuccessRepositoryRunReport,
      successRepositoryRunReports,
      badBases(successRepositoryRunReports[0])
        .concat(
          stuffWithUndefined
            .concat(failureProgresses)
            .map(progress => ({ ...successRepositoryRunReports[0], progress }))
        )
        .concat(
          stuffWithUndefined.filter(s => s !== true).map(success => ({ ...successRepositoryRunReports[0], success }))
        )
    )
  })

  describe('FailureRepositoryRunReport', function () {
    validateSchema(
      FailureRepositoryRunReport,
      failureRepositoryRunReports,
      badBases(failureRepositoryRunReports[0])
        .concat(
          stuffWithUndefined
            .concat(successProgresses)
            .map(progress => ({ ...failureRepositoryRunReports[0], progress }))
        )
        .concat(
          stuffWithUndefined.filter(s => s !== false).map(success => ({ ...failureRepositoryRunReports[0], success }))
        )
    )
  })

  describe('RepositoryRunReport', function () {
    validateSchema(RepositoryRunReport, repositoryRunReports, stuffWithUndefined)
  })
})
