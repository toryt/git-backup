/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const {
  unsafePathSegmentStringExamples,
  safePathSegmentExamples,
  SafePathSegment
} = require('../../SafePathSegmentCommon')
const testName = require('../../testName')(module)
const { inspect } = require('node:util')
const { makePathSegmentSafe } = require('../../../lib/repository/makePathSegmentSafe')
const { forbiddenWindowsChars } = require('../../../lib/RelativePath')

const escapePattern = /%u([0-9A-F]{6})/g

/**
 * @param {string} s
 * @returns {string}
 */
function deEscape(s) {
  return s.replace(escapePattern, (_, hexDigits) => String.fromCodePoint(Number.parseInt(hexDigits, 16)))
}

describe(testName, function () {
  describe('does nothing', function () {
    safePathSegmentExamples.concat(['.a', '..a', 'a.', 'a..', 'a.b', 'a..b', 'a...b', 'u000000']).forEach(sps => {
      it(`does nothing to ${sps}`, function () {
        const result = makePathSegmentSafe(sps)
        console.log(result)
        forbiddenWindowsChars.forEach(c => {
          result.should.not.containEql(c)
        })
        result.should.not.match(/\p{C}/iu)
        result.should.not.match(/%[^u]/)
        SafePathSegment.parse(result)
        result.should.equal(sps)
        const back = deEscape(result)
        back.should.equal(sps)
      })
    })
  })

  describe('escapes', function () {
    unsafePathSegmentStringExamples
      .filter(s => s !== '')
      .concat([
        'abc%def',
        'abc%uvw',
        'abc%u123456xyz',
        '%000000',
        '%u00000',
        '%u0000',
        '%u000',
        '%u00',
        '%u0',
        '%u00000'
      ])
      .forEach(uspss => {
        it(`percent-escapes ${inspect(uspss)}`, function () {
          const result = makePathSegmentSafe(uspss)
          console.log(inspect(result))
          SafePathSegment.parse(result)
          result.should.not.equal(uspss)
          const back = deEscape(result)
          back.should.equal(uspss)
        })
      })
  })
})
