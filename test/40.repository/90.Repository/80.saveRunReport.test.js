/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { ensureDir, remove, ensureFile } = require('fs-extra')
const testName = require('../../testName')(module)
const { successRepositoryRunReports, failureRepositoryRunReports } = require('../RepositoryRunReportCommon')
const { createGitRepository } = require('../RepositoryCommon')
const { RepositoryRunReport } = require('../../../lib/repository/RepositoryRunReport')
const { join } = require('node:path')
const {
  runReportSuccessFileName,
  runReportFailureFileName,
  isoYMDPath
} = require('../../../lib/runReport/saveRunReport')
const { UnsavedRunReportThrowable } = require('../../../lib/runReport/UnsavedRunReportThrowable')
const { shouldBeExpectedRunReport } = require('../../30.runReport/RunReportCommon')

describe(testName, function () {
  beforeEach(async function () {
    const { repository } = createGitRepository()
    this.subject = repository
    this.mirrorCloneAbsolutePath = this.subject.runInfo.absolutePath(this.subject.mirrorClonePath)
    this.runReportDirectoryAbsolutePath = this.subject.runInfo.absolutePath(this.subject.runReportDirectory)
    return remove(this.mirrorCloneAbsolutePath)
  })

  afterEach(async function () {
    return remove(this.mirrorCloneAbsolutePath)
  })

  it('saves a success run report in the expected place', async function () {
    await this.subject.saveRunReport(successRepositoryRunReports[0]).should.be.resolved()

    return shouldBeExpectedRunReport(
      join(
        this.runReportDirectoryAbsolutePath,
        isoYMDPath(this.subject.runInfo),
        runReportSuccessFileName(this.subject.runInfo)
      ),
      successRepositoryRunReports[0],
      this.subject.desensifier,
      RepositoryRunReport
    )
  })
  it('saves a failure run report in the expected place', async function () {
    await this.subject.saveRunReport(failureRepositoryRunReports[0]).should.be.resolved()

    return shouldBeExpectedRunReport(
      join(
        this.runReportDirectoryAbsolutePath,
        isoYMDPath(this.subject.runInfo),
        runReportFailureFileName(this.subject.runInfo)
      ),
      failureRepositoryRunReports[0],
      this.subject.desensifier,
      RepositoryRunReport
    )
  })
  it('saves a run report in the expected place when the run directory already exists', async function () {
    await ensureDir(this.subject.runInfo.absolutePath(this.subject.runReportDirectory))

    await this.subject.saveRunReport(successRepositoryRunReports[1]).should.be.resolved()

    return shouldBeExpectedRunReport(
      join(
        this.runReportDirectoryAbsolutePath,
        isoYMDPath(this.subject.runInfo),
        runReportSuccessFileName(this.subject.runInfo)
      ),
      successRepositoryRunReports[1],
      this.subject.desensifier,
      RepositoryRunReport
    )
  })
  it('throws when it cannot save a run report in the expected place', async function () {
    await ensureFile(
      join(
        this.runReportDirectoryAbsolutePath,
        isoYMDPath(this.subject.runInfo),
        runReportFailureFileName(this.subject.runInfo)
      )
    )

    const result = await this.subject.saveRunReport(failureRepositoryRunReports[1]).should.be.rejected()

    UnsavedRunReportThrowable.parse(result)
    result.runReport.should.deepEqual(this.subject.desensifier.desensify(failureRepositoryRunReports[1]))
    result.error.message.should.containEql('EEXIST')
  })
})
