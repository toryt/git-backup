/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { stub } = require('sinon')
const testName = require('../../testName')(module)
const {
  gitEnsureOriginSuccess,
  gitEnsureOriginFailurePathDoesNotExist,
  gitEnsureOriginFailurePathExists
} = require('../GitEnsureOriginProgressStepCommon')
const { gitUpdateSuccess, gitUpdateFailure } = require('../GitUpdateProgressStepCommon')
const {
  gitSymLinkSuccessOverwrite,
  gitSymLinkSuccessNoOverwrite,
  gitSymLinkFailure
} = require('../GitSymLinkProgressStepCommon')
const { createGitRepository } = require('../RepositoryCommon')
const { RepositoryRunReport } = require('../../../lib/repository/RepositoryRunReport')
const { shouldBeStringifiable } = require('../../_util/shouldBeStringifiable')
const { errorReports } = require('../../30.runReport/ErrorReportCommon')

describe(testName, function () {
  /**
   * @param {Repository} repository
   * @param {TRepositoryRunReport} runReport
   * @param {TSuccessProgress | TFailureProgress} progress
   * @param {boolean} failure
   * @returns {void}
   */
  function shouldBeExpectedRunReport(repository, runReport, progress, failure) {
    RepositoryRunReport.parse(runReport)
    runReport.id.should.equal(repository.id)
    runReport.mirrorClone.should.equal(repository.mirrorClonePath)
    runReport.remoteURL.should.equal(repository.remoteURL)
    runReport.gitSymLink.should.equal(repository.gitSymLinkPath)
    runReport.progress.should.deepEqual(progress)
    runReport.success.should.equal(!failure)

    shouldBeStringifiable(runReport)
  }

  beforeEach(function () {
    const { repository } = createGitRepository()
    this.subject = repository
    this.gitCreateStub = stub(this.subject, 'gitCreate')
    this.gitEnsureOriginStub = stub(this.subject, 'gitEnsureOrigin')
    this.gitUpdateStub = stub(this.subject, 'gitUpdate')
    this.ensureGitSymLinkStub = stub(this.subject, 'ensureGitSymLink')
  })

  afterEach(function () {
    this.ensureGitSymLinkStub.restore()
    this.gitUpdateStub.restore()
    this.gitEnsureOriginStub.restore()
    this.gitCreateStub.restore()
  })

  describe('mirror clone directory exists, and origin can be set', function () {
    describe('origin is the same', function () {
      beforeEach(function () {
        this.gitEnsureOriginStub.resolves(gitEnsureOriginSuccess)
      })

      describe('update is successful', function () {
        beforeEach(function () {
          this.gitUpdateStub.resolves(gitUpdateSuccess)
        })

        it('does not call gitCreate and resolves the ensure origin, update progress and symbolic link with overwrite progress', async function () {
          this.ensureGitSymLinkStub.resolves(gitSymLinkSuccessOverwrite)

          const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
          shouldBeExpectedRunReport(
            this.subject,
            runReport,
            [gitEnsureOriginSuccess, gitUpdateSuccess, gitSymLinkSuccessOverwrite],
            false
          )

          this.gitEnsureOriginStub.should.have.been.calledOnce()
          this.gitUpdateStub.should.have.been.calledOnce()
          this.ensureGitSymLinkStub.should.have.been.calledOnce()
          this.gitCreateStub.should.not.have.been.called()
        })

        it('does not call gitCreate and resolves the ensure origin, update progress and symbolic link with no-overwrite progress', async function () {
          this.ensureGitSymLinkStub.resolves(gitSymLinkSuccessNoOverwrite)

          const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
          shouldBeExpectedRunReport(
            this.subject,
            runReport,
            [gitEnsureOriginSuccess, gitUpdateSuccess, gitSymLinkSuccessNoOverwrite],
            false
          )

          this.gitEnsureOriginStub.should.have.been.calledOnce()
          this.gitUpdateStub.should.have.been.calledOnce()
          this.gitCreateStub.should.not.have.been.called()
          this.ensureGitSymLinkStub.should.have.been.calledOnce()
        })

        it('does not call gitCreate and resolves the ensure origin, update progress and symbolic link failure progress', async function () {
          this.ensureGitSymLinkStub.rejects(gitSymLinkFailure)

          const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
          shouldBeExpectedRunReport(
            this.subject,
            runReport,
            [gitEnsureOriginSuccess, gitUpdateSuccess, gitSymLinkFailure],
            true
          )

          this.gitEnsureOriginStub.should.have.been.calledOnce()
          this.gitUpdateStub.should.have.been.calledOnce()
          this.gitCreateStub.should.not.have.been.called()
          this.ensureGitSymLinkStub.should.have.been.calledOnce()
        })
      })

      describe('update fails', function () {
        beforeEach(function () {
          this.gitUpdateStub.rejects(gitUpdateFailure)
        })

        it('does not call gitCreate or ensureGitSymLink and throws the ensure origin and update failure progress', async function () {
          const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
          shouldBeExpectedRunReport(this.subject, runReport, [gitEnsureOriginSuccess, gitUpdateFailure], true)

          this.gitEnsureOriginStub.should.have.been.calledOnce()
          this.gitUpdateStub.should.have.been.calledOnce()
          this.gitCreateStub.should.not.have.been.called()
          this.ensureGitSymLinkStub.should.not.have.been.called()
        })
      })
    })
  })

  describe('mirror clone does not exist', function () {
    beforeEach(function () {
      /** @type {TGitCreateSuccess} */
      this.gitCreateProgress = {
        action: 'create',
        success: true,
        start: '2023-10-07T08:26:46.225Z',
        end: '2023-10-07T08:26:47.225Z',
        duration: 'PT1S',
        remoteURL: this.subject.remoteURL
      }
      this.gitEnsureOriginStub.rejects(gitEnsureOriginFailurePathDoesNotExist)
    })

    it('calls gitCreate and ensureGitSymLink and returns its result, if it succeeds with overwrite', async function () {
      this.gitCreateStub.resolves(this.gitCreateProgress)
      this.ensureGitSymLinkStub.resolves(gitSymLinkSuccessOverwrite)

      const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
      shouldBeExpectedRunReport(
        this.subject,
        runReport,
        [gitEnsureOriginFailurePathDoesNotExist, this.gitCreateProgress, gitSymLinkSuccessOverwrite],
        false
      )

      this.gitEnsureOriginStub.should.have.been.calledOnce()
      this.gitUpdateStub.should.not.have.been.called()
      this.gitCreateStub.should.have.been.calledOnce()
      this.ensureGitSymLinkStub.should.have.been.calledOnce()
    })

    it('calls gitCreate and ensureGitSymLink and returns its result, if it succeeds without overwrite', async function () {
      this.gitCreateStub.resolves(this.gitCreateProgress)
      this.ensureGitSymLinkStub.resolves(gitSymLinkSuccessNoOverwrite)

      const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
      shouldBeExpectedRunReport(
        this.subject,
        runReport,
        [gitEnsureOriginFailurePathDoesNotExist, this.gitCreateProgress, gitSymLinkSuccessNoOverwrite],
        false
      )

      this.gitEnsureOriginStub.should.have.been.calledOnce()
      this.gitUpdateStub.should.not.have.been.called()
      this.gitCreateStub.should.have.been.calledOnce()
      this.ensureGitSymLinkStub.should.have.been.calledOnce()
    })

    it('calls gitCreate and ensureGitSymLink and reports the failure if it does', async function () {
      this.gitCreateStub.resolves(this.gitCreateProgress)
      this.ensureGitSymLinkStub.rejects(gitSymLinkFailure)

      const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
      shouldBeExpectedRunReport(
        this.subject,
        runReport,
        [gitEnsureOriginFailurePathDoesNotExist, this.gitCreateProgress, gitSymLinkFailure],
        true
      )

      this.gitEnsureOriginStub.should.have.been.calledOnce()
      this.gitUpdateStub.should.not.have.been.called()
      this.gitCreateStub.should.have.been.calledOnce()
      this.ensureGitSymLinkStub.should.have.been.calledOnce()
    })

    it('propagates gitCreate’s errors when it fails, and does not call ensureGitSymLink', async function () {
      /** @type {TGitCreateFailure} */
      const createFailure = {
        .../** @type {TGitCreateSuccess} */ this.gitCreateProgress,
        success: false,
        error: errorReports[9]
      }
      this.gitCreateStub.rejects(createFailure)

      const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
      shouldBeExpectedRunReport(this.subject, runReport, [gitEnsureOriginFailurePathDoesNotExist, createFailure], true)

      this.gitEnsureOriginStub.should.have.been.calledOnce()
      this.gitUpdateStub.should.not.have.been.called()
      this.gitCreateStub.should.have.been.calledOnce()
      this.ensureGitSymLinkStub.should.not.have.been.called()
    })
  })

  describe('mirror clone directory exists, but cannot be read', function () {
    beforeEach(function () {
      this.gitEnsureOriginStub.rejects(gitEnsureOriginFailurePathExists)
    })

    it('does not call gitCreate, gitUpdate, nor ensureGitSymLink and throws the failure', async function () {
      const runReport = await this.subject.updateOrCreateAndLink().should.be.resolved()
      shouldBeExpectedRunReport(this.subject, runReport, [gitEnsureOriginFailurePathExists], true)

      this.gitEnsureOriginStub.should.have.been.calledOnce()
      this.gitUpdateStub.should.not.have.been.called()
      this.gitCreateStub.should.not.have.been.called()
      this.ensureGitSymLinkStub.should.not.have.been.called()
    })
  })
})
