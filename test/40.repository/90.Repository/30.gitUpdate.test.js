/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../../testName')(module)
const { pathExists } = require('fs-extra')
const { join } = require('node:path')
const { update, GitUpdateFailure, GitUpdateSuccess } = require('../../../lib/repository/GitUpdateProgressStep')
const {
  setupOutsideBaseWithRepositoryFixture,
  setupSomethingInTheWay,
  setupExistingMirrorClone
} = require('./setupOutsideBaseWithRepositoryFixture')
const { shouldBeProgressStep } = require('../../30.runReport/ProgressStepCommon')

/**
 * @param {TGitUpdateSuccess | TGitUpdateFailure} progress
 * @param {unknown} failure
 * @returns {void}
 */
function shouldBeGitUpdateProgressStep(progress, failure) {
  progress.action.should.equal(update)
  shouldBeProgressStep(progress, failure)
  if (failure) {
    GitUpdateFailure.parse(progress)
    console.log(progress.error.message)
  } else {
    GitUpdateSuccess.parse(progress)
  }
}

describe(testName, function () {
  /* NOTE: This test requires a directory outside of this project.
     Any directory in this project will use the origin with which this project is checked out as origin, and
     here we need setting the origin to fail. */

  setupOutsideBaseWithRepositoryFixture('update')

  describe('no mirror clone yet', function () {
    it('fails if the mirror clone does not exist', async function () {
      const result = await this.subject.gitUpdate().should.be.rejected()

      shouldBeGitUpdateProgressStep(result, true)
    })
  })

  describe('something in the way', function () {
    setupSomethingInTheWay()

    it('fails if what is at mirrorClonePath is not a git repository', async function () {
      const result = await this.subject.gitUpdate().should.be.rejected()

      shouldBeGitUpdateProgressStep(result, true)
    })
  })

  describe('existing mirror clone', function () {
    setupExistingMirrorClone()

    it('updates a mirror clone', async function () {
      this.timeout(10000)

      const result = await this.subject.gitUpdate().should.be.resolved()

      shouldBeGitUpdateProgressStep(result, false)
      await pathExists(join(this.absoluteMirrorClonePath, 'HEAD')).should.be.resolvedWith(true)
    })
  })
})
