/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const pattern = /[\n\r]/g

/**
 * When the url of a repository is returned, there is an extra EOL at the end of
 * the string. Remove this.
 *
 * This is not a bug in `simple-git`. The result of these commands of `git` is multi-line in general, and this is
 * realized just as a pass-through of what `git` returns in CLI. In this case, it happens to be 1 line.
 *
 * @param {string} origin
 * @returns {string}
 */
function fixupOrigin(origin) {
  return origin.replace(pattern, '')
}

module.exports = { fixupOrigin }
