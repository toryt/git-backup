/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { runInfo } = require('../../constants')
const { remove, ensureFile, pathExists } = require('fs-extra')
const testName = require('../../testName')(module)
const { join } = require('node:path')
const { makeISODatePathCompatible } = require('../../../lib/runReport/saveRunReport')
const { createGitRepository } = require('../RepositoryCommon')
const { symLink, GitSymLinkFailure, GitSymLinkSuccess } = require('../../../lib/repository/GitSymLinkProgressStep')
const { shouldBeProgressStep } = require('../../30.runReport/ProgressStepCommon')

/**
 * @param {TGitSymLinkSuccess | TGitSymLinkFailure} progress
 * @param {unknown} failure
 * @param {boolean } [overwritten]
 * @returns {void}
 */
function shouldBeGitSymLinkProgressStep(progress, failure, overwritten) {
  progress.action.should.equal(symLink)
  shouldBeProgressStep(progress, failure)
  if (failure) {
    GitSymLinkFailure.parse(progress)
    console.log(progress.error.message)
  } else {
    GitSymLinkSuccess.parse(progress)
    progress.overwritten.should.equal(overwritten)
  }
}

describe(testName, function () {
  beforeEach(async function () {
    const { repository } = createGitRepository()
    this.subject = repository
    this.absoluteOwnerDirectoryPath = runInfo.absolutePath(this.subject.owner)
    this.absoluteSymLinkPath = runInfo.absolutePath(join(this.subject.gitSymLinkPath))
    this.absoluteSymLinkPath.should.startWith(this.absoluteOwnerDirectoryPath)
    this.absoluteTargetPath = runInfo.absolutePath(this.subject.mirrorClonePath)
    return remove(this.absoluteOwnerDirectoryPath)
  })

  afterEach(async function () {
    return Promise.all([remove(this.absoluteOwnerDirectoryPath), remove(this.absoluteTargetPath)])
  })

  describe('target exists', function () {
    beforeEach(async function () {
      const targetFileName = makeISODatePathCompatible(new Date().toISOString())
      this.absoluteTargetFilePath = join(this.absoluteTargetPath, targetFileName)
      this.absoluteTargetFilePathViaSymLink = join(this.absoluteSymLinkPath, targetFileName)
      return ensureFile(this.absoluteTargetFilePath)
    })

    it('can create a symbolic link when nothing is in the way, and the target exists', async function () {
      const result = await this.subject.ensureGitSymLink().should.be.resolved()
      shouldBeGitSymLinkProgressStep(result, false, false)

      await pathExists(this.absoluteSymLinkPath).should.be.resolvedWith(true)
      await pathExists(this.absoluteTargetFilePathViaSymLink).should.be.resolvedWith(true)
    })

    it('can create a symbolic link when something is in the way, and the target exists', async function () {
      await ensureFile(this.absoluteSymLinkPath)

      const result = await this.subject.ensureGitSymLink().should.be.resolved()
      shouldBeGitSymLinkProgressStep(result, false, true)

      await pathExists(this.absoluteSymLinkPath).should.be.resolvedWith(true)
      await pathExists(this.absoluteTargetFilePathViaSymLink).should.be.resolvedWith(true)
    })

    /* NOTE: This test is triggered by a bug in `fs-extra ensureSymLink`, registered at
             https://github.com/jprichardson/node-fs-extra/issues/1038
             It validates that our workaround works. */
    it('can create a symbolic link twice when the target exists', async function () {
      await pathExists(this.absoluteTargetPath).should.be.resolvedWith(true)

      const result1 = await this.subject.ensureGitSymLink().should.be.resolved()
      shouldBeGitSymLinkProgressStep(result1, false, false)

      await pathExists(this.absoluteSymLinkPath).should.be.resolvedWith(true)
      await pathExists(this.absoluteTargetFilePathViaSymLink).should.be.resolvedWith(true)

      await pathExists(this.absoluteTargetPath).should.be.resolvedWith(true)

      const result2 = await this.subject.ensureGitSymLink().should.be.resolved()
      shouldBeGitSymLinkProgressStep(result2, false, true)

      await pathExists(this.absoluteSymLinkPath).should.be.resolvedWith(true)
      await pathExists(this.absoluteTargetFilePathViaSymLink).should.be.resolvedWith(true)
    })
  })

  describe('target does not exist', function () {
    beforeEach(async function () {
      return remove(this.absoluteTargetPath)
    })

    it('reports its failure when the target does not exist, when nothing is in the way (and propagates other errors)', async function () {
      const result = await this.subject.ensureGitSymLink().should.be.rejected()
      shouldBeGitSymLinkProgressStep(result, true)

      await pathExists(this.absoluteSymLinkPath).should.be.resolvedWith(false)
    })

    it('reports its failure when the target does not exist, when something is in the way (and leaves it)', async function () {
      await ensureFile(this.absoluteSymLinkPath)

      const result = await this.subject.ensureGitSymLink().should.be.rejected()
      shouldBeGitSymLinkProgressStep(result, true)

      await pathExists(this.absoluteSymLinkPath).should.be.resolvedWith(true)
    })
  })
})
