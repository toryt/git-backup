/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { Repository } = require('../../../lib/repository/Repository')
const { shouldBeAbstractProperty } = require('../../_util/abstract')
const { owner, runInfo } = require('../../constants')
const { idInService, repositoryName } = require('../constants')
const testName = require('../../testName')(module)
const { shouldAdhereToRepositoryConstructorPostConditions } = require('../RepositoryCommon')
const { Desensifier } = require('../../../lib/Desensifier')

describe(testName, function () {
  describe('constructor', function () {
    it('creates an object that adheres to the invariants', function () {
      const desensifier = new Desensifier()
      const subject = new Repository(runInfo, desensifier, idInService, owner, repositoryName)
      shouldAdhereToRepositoryConstructorPostConditions(
        subject,
        runInfo,
        desensifier,
        idInService,
        owner,
        repositoryName
      )
    })
  })

  describe('instance', function () {
    beforeEach(function () {
      const desensifier = new Desensifier()
      this.subject = new Repository(runInfo, desensifier, idInService, owner, repositoryName)
    })

    describe('abstract properties', function () {
      shouldBeAbstractProperty('remoteURL')
    })
  })
})
