/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { join } = require('node:path')
const { runDate } = require('../../constants')
const { testRepoRemoteURL, absoluteExistingDirectoryPath } = require('../constants')
const { createGitRepository } = require('../RepositoryCommon')
const { stub } = require('sinon')
const { remove, copy, pathExists } = require('fs-extra')
const simpleGit = require('simple-git')
const { setupOutsideBaseFixture } = require('../../setupOutsideBaseFixture')
const { backupDirectoryName, RunInformation } = require('../../../lib/RunInformation')

function setupOutsideBaseWithRepositoryFixture(repositoryName) {
  setupOutsideBaseFixture()

  before(async function () {
    this.runInfo = new RunInformation(this.outsideBase, runDate)
    this.mirrorClonePath = join(backupDirectoryName, `${repositoryName}-test`)
    this.absoluteMirrorClonePath = join(this.outsideBase, this.mirrorClonePath)
  })

  beforeEach(function () {
    const { repository, remoteURLStub } = createGitRepository()
    this.subject = repository
    this.remoteURLStub = remoteURLStub
    this.runInfoStub = stub(this.subject, 'runInfo').value(new RunInformation(this.outsideBase, runDate))
    this.mirrorClonePathStub = stub(this.subject, 'mirrorClonePath').value(this.mirrorClonePath)
  })

  afterEach(async function () {
    this.mirrorClonePathStub.restore()
    this.runInfoStub.restore()
    this.remoteURLStub.restore()
  })
}

async function setupSomethingInTheWay() {
  before(async function () {
    await copy(absoluteExistingDirectoryPath, this.absoluteMirrorClonePath)
  })

  beforeEach(async function () {
    await pathExists(this.absoluteMirrorClonePath).should.be.resolvedWith(true)
    await pathExists(join(this.absoluteMirrorClonePath, 'HEAD')).should.be.resolvedWith(false)
  })

  after(async function () {
    await remove(this.absoluteMirrorClonePath)
  })
}

async function setupExistingMirrorClone() {
  before(async function () {
    this.timeout(20000)

    const git = simpleGit()
    // since this is a public repository, there is no need for credentials
    await git.mirror(testRepoRemoteURL, this.absoluteMirrorClonePath)
    this.mirrorClone = simpleGit(this.absoluteMirrorClonePath)
  })

  beforeEach(async function () {
    await pathExists(join(this.absoluteMirrorClonePath, 'HEAD')).should.be.resolvedWith(true)
  })

  after(async function () {
    await remove(this.absoluteMirrorClonePath)
  })
}

module.exports = {
  setupOutsideBaseWithRepositoryFixture,
  setupSomethingInTheWay,
  setupExistingMirrorClone
}
