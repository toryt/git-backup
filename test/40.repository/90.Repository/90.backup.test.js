/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { stub } = require('sinon')
const { successRepositoryRunReports, failureRepositoryRunReports } = require('../RepositoryRunReportCommon')
const { createGitRepository } = require('../RepositoryCommon')
const { errorReport } = require('../../../lib/runReport/ErrorReport')
const testName = require('../../testName')(module)
const { inspect } = require('node:util')
const { createFailed } = require('../../../lib/repository/Progress')

describe(testName, function () {
  beforeEach(function () {
    const { repository } = createGitRepository()
    this.subject = repository
    this.updateOrCreateAndLinkStub = stub(this.subject, 'updateOrCreateAndLink')
    this.saveRunReportStub = stub(this.subject, 'saveRunReport')
  })

  afterEach(function () {
    this.saveRunReportStub.restore()
    this.updateOrCreateAndLinkStub.restore()
  })

  describe('can save run report', function () {
    beforeEach(function () {
      this.saveRunReportStub.resolves()
    })

    describe('calls updateOrCreateAndLink and saves the run report when successful', function () {
      successRepositoryRunReports.forEach(runReport => {
        it(inspect(runReport.progress), async function () {
          this.updateOrCreateAndLinkStub.resolves(runReport)

          await this.subject.backup().should.be.resolvedWith(runReport)

          this.updateOrCreateAndLinkStub.should.have.been.calledOnce()
          this.saveRunReportStub.should.have.been.calledOnce()
          this.saveRunReportStub.should.have.been.calledWithExactly(
            runReport,
            `${this.subject.owner}_${this.subject.name}`
          )
        })
      })
    })

    describe('calls updateOrCreateAndLink and saves the run report on failure, if it is not a creation failure', function () {
      failureRepositoryRunReports
        .filter(({ progress }) => !createFailed(progress))
        .forEach(runReport => {
          it(inspect(runReport.progress), async function () {
            this.updateOrCreateAndLinkStub.resolves(runReport)

            await this.subject.backup().should.be.resolvedWith(runReport)

            this.updateOrCreateAndLinkStub.should.have.been.calledOnce()
            this.saveRunReportStub.should.have.been.calledOnce()
            this.saveRunReportStub.should.have.been.calledWithExactly(
              runReport,
              `${this.subject.owner}_${this.subject.name}`
            )
          })
        })
    })
  })

  describe('calls updateOrCreateAndLink and does not save the run report on create failure', function () {
    failureRepositoryRunReports
      .filter(({ progress }) => createFailed(progress))
      .forEach(runReport => {
        it(inspect(runReport.progress), async function () {
          this.updateOrCreateAndLinkStub.resolves(runReport)

          await this.subject.backup().should.be.resolvedWith(runReport)

          this.updateOrCreateAndLinkStub.should.have.been.calledOnce()
          this.saveRunReportStub.should.not.have.been.calledOnce()
        })
      })
  })

  describe('cannot save run report', function () {
    beforeEach(function () {
      this.errorReport = errorReport(new Error('cannot save repository run report'))
    })

    describe('calls updateOrCreateAndLink successfully and reports when it cannot save the run report', function () {
      successRepositoryRunReports.forEach(runReport => {
        it(inspect(runReport.progress), async function () {
          const urrt = { runReport, error: this.errorReport }
          this.updateOrCreateAndLinkStub.resolves(runReport)
          this.saveRunReportStub.rejects(urrt)

          await this.subject.backup().should.be.rejectedWith(urrt)

          this.updateOrCreateAndLinkStub.should.have.been.calledOnce()
          this.saveRunReportStub.should.have.been.calledOnce()
          this.saveRunReportStub.should.have.been.calledWithExactly(
            runReport,
            `${this.subject.owner}_${this.subject.name}`
          )
        })
      })
    })

    describe('calls updateOrCreateAndLink with a failure and reports when it cannot save the run report, if it is not a creation failure', function () {
      failureRepositoryRunReports
        .filter(({ progress }) => !createFailed(progress))
        .forEach(runReport => {
          it(inspect(runReport.progress), async function () {
            const urrt = { runReport, error: this.errorReport }
            this.updateOrCreateAndLinkStub.resolves(runReport)
            this.saveRunReportStub.rejects(urrt)

            await this.subject.backup().should.be.rejectedWith(urrt)

            this.updateOrCreateAndLinkStub.should.have.been.calledOnce()
            this.saveRunReportStub.should.have.been.calledOnce()
            this.saveRunReportStub.should.have.been.calledWithExactly(
              runReport,
              `${this.subject.owner}_${this.subject.name}`
            )
          })
        })
    })
  })
})
