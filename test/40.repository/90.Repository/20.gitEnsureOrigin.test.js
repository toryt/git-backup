/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../../testName')(module)
const { pathExists } = require('fs-extra')
const { fixupOrigin } = require('./fixupOrigin')
const {
  ensureOrigin,
  GitEnsureOriginFailure,
  GitEnsureOriginSuccess
} = require('../../../lib/repository/GitEnsureOriginProgressStep')
const {
  setupOutsideBaseWithRepositoryFixture,
  setupSomethingInTheWay,
  setupExistingMirrorClone
} = require('./setupOutsideBaseWithRepositoryFixture')
const { shouldBeProgressStep } = require('../../30.runReport/ProgressStepCommon')

/**
 * @param {TGitEnsureOriginSuccess | TGitEnsureOriginFailure} progress
 * @param {unknown} failure
 * @param {boolean} [pathExists]
 * @returns {void}
 */
function shouldBeGitEnsureOriginProgressStep(progress, failure, pathExists) {
  progress.action.should.equal(ensureOrigin)
  shouldBeProgressStep(progress, failure)
  if (failure) {
    GitEnsureOriginFailure.parse(progress)
    progress.pathExists.should.equal(pathExists)
    console.log(progress.error.message)
  } else {
    GitEnsureOriginSuccess.parse(progress)
    progress.should.not.have.property('pathExists')
  }
}

describe(testName, function () {
  /* NOTE: This test requires a directory outside of this project.
       Any directory in this project will use the origin with which this project is checked out as origin, and
       here we need setting the origin to fail. */

  setupOutsideBaseWithRepositoryFixture('ensure-origin')

  describe('no mirror clone', function () {
    beforeEach(async function () {
      await pathExists(this.absoluteMirrorClonePath).should.be.resolvedWith(false)
    })

    it('fails if the mirror clone does not exist', async function () {
      const result = await this.subject.gitEnsureOrigin().should.be.rejected()
      shouldBeGitEnsureOriginProgressStep(result, true, false)
    })
  })

  describe('something in the way', function () {
    setupSomethingInTheWay()

    it('fails if it cannot set the origin of the existing directory', async function () {
      const result = await this.subject.gitEnsureOrigin().should.be.rejected()

      shouldBeGitEnsureOriginProgressStep(result, true, true)
    })
  })

  describe('existing mirror clone', function () {
    setupExistingMirrorClone()

    it('does not do anything if the origin is as expected', async function () {
      const initialMirrorCloneOrigin = fixupOrigin(await this.mirrorClone.remote(['get-url', 'origin']))
      this.remoteURLStub.value(initialMirrorCloneOrigin)

      const result = await this.subject.gitEnsureOrigin().should.be.resolved()

      shouldBeGitEnsureOriginProgressStep(result, false)
      const mirrorCloneOrigin = fixupOrigin(await this.mirrorClone.remote(['get-url', 'origin']))
      mirrorCloneOrigin.should.equal(initialMirrorCloneOrigin)
    })

    it('changes the origin if it is different', async function () {
      const initialMirrorCloneOrigin = fixupOrigin(await this.mirrorClone.remote(['get-url', 'origin']))
      const otherOrigin = 'https://user:password@example.org/a/different/origin'
      otherOrigin.should.not.equal(initialMirrorCloneOrigin)
      this.remoteURLStub.value(otherOrigin)

      const result = await this.subject.gitEnsureOrigin().should.be.resolved()

      shouldBeGitEnsureOriginProgressStep(result, false)
      const mirrorCloneOrigin = fixupOrigin(await this.mirrorClone.remote(['get-url', 'origin']))
      mirrorCloneOrigin.should.equal(otherOrigin)
    })
  })
})
