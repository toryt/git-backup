/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../../testName')(module)
const { testRepoName, testRepoRemoteURL } = require('../constants')
const { join } = require('node:path')
const { pathExists, remove } = require('fs-extra')
const { create, GitCreateFailure, GitCreateSuccess } = require('../../../lib/repository/GitCreateProgressStep')
const {
  setupOutsideBaseWithRepositoryFixture,
  setupSomethingInTheWay
} = require('./setupOutsideBaseWithRepositoryFixture')
const { shouldBeProgressStep } = require('../../30.runReport/ProgressStepCommon')

/**
 * @param {TGitCreateSuccess | TGitCreateFailure} progress
 * @param {unknown} failure
 * @returns {void}
 */
function shouldBeGitCreateProgressStep(progress, failure) {
  progress.action.should.equal(create)
  shouldBeProgressStep(progress, failure)
  if (failure) {
    GitCreateFailure.parse(progress)
    console.log(progress.error.message)
  } else {
    GitCreateSuccess.parse(progress)
  }
}

describe(testName, function () {
  /* NOTE: This test requires a directory outside of this project.
     Any directory in this project will use the origin with which this project is checked out as origin, and
     here we need setting the origin to fail. */

  setupOutsideBaseWithRepositoryFixture('create')

  describe('nothing there', function () {
    beforeEach(async function () {
      await pathExists(this.absoluteMirrorClonePath).should.be.resolvedWith(false)
    })

    afterEach(async function () {
      await remove(this.absoluteMirrorClonePath)
    })

    it('creates a new repository backup', async function () {
      this.timeout(20000)

      // since `thisRepoOrigin` this is a public repository, there is no need for credentials
      const result = await this.subject.gitCreate().should.be.resolved()

      shouldBeGitCreateProgressStep(result, false)
      await pathExists(join(this.absoluteMirrorClonePath, 'HEAD')).should.be.resolvedWith(true)
    })

    it('propagates git errors (repository does not exist)', async function () {
      this.timeout(5000)

      this.remoteURLStub.value(testRepoRemoteURL.replace(testRepoName, 'does-not-exist'))

      const result = await this.subject.gitCreate().should.be.rejected()

      shouldBeGitCreateProgressStep(result, true)
      await pathExists(join(this.absoluteMirrorClonePath, 'HEAD')).should.be.resolvedWith(false)
    })
  })

  describe('something in the way', function () {
    setupSomethingInTheWay()

    it('propagates git errors (path not free)', async function () {
      const result = await this.subject.gitCreate().should.be.rejected()

      shouldBeGitCreateProgressStep(result, true)
      await pathExists(this.absoluteMirrorClonePath).should.be.resolvedWith(true)
      await pathExists(join(this.absoluteMirrorClonePath, 'HEAD')).should.be.resolvedWith(false)
    })
  })
})
