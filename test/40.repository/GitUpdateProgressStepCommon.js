/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { update } = require('../../lib/repository/GitUpdateProgressStep')
const { errorReports } = require('../30.runReport/ErrorReportCommon')

/**
 * @type {TGitUpdateSuccess}
 */
const gitUpdateSuccess = {
  action: update,
  start: '2023-11-04T18:27:27.758Z',
  end: '2023-11-04T18:27:28.758Z',
  duration: 'PT1S',
  success: true
}

/**
 * @type {TGitUpdateFailure}
 */
const gitUpdateFailure = { ...gitUpdateSuccess, success: false, error: errorReports[2] }

module.exports = {
  gitUpdateSuccess,
  gitUpdateFailure
}
