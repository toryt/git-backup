/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined, notASimpleError } = require('../_util/stuff')
const { create, GitCreateSuccess, GitCreateFailure } = require('../../lib/repository/GitCreateProgressStep')
const { gitCreateSuccess, gitCreateFailure } = require('./GitCreateProgressStepCommon')
const { repositoryActions } = require('./RepositoryActionCommon')

describe(testName, function () {
  describe('GitCreateSuccess', function () {
    validateSchema(
      GitCreateSuccess,
      [gitCreateSuccess],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== create))
            .map(action => ({ ...gitCreateSuccess, action }))
        )
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== true)
            .map(success => ({ ...gitCreateSuccess, success }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitCreateSuccess, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitCreateSuccess, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitCreateSuccess, duration })))
    )
  })
  describe('GitCreateFailure', function () {
    validateSchema(
      GitCreateFailure,
      [gitCreateFailure],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== create))
            .map(action => ({ ...gitCreateFailure, action }))
        )
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== false)
            .map(success => ({ ...gitCreateFailure, success }))
        )

        .concat(stuffWithUndefined.map(start => ({ ...gitCreateFailure, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitCreateFailure, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitCreateFailure, duration })))
        .concat(stuffWithUndefined.filter(notASimpleError).map(error => ({ ...gitCreateFailure, error })))
    )
  })
})
