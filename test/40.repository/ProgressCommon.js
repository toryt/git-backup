/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const {
  gitEnsureOriginSuccess,
  gitEnsureOriginFailurePathDoesNotExist
} = require('./GitEnsureOriginProgressStepCommon')
const { gitUpdateFailure, gitUpdateSuccess } = require('./GitUpdateProgressStepCommon')
const { gitCreateSuccess, gitCreateFailure } = require('./GitCreateProgressStepCommon')
const {
  gitSymLinkSuccessOverwrite,
  gitSymLinkSuccessNoOverwrite,
  gitSymLinkFailure
} = require('./GitSymLinkProgressStepCommon')

/** @type {TUpdateSuccessProgress} */
const updateSuccessProgress = [gitEnsureOriginSuccess, gitUpdateSuccess, gitSymLinkSuccessOverwrite]

/** @type {TCreateSuccessProgress} */
const createSuccessProgress = [gitEnsureOriginFailurePathDoesNotExist, gitCreateSuccess, gitSymLinkSuccessNoOverwrite]

/** @type {Array<TSuccessProgress>} */
const successProgresses = [updateSuccessProgress, createSuccessProgress]

/** @type {TUpdateFailureProgress} */
const updateFailureProgress = [gitEnsureOriginSuccess, gitUpdateFailure]

/** @type {TCreateFailureProgress} */
const createFailureProgress = [gitEnsureOriginFailurePathDoesNotExist, gitCreateFailure]

/** @type {TEnsureOriginFailureProgress} */
const ensureOriginFailureProgress = [gitEnsureOriginFailurePathDoesNotExist]

/** @type {TUpdateSymLinkFailureProgress} */
const updateSymLinkFailureProgress = [gitEnsureOriginSuccess, gitUpdateSuccess, gitSymLinkFailure]

/** @type {TCreateSymLinkFailureProgress} */
const createSymLinkFailureProgress = [gitEnsureOriginFailurePathDoesNotExist, gitCreateSuccess, gitSymLinkFailure]

/** @type {Array<TFailureProgress>} */
const failureProgresses = [
  updateFailureProgress,
  createFailureProgress,
  ensureOriginFailureProgress,
  updateSymLinkFailureProgress,
  createSymLinkFailureProgress
]

module.exports = {
  updateSuccessProgress,
  createSuccessProgress,
  successProgresses,
  updateFailureProgress,
  createFailureProgress,
  ensureOriginFailureProgress,
  updateSymLinkFailureProgress,
  createSymLinkFailureProgress,
  failureProgresses
}
