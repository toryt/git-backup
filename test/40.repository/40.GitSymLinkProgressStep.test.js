/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { gitSymLinkSuccessOverwrite, gitSymLinkFailure, gitSymLinkSuccesses } = require('./GitSymLinkProgressStepCommon')
const { stuffWithUndefined, notABoolean, notASimpleError } = require('../_util/stuff')
const { GitSymLinkSuccess, GitSymLinkFailure } = require('../../lib/repository/GitSymLinkProgressStep')
const { symLink } = require('../../lib/repository/GitSymLinkProgressStep')
const { repositoryActions } = require('./RepositoryActionCommon')

describe(testName, function () {
  describe('GitSymLinkSuccess', function () {
    validateSchema(
      GitSymLinkSuccess,
      gitSymLinkSuccesses,
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== symLink))
            .map(action => ({ ...gitSymLinkSuccessOverwrite, action }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitSymLinkSuccessOverwrite, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitSymLinkSuccessOverwrite, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitSymLinkSuccessOverwrite, duration })))
        .concat(
          stuffWithUndefined.filter(notABoolean).map(overwritten => ({ ...gitSymLinkSuccessOverwrite, overwritten }))
        )
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== true)
            .map(success => ({ ...gitSymLinkSuccessOverwrite, success }))
        )
    )
  })
  describe('GitSymLinkFailure', function () {
    validateSchema(
      GitSymLinkFailure,
      [gitSymLinkFailure],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== symLink))
            .map(action => ({ ...gitSymLinkFailure, action }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitSymLinkFailure, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitSymLinkFailure, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitSymLinkFailure, duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== false)
            .map(success => ({ ...gitSymLinkFailure, success }))
        )
        .concat(stuffWithUndefined.filter(notASimpleError).map(error => ({ ...gitSymLinkFailure, error })))
    )
  })
})
