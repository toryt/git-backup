/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { join } = require('node:path')
const { backupDirectoryName } = require('../../lib/RunInformation')
const { runInfo, owner } = require('../constants')

const testRepoName = 'git-backup-test-subject'
const testRepoRemoteURL = `https://bitbucket.org/toryt/${testRepoName}.git`

const repositoryName = 'test-repository'

/**
 * Relative path to a directory that does not exist, but could exist.
 *
 * @type {string}
 */
const baseRelativeEmptyPath = join(backupDirectoryName, `nothing-here-yet`)

/**
 * Relative path to a directory that does exist for tests.
 *
 * @type {string}
 */
const baseRelativeExistingDirectoryPath = join(backupDirectoryName, 'repository-exists')

/**
 * Path to a directory that does exist for tests.
 *
 * @type {string}
 */
const absoluteExistingDirectoryPath = runInfo.absolutePath(baseRelativeExistingDirectoryPath)

const idInService = `anId`

/**
 * @type {TRelativePath}
 */
const gitSymLink = join(owner, `${repositoryName}.git`)

module.exports = {
  testRepoName,
  testRepoRemoteURL,
  repositoryName,
  baseRelativeEmptyPath,
  absoluteExistingDirectoryPath,
  idInService,
  gitSymLink
}
