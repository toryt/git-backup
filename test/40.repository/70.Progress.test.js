/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuff } = require('../_util/stuff')
const { create } = require('../../lib/repository/GitCreateProgressStep')
const {
  UpdateSuccessProgress,
  CreateSuccessProgress,
  SuccessProgress,
  UpdateFailureProgress,
  CreateFailureProgress,
  FailureProgress,
  EnsureOriginFailureProgress,
  UpdateSymLinkFailureProgress,
  CreateSymLinkFailureProgress,
  createFailed
} = require('../../lib/repository/Progress')
const {
  updateSuccessProgress,
  updateFailureProgress,
  createSuccessProgress,
  createFailureProgress,
  successProgresses,
  failureProgresses,
  ensureOriginFailureProgress,
  updateSymLinkFailureProgress,
  createSymLinkFailureProgress
} = require('./ProgressCommon')
const {
  gitEnsureOriginSuccess,
  gitEnsureOriginFailurePathDoesNotExist,
  gitEnsureOriginFailurePathExists
} = require('./GitEnsureOriginProgressStepCommon')
const { gitUpdateFailure, gitUpdateSuccess } = require('./GitUpdateProgressStepCommon')
const { gitCreateSuccess } = require('./GitCreateProgressStepCommon')
const {
  gitSymLinkFailure,
  gitSymLinkSuccessNoOverwrite,
  gitSymLinkSuccessOverwrite
} = require('./GitSymLinkProgressStepCommon')
const { inspect } = require('node:util')

describe(testName, function () {
  describe('UpdateSuccessProgress', function () {
    validateSchema(
      UpdateSuccessProgress,
      [updateSuccessProgress],
      stuff.concat([
        [gitEnsureOriginSuccess],
        [gitUpdateSuccess, gitEnsureOriginSuccess],
        [gitUpdateSuccess],
        [gitEnsureOriginSuccess, gitSymLinkFailure],
        [gitUpdateSuccess, gitEnsureOriginSuccess, gitSymLinkFailure],
        [gitUpdateSuccess, gitSymLinkFailure],
        [gitEnsureOriginSuccess, ...updateSuccessProgress],
        [...updateSuccessProgress, gitUpdateSuccess],
        createSuccessProgress,
        updateFailureProgress,
        createFailureProgress,
        ensureOriginFailureProgress,
        updateSymLinkFailureProgress,
        createSymLinkFailureProgress
      ])
    )
  })

  describe('CreateSuccessProgress', function () {
    validateSchema(
      CreateSuccessProgress,
      [createSuccessProgress],
      stuff.concat([
        [gitEnsureOriginFailurePathDoesNotExist],
        [gitCreateSuccess, gitEnsureOriginFailurePathDoesNotExist],
        [gitCreateSuccess],
        [gitEnsureOriginFailurePathDoesNotExist, gitSymLinkFailure],
        [gitCreateSuccess, gitEnsureOriginFailurePathDoesNotExist, gitSymLinkFailure],
        [gitCreateSuccess, gitSymLinkFailure],
        [gitEnsureOriginFailurePathDoesNotExist, ...createSuccessProgress],
        [...createSuccessProgress, gitCreateSuccess],
        updateSuccessProgress,
        updateFailureProgress,
        createFailureProgress,
        ensureOriginFailureProgress,
        updateSymLinkFailureProgress,
        createSymLinkFailureProgress
      ])
    )
  })

  describe('SuccessProgress', function () {
    validateSchema(SuccessProgress, successProgresses, stuff.concat([failureProgresses]))
  })

  describe('UpdateFailureProgress', function () {
    validateSchema(
      UpdateFailureProgress,
      [updateFailureProgress],
      stuff.concat([
        [gitEnsureOriginSuccess],
        [gitUpdateFailure, gitEnsureOriginSuccess],
        [gitUpdateFailure],
        [gitEnsureOriginSuccess, ...updateSuccessProgress],
        [...updateSuccessProgress, gitUpdateFailure],
        createSuccessProgress,
        updateSuccessProgress,
        createFailureProgress,
        ensureOriginFailureProgress,
        updateSymLinkFailureProgress,
        createSymLinkFailureProgress
      ])
    )
  })

  describe('CreateFailureProgress', function () {
    validateSchema(
      CreateFailureProgress,
      [createFailureProgress],
      stuff.concat([
        [gitEnsureOriginSuccess],
        [gitCreateSuccess, gitEnsureOriginSuccess],
        [gitCreateSuccess],
        [gitEnsureOriginSuccess, ...createSuccessProgress],
        [...createSuccessProgress, gitCreateSuccess],
        updateSuccessProgress,
        updateFailureProgress,
        updateSuccessProgress,
        ensureOriginFailureProgress,
        updateSymLinkFailureProgress,
        createSymLinkFailureProgress
      ])
    )
  })

  describe('EnsureOriginFailureProgress', function () {
    validateSchema(
      EnsureOriginFailureProgress,
      [ensureOriginFailureProgress],
      stuff.concat([
        [gitEnsureOriginSuccess],
        [gitCreateSuccess, gitEnsureOriginSuccess],
        [gitCreateSuccess],
        [gitEnsureOriginSuccess, ...createSuccessProgress],
        [...createSuccessProgress, gitCreateSuccess],
        updateSuccessProgress,
        updateFailureProgress,
        updateSuccessProgress,
        createFailureProgress,
        updateSymLinkFailureProgress,
        createSymLinkFailureProgress
      ])
    )
  })

  describe('UpdateSymLinkFailureProgress', function () {
    validateSchema(
      UpdateSymLinkFailureProgress,
      [updateSymLinkFailureProgress],
      stuff.concat([
        [gitUpdateSuccess, gitSymLinkFailure],
        [gitEnsureOriginSuccess, gitSymLinkFailure],
        [gitEnsureOriginSuccess, gitUpdateSuccess],
        [gitEnsureOriginSuccess, gitSymLinkFailure, gitUpdateSuccess],
        [gitSymLinkFailure, ...updateSymLinkFailureProgress],
        [...updateSymLinkFailureProgress, gitSymLinkFailure],
        updateSuccessProgress,
        updateFailureProgress,
        updateSuccessProgress,
        createFailureProgress,
        createSymLinkFailureProgress
      ])
    )
  })

  describe('CreateSymLinkFailureProgress', function () {
    validateSchema(
      CreateSymLinkFailureProgress,
      [createSymLinkFailureProgress],
      stuff.concat([
        [gitCreateSuccess, gitSymLinkFailure],
        [gitEnsureOriginFailurePathExists, gitCreateSuccess, gitSymLinkSuccessOverwrite],
        [gitEnsureOriginFailurePathExists, gitCreateSuccess, gitSymLinkSuccessNoOverwrite],
        [gitEnsureOriginSuccess, gitCreateSuccess],
        [gitEnsureOriginFailurePathDoesNotExist, gitSymLinkFailure, gitCreateSuccess],
        [gitSymLinkFailure, ...createSymLinkFailureProgress],
        [...createSymLinkFailureProgress, gitSymLinkFailure],
        updateSuccessProgress,
        updateFailureProgress,
        updateSuccessProgress,
        createFailureProgress,
        updateSymLinkFailureProgress
      ])
    )
  })

  describe('FailureProgress', function () {
    validateSchema(FailureProgress, failureProgresses, stuff.concat([successProgresses]))
  })

  describe('createFailed', function () {
    describe('it reports false for a SuccessProgress', function () {
      successProgresses.forEach(p => {
        it(inspect(p), function () {
          createFailed(p).should.equal(false)
        })
      })
    })
    describe('it reports false for a FailureProgress that does not contain a GitCreateFailure, unless it contains a GitCreateFailure failure', function () {
      failureProgresses.forEach(p => {
        const containsGitCreateFailure = !p.every(q => q.action !== create || q.success)
        it(`${containsGitCreateFailure}: ${inspect(p)}`, function () {
          createFailed(p).should.equal(containsGitCreateFailure)
        })
      })
    })
  })
})
