/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { create } = require('../../lib/repository/GitCreateProgressStep')
const { errorReports } = require('../30.runReport/ErrorReportCommon')

/**
 * @type {TGitCreateSuccess}
 */
const gitCreateSuccess = {
  action: create,
  success: true,
  start: '2023-11-04T17:09:47.015Z',
  end: '2023-11-04T17:09:49.015Z',
  duration: 'PT2S'
}

/**
 * @type {TGitCreateFailure}
 */
const gitCreateFailure = { ...gitCreateSuccess, success: false, error: errorReports[3] }

module.exports = {
  gitCreateSuccess,
  gitCreateFailure
}
