/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined, notABoolean, notASimpleError } = require('../_util/stuff')
const { GitEnsureOriginSuccess, GitEnsureOriginFailure } = require('../../lib/repository/GitEnsureOriginProgressStep')
const {
  gitEnsureOriginSuccess,
  gitEnsureOriginFailures,
  gitEnsureOriginFailurePathExists
} = require('./GitEnsureOriginProgressStepCommon')
const { ensureOrigin } = require('../../lib/repository/GitEnsureOriginProgressStep')
const { repositoryActions } = require('./RepositoryActionCommon')

describe(testName, function () {
  describe('GitEnsureOriginSuccess', function () {
    validateSchema(
      GitEnsureOriginSuccess,
      [gitEnsureOriginSuccess],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== ensureOrigin))
            .map(action => ({ ...gitEnsureOriginSuccess, action }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitEnsureOriginSuccess, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitEnsureOriginSuccess, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitEnsureOriginSuccess, duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== true)
            .map(success => ({ ...gitEnsureOriginSuccess, success }))
        )
    )
  })

  describe('GitEnsureOriginFailure', function () {
    validateSchema(
      GitEnsureOriginFailure,
      gitEnsureOriginFailures,
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== ensureOrigin))
            .map(action => ({ ...gitEnsureOriginFailurePathExists, action }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitEnsureOriginFailurePathExists, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitEnsureOriginFailurePathExists, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitEnsureOriginFailurePathExists, duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== false)
            .map(success => ({ ...gitEnsureOriginFailurePathExists, success }))
        )
        .concat(
          stuffWithUndefined
            .filter(notABoolean)
            .map(pathExists => ({ ...gitEnsureOriginFailurePathExists, pathExists }))
        )
        .concat(
          stuffWithUndefined.filter(notASimpleError).map(error => ({ ...gitEnsureOriginFailurePathExists, error }))
        )
    )
  })
})
