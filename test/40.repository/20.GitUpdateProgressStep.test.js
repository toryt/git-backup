/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { gitUpdateSuccess, gitUpdateFailure } = require('./GitUpdateProgressStepCommon')
const { stuffWithUndefined, notASimpleError } = require('../_util/stuff')
const { GitUpdateSuccess, GitUpdateFailure } = require('../../lib/repository/GitUpdateProgressStep')
const { update } = require('../../lib/repository/GitUpdateProgressStep')
const { repositoryActions } = require('./RepositoryActionCommon')

describe(testName, function () {
  describe('GitUpdateSuccess', function () {
    validateSchema(
      GitUpdateSuccess,
      [gitUpdateSuccess],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== update))
            .map(action => ({ ...gitUpdateSuccess, action }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitUpdateSuccess, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitUpdateSuccess, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitUpdateSuccess, duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== true)
            .map(success => ({ ...gitUpdateSuccess, success }))
        )
    )
  })
  describe('GitUpdateFailure', function () {
    validateSchema(
      GitUpdateFailure,
      [gitUpdateFailure],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .concat(repositoryActions.filter(a => a !== update))
            .map(action => ({ ...gitUpdateFailure, action }))
        )
        .concat(stuffWithUndefined.map(start => ({ ...gitUpdateFailure, start })))
        .concat(stuffWithUndefined.map(end => ({ ...gitUpdateFailure, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...gitUpdateFailure, duration })))
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== false)
            .map(success => ({ ...gitUpdateFailure, success }))
        )
        .concat(stuffWithUndefined.filter(notASimpleError).map(error => ({ ...gitUpdateFailure, error })))
    )
  })
})
