/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { runReportsDirectoryName, Repository } = require('../../lib/repository/Repository')
const { join, relative } = require('node:path')
const { stub } = require('sinon')
const { owner, runInfo } = require('../constants')
const { idInService, testRepoRemoteURL, repositoryName } = require('./constants')
const { RelativePath } = require('../../lib/RelativePath')
const { shouldBeGoodAbsolutePath } = require('../_util/shouldBeGoodAbsolutePath')
const { RunInformation } = require('../../lib/RunInformation')
const { Logger } = require('../../lib/Logger')
const { RepositoryRunReport } = require('../../lib/repository/RepositoryRunReport')
const { Desensifier } = require('../../lib/Desensifier')

/**
 * @param {Repository} subject
 * @param {RunInformation} runInfo
 * @param {Desensifier} desensifier
 * @param {string} id
 * @param {string} owner
 * @param {string} name
 * @returns {void}
 */
function shouldAdhereToRepositoryConstructorPostConditions(subject, runInfo, desensifier, id, owner, name) {
  subject.should.be.an.instanceof(Repository)

  // runReportSchema
  subject.runReportSchema.should.equal(RepositoryRunReport)

  // runInfo
  subject.runInfo.should.be.an.instanceof(RunInformation)
  subject.runInfo.should.equal(runInfo)

  // runInfo
  subject.desensifier.should.be.an.instanceof(Desensifier)
  subject.desensifier.should.equal(desensifier)

  // id
  subject.id.should.be.a.String()
  subject.id.should.equal(id)

  // owner
  subject.owner.should.be.a.String()
  subject.owner.should.equal(owner)

  // name
  subject.name.should.be.a.String()
  subject.name.should.equal(name)

  // logger
  subject.logger.should.be.instanceof(Logger)
  subject.logger.identifier.should.be.a.Function()
  subject.logger.identifier().should.startWith(subject.runInfo.baseDirectoryName)
  subject.logger.identifier().should.containEql(subject.owner)
  subject.logger.identifier().should.endWith(subject.name)
  subject.logger.desensifier.should.equal(subject.desensifier)

  // mirrorClonePath
  RelativePath.parse(subject.mirrorClonePath)
  relative(runInfo.backupDirectory, subject.mirrorClonePath).should.equal(`${id}.git`)
  shouldBeGoodAbsolutePath(subject.runInfo.absolutePath(subject.mirrorClonePath))

  // subject.gitSymLinkPath
  console.log('gitSymLinkPath:', subject.gitSymLinkPath)
  RelativePath.parse(subject.gitSymLinkPath)
  subject.gitSymLinkPath.should.endWith('.git')
  subject.gitSymLinkPath.should.equal(join(subject.owner, `${subject.name}.git`))
  shouldBeGoodAbsolutePath(subject.runInfo.absolutePath(subject.gitSymLinkPath))

  // subject.gitSymLinkReference
  console.log('gitSymLinkReference:', subject.gitSymLinkReference)
  subject.gitSymLinkReference.should.be.a.String()
  join(subject.gitSymLinkPath, '..', subject.gitSymLinkReference).should.equal(subject.mirrorClonePath)
  relative(
    subject.runInfo.absolutePath(join(subject.gitSymLinkPath, '..')),
    subject.runInfo.absolutePath(subject.mirrorClonePath)
  ).should.equal(subject.gitSymLinkReference)

  // subject.runReportDirectory
  console.log('runReportDirectory:', subject.runReportDirectory)
  RelativePath.parse(subject.runReportDirectory)
  subject.runReportDirectory.should.equal(join(subject.mirrorClonePath, runReportsDirectoryName))
  shouldBeGoodAbsolutePath(subject.runInfo.absolutePath(subject.runReportDirectory))
}

/**
 * @returns {{repository: Repository, remoteURLStub: SinonStub<[], string>}}
 */
function createGitRepository() {
  const repository = new Repository(runInfo, new Desensifier(), idInService, owner, repositoryName)
  const remoteURLStub = stub(repository, 'remoteURL').value(testRepoRemoteURL)
  return { repository, remoteURLStub }
}

module.exports = { shouldAdhereToRepositoryConstructorPostConditions, createGitRepository }
