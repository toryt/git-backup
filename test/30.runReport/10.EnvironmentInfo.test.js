/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { environmentInfo } = require('../../lib/runReport/EnvironmentInfo')
const { validateSchema } = require('../_util/validateSchema')
const { EnvironmentInfo } = require('../../lib/runReport/EnvironmentInfo')
const { environmentInfo: environmentInfoExample } = require('./EnvironmentInfoCommon')
const { stuffWithUndefined, notATrimmedString } = require('../_util/stuff')

describe(testName, function () {
  describe('VersionInfo', function () {
    validateSchema(
      EnvironmentInfo,
      [environmentInfoExample],
      stuffWithUndefined
        .concat(stuffWithUndefined.filter(notATrimmedString).map(version => ({ ...environmentInfoExample, version })))
        .concat(stuffWithUndefined.filter(notATrimmedString).map(host => ({ ...environmentInfoExample, host })))
        .concat(stuffWithUndefined.filter(notATrimmedString).map(engine => ({ ...environmentInfoExample, engine })))
        .concat(stuffWithUndefined.filter(notATrimmedString).map(os => ({ ...environmentInfoExample, os })))
        .concat(stuffWithUndefined.filter(notATrimmedString).map(git => ({ ...environmentInfoExample, git })))
    )
  })

  describe('environmentInfo', function () {
    it('returns some version info', async function () {
      const result = await environmentInfo()

      console.dir(result)
      EnvironmentInfo.parse(result)
    })
  })
})
