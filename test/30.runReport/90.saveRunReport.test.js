/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { ensureDir, remove, ensureFile } = require('fs-extra')
const testName = require('../testName')(module)
const {
  successMark,
  failureMark,
  runReportSuccessFileName,
  runReportFailureFileName,
  saveRunReport,
  isoYMDPath,
  makeISODatePathCompatible
} = require('../../lib/runReport/saveRunReport')
const { runInfo, runMonth, runYear, runDay } = require('../constants')
const { Logger } = require('../../lib/Logger')
const { RelativePath } = require('../../lib/RelativePath')
const { join } = require('node:path')
const { shouldBeGoodAbsolutePath } = require('../_util/shouldBeGoodAbsolutePath')
const { successRunReport, failureRunReport, shouldBeExpectedRunReport } = require('./RunReportCommon')
const { RunReport } = require('../../lib/runReport/RunReport')
const { UnsavedRunReportThrowable } = require('../../lib/runReport/UnsavedRunReportThrowable')
const { SafePathSegment } = require('../SafePathSegmentCommon')
const { Desensifier } = require('../../lib/Desensifier')

const loggingIdentifier = () => 'logging identifier'
const logger = new Logger(loggingIdentifier, new Desensifier())
const runReportDirectory = 'run-report-directory'
const anExtra = 'this_is_an_extra'
const extras = [undefined, anExtra]

describe(testName, function () {
  describe('isoYMDPath', function () {
    it('returns the path', function () {
      const result = isoYMDPath(runInfo)

      RelativePath.parse(result)
      result.should.equal(
        join(String(runYear).padStart(4, '0'), String(runMonth).padStart(2, '0'), String(runDay).padStart(2, '0'))
      )
      shouldBeGoodAbsolutePath(runInfo.absolutePath(join(runReportDirectory, result)))
    })
  })

  describe('makeISODatePathCompatible', function () {
    it('replaces colons', function () {
      const str = '2023-11-02T21:15:27.842Z'
      const result = makeISODatePathCompatible(str)
      console.log(result)
      SafePathSegment.parse(result)
    })
  })

  describe('runReportSuccessFileName', function () {
    extras.forEach(extra => {
      it(`returns the expected file name${extra ? ' with an extra' : ''}`, function () {
        const expected = extra
          ? `${makeISODatePathCompatible(runInfo.runDate.toISOString())}-${successMark}-${anExtra}.yaml`
          : `${makeISODatePathCompatible(runInfo.runDate.toISOString())}-${successMark}.yaml`
        const result = runReportSuccessFileName(runInfo, extra)

        RelativePath.parse(result)
        result.should.equal(expected)
        shouldBeGoodAbsolutePath(runInfo.absolutePath(join(runReportDirectory, result)))
      })
    })
  })

  describe('runReportFailureFileName', function () {
    extras.forEach(extra => {
      it(`returns the expected file name${extra ? ' with an extra' : ''}`, function () {
        const expected = extra
          ? `${makeISODatePathCompatible(runInfo.runDate.toISOString())}-${failureMark}-${anExtra}.yaml`
          : `${makeISODatePathCompatible(runInfo.runDate.toISOString())}-${failureMark}.yaml`
        const result = runReportFailureFileName(runInfo, extra)

        RelativePath.parse(result)
        result.should.equal(expected)
        shouldBeGoodAbsolutePath(runInfo.absolutePath(join(runReportDirectory, result)))
      })
    })
  })

  describe('saveRunReport', function () {
    beforeEach(async function () {
      /** @type {TSaveRunReportThis<typeof RunReport>} */
      this.subject = { logger, runInfo, desensifier: new Desensifier(), runReportDirectory, runReportSchema: RunReport }
      this.runReportDirectoryAbsolutePath = this.subject.runInfo.absolutePath(this.subject.runReportDirectory)
      return remove(this.runReportDirectoryAbsolutePath)
    })

    afterEach(async function () {
      return remove(this.runReportDirectoryAbsolutePath)
    })

    extras.forEach(extra => {
      describe(extra ? 'without an extra' : 'with an extra', function () {
        it('saves a success run report in the expected place', async function () {
          await saveRunReport.call(this.subject, successRunReport, extra).should.be.resolved()

          return shouldBeExpectedRunReport(
            join(this.runReportDirectoryAbsolutePath, isoYMDPath(runInfo), runReportSuccessFileName(runInfo, extra)),
            successRunReport,
            this.subject.desensifier,
            RunReport
          )
        })
        it('saves a failure run report in the expected place', async function () {
          await saveRunReport.call(this.subject, failureRunReport, extra).should.be.resolved()

          return shouldBeExpectedRunReport(
            join(this.runReportDirectoryAbsolutePath, isoYMDPath(runInfo), runReportFailureFileName(runInfo, extra)),
            failureRunReport,
            this.subject.desensifier,
            RunReport
          )
        })
        it('saves a run report in the expected place when the run directory already exists', async function () {
          await ensureDir(this.subject.runInfo.absolutePath(this.subject.runReportDirectory))

          await saveRunReport.call(this.subject, successRunReport, extra).should.be.resolved()

          return shouldBeExpectedRunReport(
            join(this.runReportDirectoryAbsolutePath, isoYMDPath(runInfo), runReportSuccessFileName(runInfo, extra)),
            successRunReport,
            this.subject.desensifier,
            RunReport
          )
        })
        it('throws when it cannot save a run report in the expected place', async function () {
          await ensureFile(
            join(this.runReportDirectoryAbsolutePath, isoYMDPath(runInfo), runReportFailureFileName(runInfo, extra))
          )

          const result = await saveRunReport.call(this.subject, failureRunReport, extra).should.be.rejected()
          UnsavedRunReportThrowable.parse(result)
          result.runReport.should.deepEqual(this.subject.desensifier.desensify(failureRunReport))
          result.error.message.should.containEql('EEXIST')
        })
      })
    })
  })
})
