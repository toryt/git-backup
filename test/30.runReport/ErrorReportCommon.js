/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { jsonValues } = require('../01.schemata/JSONValueCommon')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')

/**
 * @type {TErrorReport}
 */
const errorReportBase = {
  message: 'an error message'
}

/**
 * @type {Array<TErrorReport>}
 */
const errorReportsWithAndWithoutStack = [
  errorReportBase,
  {
    ...errorReportBase,
    stack: `a stack
is
multiline
in general`
  }
]

/**
 * @type {Array<TErrorReport>}
 */
const errorReportsWithJSONDetails = jsonValues.reduce((/* @type {Array<TErrorReport>} */ acc, details) => {
  return acc.concat(errorReportsWithAndWithoutStack.map(base => ({ ...base, details })))
}, [])

/**
 * @type {Array<TErrorReport>}
 */
const errorReports = errorReportsWithAndWithoutStack.concat(errorReportsWithJSONDetails)

/**
 * @param {unknown} report
 * @param {string } expectedMessage
 * @param {string} [expectedStack]
 * @param {TJSONValue} [expectedDetails]
 * @return {void}
 */
function shouldBeAnErrorReport(report, expectedMessage, expectedStack, expectedDetails) {
  console.dir(report, { depth: 99 })

  report.should.be.an.Object()
  report.should.have.property('message', expectedMessage)

  if (expectedStack) {
    report.should.have.property('stack', expectedStack)
  } else {
    report.should.not.have.property('stack')
  }

  if (expectedDetails) {
    report.should.have.property('details')
    report.details.should.deepEqual(expectedDetails)
  } else {
    report.should.not.have.property('details')
  }

  shouldBeStringifiable(report)
}

module.exports = { errorReports, shouldBeAnErrorReport }
