/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { stuffWithUndefined, notASimpleError } = require('../_util/stuff')
const { validateSchema } = require('../_util/validateSchema')
const { serviceRunReports } = require('../60.service/ServiceRunReportCommon')
const { UnsavedRunReportThrowable } = require('../../lib/runReport/UnsavedRunReportThrowable')
const { unsavedRunReportThrowables } = require('./UnsavedRunReportThrowableCommon')

describe(testName, function () {
  validateSchema(
    UnsavedRunReportThrowable,
    unsavedRunReportThrowables,
    stuffWithUndefined
      .concat(stuffWithUndefined.map(runReport => ({ ...serviceRunReports[0], runReport })))
      .concat(stuffWithUndefined.filter(notASimpleError).map(error => ({ ...serviceRunReports[6], error })))
  )
})
