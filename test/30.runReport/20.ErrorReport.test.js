/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuff, stuffWithUndefined, notATrimmedString, notAString } = require('../_util/stuff')
const { notJSONValues } = require('../01.schemata/JSONValueCommon')
const { errorReports, shouldBeAnErrorReport } = require('./ErrorReportCommon')
const { ErrorReport, singleLineTrimmed, makeStringifiable, errorReport } = require('../../lib/runReport/ErrorReport')
const { TrimmedNonEmptyString } = require('../../lib/TrimmedNonEmptyString')
const { inspect } = require('node:util')
const { messageWithCauses, stackWithCauses } = require('pony-cause')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')

function doesNotHaveAMessage(s) {
  if (typeof s !== 'object' || s === null) {
    return true
  }

  const { success, data } = TrimmedNonEmptyString.safeParse(s.message)
  return !success || data.includes('circular') // this forces a failing counter example to pass!
  // NOTE: https://github.com/colinhacks/zod?tab=readme-ov-file#cyclical-objects
}

describe(testName, function () {
  describe('ErrorReport', function () {
    validateSchema(
      ErrorReport,
      errorReports,
      stuff
        .filter(doesNotHaveAMessage)
        .concat(stuffWithUndefined.filter(notATrimmedString).map(message => ({ ...errorReports[0], message })))
        .concat(stuff.filter(notAString).map(stack => ({ ...errorReports[0], stack })))
        .concat(notJSONValues.filter(j => j !== undefined).map(details => ({ ...errorReports[0], details })))
    )
  })

  describe('singleLineTrimmed', function () {
    const cases = stuffWithUndefined.concat([
      '\na multiline\nstring\n\non *nix',
      'a multiline\r\nstring\r\n\r\non windows\r\n',
      'a multiline\nstring\r\n\r\nwith mixed\n\r\nplatform\r\n\nend-of-lines\r'
    ])

    cases.forEach(c => {
      it(`should return a string without EOLs for ${inspect(c)}`, function () {
        const result = singleLineTrimmed(c)

        console.log(result)

        result.should.be.a.String()
        result.should.not.containEql('\n')
        result.should.not.containEql('\r')
        result.trim().should.equal(result)
      })
    })
  })

  describe('makeStringifiable', function () {
    stuffWithUndefined.concat(stuffWithUndefined.map(s => [s])).forEach(s => {
      it(`makes ${inspect(s)} stringifiable`, function () {
        const result = makeStringifiable(s)

        shouldBeStringifiable(result)
      })
    })
  })

  describe('errorReport', function () {
    describe('not an object', function () {
      stuffWithUndefined
        .filter(s => s === null || typeof s !== 'object')
        .forEach(s => {
          it(`only returns a string-version of “${String(s)}”`, function () {
            const result = errorReport(s)

            shouldBeAnErrorReport(result, singleLineTrimmed(s))
          })
        })
    })

    describe('objects without a cause', function () {
      stuffWithUndefined
        .filter(s => s !== null && typeof s === 'object')
        .forEach(s => {
          it(`reports stacks and details of “${inspect(s)}”`, function () {
            const result = errorReport(s)

            shouldBeAnErrorReport(
              result,
              s instanceof Error ? s.message : '',
              s instanceof Error ? s.stack : undefined,
              makeStringifiable(s)
            )
          })
        })
    })

    describe('objects with a cause', function () {
      const objectsWithStuffAsACause = stuffWithUndefined.map(cause => ({ cause }))

      objectsWithStuffAsACause.forEach(s => {
        it(`reports stacks and details of “${inspect(s)}”`, function () {
          const result = errorReport(s)

          // noinspection JSCheckFunctionSignatures
          shouldBeAnErrorReport(result, messageWithCauses(s), s.stack, makeStringifiable(s))
        })
      })
    })

    describe('errors with a cause', function () {
      const errorWithACause = new Error('has a cause')
      errorWithACause.cause = new Error('the ultimate cause')
      const errorWithACauseInACause = new Error('has a cause in a cause')
      errorWithACauseInACause.cause = errorWithACause

      const errorsWithStuffAsACause = stuffWithUndefined.map(cause => {
        const err = new Error(inspect(cause))
        err.cause = cause
        return err
      })

      const cases = [errorWithACause, errorWithACauseInACause].concat(errorsWithStuffAsACause)

      cases.forEach(s => {
        it(`reports stacks and details of “${inspect(s)}”`, function () {
          const result = errorReport(s)

          shouldBeAnErrorReport(
            result,
            singleLineTrimmed(messageWithCauses(s)),
            stackWithCauses(s),
            makeStringifiable(s)
          )
        })
      })
    })
  })
})
