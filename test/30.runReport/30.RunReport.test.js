/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const {
  stuffWithUndefined,
  notANonEmptyString,
  notABoolean,
  notATrimmedString,
  notANonNegativeSafeInteger
} = require('../_util/stuff')
const { validateSchema } = require('../_util/validateSchema')
const { RunReport } = require('../../lib/runReport/RunReport')
const { runReports, successRunReport } = require('./RunReportCommon')

describe(testName, function () {
  validateSchema(
    RunReport,
    runReports,
    stuffWithUndefined
      .concat(stuffWithUndefined.filter(notANonNegativeSafeInteger).map(v => ({ ...successRunReport, v })))
      .concat(stuffWithUndefined.filter(notATrimmedString).map(environment => ({ ...successRunReport, environment })))
      .concat(stuffWithUndefined.filter(notATrimmedString).map(service => ({ ...successRunReport, service })))
      .concat(stuffWithUndefined.filter(notANonEmptyString).map(start => ({ ...successRunReport, start })))
      .concat(stuffWithUndefined.filter(notANonEmptyString).map(end => ({ ...successRunReport, end })))
      .concat(stuffWithUndefined.filter(notANonEmptyString).map(duration => ({ ...successRunReport, duration })))
      .concat(stuffWithUndefined.filter(notABoolean).map(success => ({ ...successRunReport, success })))
  )
})
