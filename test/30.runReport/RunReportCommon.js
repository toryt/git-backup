/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { runDate, testBaseDirectoryName } = require('../constants')
const { environmentInfo } = require('./EnvironmentInfoCommon')
const { readFile } = require('node:fs/promises')
const { parse } = require('yaml')

/**
 * @type {Omit<TRunReport, 'success'>}}
 */
const runReportBase = {
  v: 1,
  environment: environmentInfo,
  git: 'git version 9.99.999',
  service: testBaseDirectoryName,
  start: runDate.toISOString(),
  end: '2024-03-25T07:23:13.949Z',
  duration: 'PT84H23M23.345S'
}

/**
 * @type {TRunReport & {success: true}}
 */
const successRunReport = {
  ...runReportBase,
  success: true
}

/**
 * @type {TRunReport & {success: false}}
 */
const failureRunReport = {
  ...runReportBase,
  success: false
}

/**
 * @type {Array<TRunReport>}
 */
const runReports = [successRunReport, failureRunReport]

/**
 *
 * @param {string} runReportPath
 * @param {TRunReport} runReport
 * @param {Desensifier} desensifier
 * @param {import('zod').ZodType} schema
 * @returns {Promise<void>}
 */
async function shouldBeExpectedRunReport(runReportPath, runReport, desensifier, schema) {
  const fileContents = await readFile(runReportPath, { encoding: 'utf8' })
  const fromFile = parse(fileContents)
  fromFile.should.deepEqual(desensifier.desensify(runReport))
  schema.parse(fromFile)
}

module.exports = { runReportBase, successRunReport, failureRunReport, runReports, shouldBeExpectedRunReport }
