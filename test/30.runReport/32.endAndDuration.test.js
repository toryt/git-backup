/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { asISODuration, endAndDuration } = require('../../lib/runReport/endAndDuration')
const { ISODuration } = require('../../lib/runReport/ISODuration')
const { Temporal } = require('@js-temporal/polyfill')
const testName = require('../testName')(module)

describe(testName, function () {
  describe('asISODuration', function () {
    const cases = [
      { ms: 0, expected: 'PT0S' },
      { ms: 0.1, expected: 'PT0S' },
      { ms: 0.001, expected: 'PT0S' },
      { ms: 0.503004, expected: 'PT0.001S' },
      { ms: 1, expected: 'PT0.001S' },
      { ms: 100, expected: 'PT0.1S' },
      { ms: 100.0001, expected: 'PT0.1S' },
      { ms: 100.8001, expected: 'PT0.101S' },
      { ms: 199.8001, expected: 'PT0.2S' },
      { ms: 3.5, expected: 'PT0.004S' },
      { ms: 1000, expected: 'PT1S' },
      { ms: 999.9, expected: 'PT1S' },
      { ms: 1001, expected: 'PT1.001S' },
      { ms: 1001.22, expected: 'PT1.001S' },
      { ms: 1001.92, expected: 'PT1.002S' },
      { ms: 59000, expected: 'PT59S' },
      { ms: 59999.332, expected: 'PT59.999S' },
      { ms: 59999.9997000004, expected: 'PT1M' },
      { ms: 60000, expected: 'PT1M' },
      { ms: 60000.234, expected: 'PT1M' },
      { ms: 60000.8, expected: 'PT1M0.001S' },
      { ms: 60122.8, expected: 'PT1M0.123S' },
      { ms: 61122.3, expected: 'PT1M1.122S' },
      { ms: (60 * 3 + 23.33) * 1000, expected: 'PT3M23.33S' },
      { ms: (60 * 59 + 23.3359) * 1000, expected: 'PT59M23.336S' },
      { ms: (60 * 59 + 59.999) * 1000, expected: 'PT59M59.999S' },
      { ms: (60 * 59 + 59.9995) * 1000, expected: 'PT1H' },
      { ms: 3600 * 1000, expected: 'PT1H' },
      { ms: ((24 * 123 + 7) * 3600 + 44 * 60 + 58.987) * 1000, expected: 'PT2959H44M58.987S' },
      { ms: Number.MAX_SAFE_INTEGER },
      { ms: Number.MAX_VALUE }
    ]

    cases.forEach(({ ms, expected }) => {
      it(`transforms ${ms}${expected ? ` into ${expected}` : ''}`, function () {
        const result = asISODuration(ms)

        ISODuration.parse(result)
        if (expected) {
          result.should.equal(expected)
        } else {
          console.log(result)
        }
        Temporal.Duration.from(result).total('millisecond').should.equal(Math.round(ms))
      })
    })
  })

  describe('endAndDuration', function () {
    it('returns 0 duration when expected', function () {
      const end = new Date(2024, 3, 28, 11, 28, 25, 8)

      const result = endAndDuration(end, end)

      console.dir(result)
      result.should.be.an.Object()
      result.should.have.property('end', end.toISOString())
      result.should.have.property('duration')
      ISODuration.parse(result.duration)
      Temporal.Duration.from(result.duration).total('millisecond').should.equal(0)
    })

    it('returns what is expected', function () {
      const start = new Date(2024, 3, 28, 11, 27, 4, 334)
      const end = new Date(2024, 3, 28, 11, 28, 25, 8)

      const result = endAndDuration(start, end)

      console.dir(result)
      result.should.be.an.Object()
      result.should.have.property('end', end.toISOString())
      result.should.have.property('duration')
      ISODuration.parse(result.duration)
      Temporal.Duration.from(result.duration)
        .total('millisecond')
        .should.equal(end.getTime() - start.getTime())
    })
  })
})
