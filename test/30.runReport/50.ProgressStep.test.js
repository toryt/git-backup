/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../testName')(module)
const { validateSchema } = require('../_util/validateSchema')
const { stuffWithUndefined, notABoolean, notASimpleError } = require('../_util/stuff')
const { ProgressStep, ProgressStepSuccess, ProgressStepFailure } = require('../../lib/runReport/ProgressStep')
const { create } = require('../../lib/repository/GitCreateProgressStep')
const { errorReports } = require('./ErrorReportCommon')

const progressSuccess = {
  action: create,
  start: '2024-01-04T09:19:54.301Z',
  end: '2024-01-04T09:19:55.301Z',
  duration: 'PT1S',
  success: true
}
const progressExamples = [progressSuccess, { ...progressSuccess, success: false }]
const progressFailure = { ...progressSuccess, success: false, error: errorReports[0] }

describe(testName, function () {
  describe('ProgressStep', function () {
    validateSchema(
      ProgressStep,
      progressExamples,
      stuffWithUndefined
        .concat(stuffWithUndefined.map(start => ({ ...progressSuccess, start })))
        .concat(stuffWithUndefined.map(end => ({ ...progressSuccess, end })))
        .concat(stuffWithUndefined.map(duration => ({ ...progressSuccess, duration })))
        .concat(stuffWithUndefined.filter(notABoolean).map(success => ({ ...progressSuccess, success })))
    )
  })

  describe('ProgressStepSuccess', function () {
    validateSchema(
      ProgressStepSuccess,
      [progressSuccess],
      stuffWithUndefined.concat(
        stuffWithUndefined
          .filter(stuffWithUndefined => stuffWithUndefined !== true)
          .map(success => ({ ...progressSuccess, success }))
      )
    )
  })

  describe('ProgressStepFailure', function () {
    validateSchema(
      ProgressStepFailure,
      [progressFailure],
      stuffWithUndefined
        .concat(
          stuffWithUndefined
            .filter(stuffWithUndefined => stuffWithUndefined !== false)
            .map(success => ({ ...progressFailure, success }))
        )
        .concat(stuffWithUndefined.filter(notASimpleError).map(error => ({ ...progressFailure, error })))
    )
  })
})
