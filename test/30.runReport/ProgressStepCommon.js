/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2023 – 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

const { ProgressStepFailure } = require('../../lib/runReport/ProgressStep')
const { shouldBeStringifiable } = require('../_util/shouldBeStringifiable')
const { errorReport } = require('../../lib/runReport/ErrorReport')

/**
 * @param {TProgressStep} progress
 * @param {unknown} failure
 * @param {string} [start]
 * @returns {void}
 */
function shouldBeProgressStep(progress, failure, start) {
  progress.should.be.an.Object()
  progress.start.should.be.a.String()
  progress.start.should.not.be.empty()
  if (start) {
    progress.start.should.equal(start)
  }
  progress.success.should.be.a.Boolean()
  progress.success.should.equal(!failure)
  if (failure) {
    const progressFailure = ProgressStepFailure.parse(progress)
    progressFailure.error.should.be.an.Object()
    if (typeof failure === 'object') {
      progressFailure.error.should.deepEqual(errorReport(failure))
    }
  } else {
    progress.should.not.have.property('error')
  }

  shouldBeStringifiable(progress)
}

module.exports = { shouldBeProgressStep }
