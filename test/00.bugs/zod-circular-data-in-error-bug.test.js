/*
 * `git-backup` backups all repositories in a `git` service credentials give access to.
 * Copyright © 2024 Jan Dockx
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const { z, ZodError } = require('zod')
const { fail } = require('assert')

/**
 * Demonstration of bug.
 *
 * NOTE: submitted as https://github.com/colinhacks/zod/issues/3422
 */
describe('zod', function () {
  it('cannot deal with circular data structures', function () {
    const AnObjectSchema = z.object({ someLiteralProperty: z.literal(1) })

    /**
     * @type {Record<string, unknown>}
     */
    const cicrularObject = {
      aProperty: 'a property',
      anotherProperty: 137,
      anObjectProperty: { anObjectPropertyProperty: 'an object property in a property' },
      anArrayProperty: [{ anArrayObjectPropertyProperty: 'an object property in a property' }]
    }
    cicrularObject.anObjectProperty.cicrularObject = cicrularObject
    cicrularObject.anArrayProperty.push(cicrularObject.anObjectProperty)
    const violatingObject = { someLiteralProperty: cicrularObject }

    const { success, error } = AnObjectSchema.safeParse(violatingObject)

    success.should.be.false()
    error.should.be.an.instanceof(ZodError)
    try {
      const message = error.message
      fail(`will never get here: ${message}`)
    } catch (tErr) {
      console.log(tErr)
      tErr.should.be.instanceof(TypeError)
    }
  })
})
