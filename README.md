<!--
`git-backup` backups all repositories in a `git` service credentials give access to.
Copyright © 2017 – 2024 Jan Dockx

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see
<https://www.gnu.org/licenses/>.
-->

Node script that backups all repositories found under a Bitbucket or GitHub account. Put it in a crontab.

Bare mirror clones for all repositories found are created or updated. Each run stores a report next to the mirror clone.

A directory structure that organises the repositories by service, owner (user or team), and repository name with
symbolic links is maintained. In case of issues with the service, this directory structure can immediately be made
available via a supported `git` protocol.
